package pe.gob.mc.cnp.view.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class NumerosComaPunto implements Validator {

	
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2)
			throws ValidatorException {
		
		String contenidoCampo = (String) arg2;
		contenidoCampo= contenidoCampo.trim();
		
		// parametros que se van a validar
		Pattern pattern =  Pattern.compile("[^0-9.,-]+");
		// para compara
		Matcher matcher = pattern.matcher(contenidoCampo);
		//-----------------
		boolean resultado =  matcher.find();
		boolean caracteresIlegales = false;
		
		
		while(resultado){
			caracteresIlegales=true;
			resultado=matcher.find();			
		}
		//-------------------------------------
		if (caracteresIlegales==true){
			
			throw new ValidatorException(new FacesMessage("El parametro no es correcto."));
		}
		
		
		if(contenidoCampo == null || contenidoCampo.equals("")){
			throw new ValidatorException(new FacesMessage("El parametro es nulo."));
		    
		}
		
	}
}
