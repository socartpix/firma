package pe.gob.mc.cnp.view.reports;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.FontKey;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.PdfFont;
import net.sf.jasperreports.engine.util.JRLoader;
import com.itextpdf.text.pdf.BaseFont;

import pe.gob.mc.cnp.model.factory.Factory;

@SuppressWarnings("deprecation")
public class UtilReporte
{
	private static UtilReporte utilreporte;
    private ServletOutputStream out;
    protected String delimiter;


    public UtilReporte()
	{	
	}
    
	public final synchronized static UtilReporte getInstance()
	{
		if(utilreporte==null)
		{
			utilreporte=new UtilReporte();
		}
		return utilreporte;
	}


	public static Connection getConnection() {
        Connection conn = null;
        try {        	
        	//***PARA DESARROOLO***
        	conn = (Connection) Factory.getInstance().getConnectionDb().getConnection();
        	
        	//***PARA PRODUCCION***
        	//InitialContext ctx = new InitialContext();            
            //DataSource ds = (DataSource) ctx.lookup("jdbc/CertificadosPool");
            //conn = ds.getConnection();	
        	
            //System.out.println("Connected to the database");
        } catch (Exception e) {
            e.printStackTrace();
        }		
        return conn; 	
    }	
	
	
	public void closeConeccionDB(Connection conn){
		try {
			conn.close();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
	

	@SuppressWarnings("rawtypes")
	public void imprimir(String nombreRpt, int tipoReporte, Map parametros,ServletContext servletContext,HttpServletResponse response, Integer opcReporte){
		switch (tipoReporte) {
			case 1:
			{   System.err.println(nombreRpt);
				imprimirPDF(nombreRpt,parametros,servletContext,response,opcReporte);
				break;
			}
			case 2 :
			{
				imprimirXLS(nombreRpt,parametros,servletContext,response);
				break;
			}		
		}
	}
	
	
	@SuppressWarnings("rawtypes")
	public void imprimirObjeto(String nombreRpt, int tipoReporte, Collection lista, Map parametros,ServletContext servletContext,HttpServletResponse response, Integer opcReporte){
		switch (tipoReporte) {
			case 1:
			{
				imprimirObjetoToPDF(nombreRpt, lista, parametros, servletContext, response, opcReporte);
				break;
			}
			case 2 :
			{
				imprimirObjetoToXLS(nombreRpt, lista, parametros, servletContext, response);
				break;
			}		
		}
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void imprimirPDF(String nombreRpt, Map parametros,ServletContext servletContext,HttpServletResponse response,Integer opc){
		Connection conn = getConnection();		
		try {	
	//		
			
			JasperReport jasperReport = (JasperReport)JRLoader.loadObject(servletContext.getRealPath("/")+"/reports/"+nombreRpt+".jasper");
  
			//Importacion Fuentes 
			HashMap<FontKey, PdfFont> fontMap = new HashMap<FontKey, PdfFont>();
			//HashMap fontMapGet = new HashMap();
			FontKey key1 = new FontKey("Arial", true, true);  
			PdfFont font1 = new PdfFont(servletContext.getRealPath("")+"\\reports\\Font\\arial.ttf",BaseFont.IDENTITY_H,true);
			FontKey key2 = new FontKey("Arial Black", true, true);  
			PdfFont font2 = new PdfFont(servletContext.getRealPath("")+"\\reports\\Font\\ARIBLK.TTF",BaseFont.IDENTITY_H,true); 
			
		System.out.println("ruta font: " + servletContext.getRealPath("")+"\\reports\\Font\\ARIALNB.TTF");
			
			FontKey key3 = new FontKey("Arial Narrow", true, true);  
			PdfFont font3 = new PdfFont(servletContext.getRealPath("")+"\\reports\\Font\\ARIALN.TTF",BaseFont.IDENTITY_H,true); 
			FontKey key4 = new FontKey("Arial Narrow1", true, true);  
			PdfFont font4 = new PdfFont(servletContext.getRealPath("")+"\\reports\\Font\\ARIALNB.TTF",BaseFont.IDENTITY_H,true); 
            FontKey key5 = new FontKey("Arial Narrow2", true, true);  
			PdfFont font5 = new PdfFont(servletContext.getRealPath("")+"\\reports\\Font\\ARIALNBI.TTF",BaseFont.IDENTITY_H,true); 
			FontKey key6 = new FontKey("Arial Narrow3", true, true);  
			PdfFont font6 = new PdfFont(servletContext.getRealPath("")+"\\reports\\Font\\ARIALNI.TTF",BaseFont.IDENTITY_H,true); 
			FontKey key7 = new FontKey("Arial Narrow4", true, true);  
			PdfFont font7 = new PdfFont(servletContext.getRealPath("")+"\\reports\\Font\\ARIBLK.TTF",BaseFont.IDENTITY_H,true); 
			
			
			fontMap.put(key1, font1);
			fontMap.put(key2, font2);
			
			fontMap.put(key3, font3);
			fontMap.put(key4, font4);
			fontMap.put(key5, font5);
			fontMap.put(key6, font6);
			fontMap.put(key7, font7);
			
			JRPdfExporter exporter = new JRPdfExporter();
            //exporter.setParameter(JRExporterParameter.JASPER_PRINT, jprint);
            //exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, rtfOutput);
            //exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, fileName);
            exporter.setParameter(JRPdfExporterParameter.FONT_MAP, fontMap);
            exporter.setParameter(JRPdfExporterParameter.FORCE_SVG_SHAPES,false);
            //exporter.exportReport();
			
		  //  exporter.exportReport ();
		
			byte[] fichero = JasperRunManager.runReportToPdf(jasperReport,parametros,conn);

			// exporter.exportReport ();
			 
				response.setContentType ("application/pdf");
			 
			//
			 java.text.SimpleDateFormat sdHora=new java.text.SimpleDateFormat("HH mm ss");	
	 	    // response.setHeader("Content-Disposition","attachment;filename=\"actas"+" "+sdHora.format(new Date())+".pdf\"");
	       
			if(opc==1){
				response.setHeader ("Content-disposition", "inline; filename="+nombreRpt+"_"+sdHora.format(new Date())+".pdf");	
			}else{
				 response.setHeader("Content-disposition", "attachment; filename="+nombreRpt+"_"+sdHora.format(new Date())+".pdf");	
			}
 
			    response.setHeader("Cache-Control", "max-age=30");
		        response.setHeader("Pragma", "No-cache");
		        response.setDateHeader("Expires",0);
		        response.setContentLength(fichero.length);
		        out = response.getOutputStream();
		        out.write(fichero,0,fichero.length);
		        out.flush();
		        out.close();
			   
			    closeConeccionDB(conn);

		} catch (Exception e) {
			System.err.println(e.getCause());
		    System.out.println("Util caida reporte "+e.getMessage());
			closeConeccionDB(conn);
		}
		
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void imprimirObjetoToPDF(String nombreRpt, Collection lista, Map parametros,ServletContext servletContext,HttpServletResponse response,Integer opcReporte){
		
		try {
			JRBeanCollectionDataSource data = new JRBeanCollectionDataSource(lista);
						
			JasperReport jasperReport = (JasperReport)JRLoader.loadObject(servletContext.getRealPath("/")+"/reports/"+nombreRpt+".jasper");
   
			//Importacion Fuentes 
	    	HashMap<FontKey, PdfFont> fontMap = new HashMap<FontKey, PdfFont>();
			//HashMap fontMapGet = new HashMap();
			FontKey key1 = new FontKey("Arial", true, true);  
			PdfFont font1 = new PdfFont(servletContext.getRealPath("")+"/reports/Font/arial.ttf",BaseFont.IDENTITY_H,true);
			FontKey key2 = new FontKey("Arial Black", true, true);  
			PdfFont font2 = new PdfFont(servletContext.getRealPath("")+"/reports/Font/ARIBLK.TTF",BaseFont.IDENTITY_H,true); 
						
			fontMap.put(key1, font1);
			fontMap.put(key2, font2);
			JRPdfExporter exporter = new JRPdfExporter();
            //exporter.setParameter(JRExporterParameter.JASPER_PRINT, jprint);
            //exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, rtfOutput);
            //exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, fileName);
            exporter.setParameter(JRPdfExporterParameter.FONT_MAP, fontMap);
            exporter.setParameter(JRPdfExporterParameter.FORCE_SVG_SHAPES,false);
            //exporter.exportReport();			
			
			byte[] fichero = JasperRunManager.runReportToPdf(jasperReport, parametros, data);
			
			response.setContentType ("application/pdf");			
			//
			SimpleDateFormat formateador=new SimpleDateFormat("ddMMyyyyHHmmss");	
	 	    // response.setHeader("Content-Disposition","attachment;filename=\"actas"+" "+sdHora.format(new Date())+".pdf\"");	       
			if(opcReporte==1){
				response.setHeader ("Content-disposition", "inline; filename="+nombreRpt+"_"+formateador.format(new Date())+".pdf");	
			}else{
				 response.setHeader("Content-disposition", "attachment; filename="+nombreRpt+"_"+formateador.format(new Date())+".pdf");	
			}

			    response.setHeader("Cache-Control", "max-age=30");
		        response.setHeader("Pragma", "No-cache");
		        response.setDateHeader("Expires",0);
		        response.setContentLength(fichero.length);
		        out = response.getOutputStream();
		        out.write(fichero,0,fichero.length);
		        out.flush();
		        out.close();			    

		} catch (Exception e) {
			System.err.println(e.getMessage());
		    System.out.println("Util caida reporte "+e.getMessage());			
		}
		
	}
		
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public byte[] getPdfBytes(String nombreRpt, Map parametros,ServletContext servletContext) {		
		byte[] fichero = null;	
		Connection conn = getConnection();
		try {			
			JasperReport jasperReport = (JasperReport)JRLoader.loadObject(servletContext.getRealPath("/")+"/reports/"+nombreRpt+".jasper");	   
			fichero = JasperRunManager.runReportToPdf(jasperReport,parametros,conn);
			closeConeccionDB(conn);				
		} catch (Exception e) {
			System.err.println(e.getMessage());
			closeConeccionDB(conn);
		}		
		return fichero;	
			
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public byte[] getObjetoToPdfBytes(String nombreRpt, Collection lista, Map parametros,ServletContext servletContext) {		
		byte[] fichero = null;		
		try {	
			JRBeanCollectionDataSource data = new JRBeanCollectionDataSource(lista);
			JasperReport jasperReport = (JasperReport)JRLoader.loadObject(servletContext.getRealPath("/")+"/reports/"+nombreRpt+".jasper");
	        fichero = JasperRunManager.runReportToPdf(jasperReport,parametros,data);			
		} catch (Exception e) {
			System.err.println(e.getMessage());			
		}		
		return fichero;				
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public byte[] getXLSBytes(String nombreRpt, Map parametros,ServletContext servletContext) {		
		byte[] fichero = null;	
		Connection conn = getConnection();
		try {			
			JasperPrint jasperPrint=JasperFillManager.fillReport (servletContext.getRealPath("/") + System.getProperty("file.separator")+ "reports" + System.getProperty("file.separator")+ nombreRpt+".jasper",parametros,conn);
			String xlsFileName = nombreRpt+".xls";					

			//Creacion del XLS
			JRXlsExporter exporter = new JRXlsExporter ();
			ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
			
			exporter.setParameter (JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter (JRExporterParameter.OUTPUT_STREAM, xlsReport);
			exporter.setParameter (JRExporterParameter.OUTPUT_FILE_NAME, xlsFileName);
			exporter.setParameter (JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
			exporter.setParameter (JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.FALSE);
			exporter.setParameter (JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);			
			exporter.exportReport ();
			
			fichero = xlsReport.toByteArray();		
			closeConeccionDB(conn);
		} catch (Exception e) {
			System.err.println(e.getMessage());	
			closeConeccionDB(conn);
		}		
		return fichero;				
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public byte[] getObjetoToXLSBytes(String nombreRpt, Collection lista, Map parametros,ServletContext servletContext) {		
		byte[] fichero = null;		
		try {			
			JRBeanCollectionDataSource data = new JRBeanCollectionDataSource(lista);			
			JasperPrint jasperPrint=JasperFillManager.fillReport (servletContext.getRealPath("/") + System.getProperty("file.separator")+ "reports" + System.getProperty("file.separator")+ nombreRpt+".jasper",parametros,data);
			String xlsFileName = nombreRpt+".xls";					

			//Creacion del XLS
			JRXlsExporter exporter = new JRXlsExporter ();
			ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
			
			exporter.setParameter (JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter (JRExporterParameter.OUTPUT_STREAM, xlsReport);
			exporter.setParameter (JRExporterParameter.OUTPUT_FILE_NAME, xlsFileName);
			exporter.setParameter (JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
			exporter.setParameter (JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.FALSE);
			exporter.setParameter (JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);			
			exporter.exportReport ();
			
			fichero = xlsReport.toByteArray();			
		} catch (Exception e) {
			System.err.println(e.getMessage());			
		}		
		return fichero;				
	}

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void imprimirXLS(String nombreRpt, Map parametros,ServletContext servletContext,HttpServletResponse response){
		Connection conn = getConnection();	
		try {			
			int bit;
			File f;
			InputStream in;
		
			//System.out.println("ruta jasper = " + servletContext.getRealPath("/") + "reports" + System.getProperty("file.separator")+ nombreRpt+".jasper");
			JasperPrint jasperPrint=JasperFillManager.fillReport (servletContext.getRealPath("/") + System.getProperty("file.separator")+ "reports" + System.getProperty("file.separator")+ nombreRpt+".jasper",parametros,conn);
			SimpleDateFormat formateador=new SimpleDateFormat("ddMMyyyyHHmmss");				
			String xlsFileName = nombreRpt+"_"+formateador.format(new Date())+".xls";

			//Creacion del XLS
			JRXlsExporter exporter = new JRXlsExporter ();
			exporter.setParameter (JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter (JRExporterParameter.OUTPUT_FILE_NAME, xlsFileName);
			exporter.setParameter (JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
			exporter.setParameter (JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.FALSE);
			exporter.setParameter (JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			exporter.exportReport ();
			
			// En este punto ya esta Creado el XLS
			// Ahora leemos el fichero y forzamos al navegador a que nos descargue el fichero.
			f = new File ( xlsFileName);

			//Configurar cabecera http
			response.setContentType ("application/vnd.ms-excel"); //Tipo de fichero.
			response.setHeader ("Content-Disposition", "attachment;filename=\"" + xlsFileName + "\"");

			in = new FileInputStream (f);
			out = response.getOutputStream ();

			bit = 256;
			while ((bit)>= 0)
			{
			   bit = in.read ();
			   out.write (bit);
			}

			out.flush ();
			out.close ();
			in.close ();

			closeConeccionDB(conn);

		} catch (Exception e) {
			System.err.println(e.getMessage());
			closeConeccionDB(conn);
		}
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })	
	public void imprimirObjetoToXLS(String nombreRpt, Collection lista, Map parametros,ServletContext servletContext,HttpServletResponse response){
		try {
			JRBeanCollectionDataSource data = new JRBeanCollectionDataSource(lista);
			//JasperReport jasperReport = (JasperReport)JRLoader.loadObject(servletContext.getRealPath("/")+"/reports/"+nombreRpt+".jasper");
			
			int bit;
			File f;
			InputStream in;
		
			//System.out.println("ruta jasper = " + servletContext.getRealPath("/") + "reports" + System.getProperty("file.separator")+ nombreRpt+".jasper");
			JasperPrint jasperPrint=JasperFillManager.fillReport (servletContext.getRealPath("/") + System.getProperty("file.separator")+ "reports" + System.getProperty("file.separator")+ nombreRpt+".jasper",parametros,data);
		
			SimpleDateFormat formateador=new SimpleDateFormat("ddMMyyyyHHmmss");				
			String xlsFileName = nombreRpt+"_"+formateador.format(new Date())+".xls";						

			//Creacion del XLS
			JRXlsExporter exporter = new JRXlsExporter ();
			exporter.setParameter (JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter (JRExporterParameter.OUTPUT_FILE_NAME, xlsFileName);
			exporter.setParameter (JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
			exporter.setParameter (JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.FALSE);
			exporter.setParameter (JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);			
			exporter.exportReport ();
			
			// En este punto ya esta Creado el XLS
			// Ahora leemos el fichero y forzamos al navegador a que nos descargue el fichero.
			f = new File ( xlsFileName);

			//Configurar cabecera http
			response.setContentType ("application/vnd.ms-excel"); //Tipo de fichero.
			response.setHeader ("Content-Disposition", "attachment;filename=\"" + xlsFileName + "\"");

			in = new FileInputStream (f);
			out = response.getOutputStream ();

			bit = 256;
			while ((bit)>= 0)
			{
			   bit = in.read ();
			   out.write (bit);
			}

			out.flush ();
			out.close ();
			in.close ();
			

		} catch (Exception e) {
			System.err.println(e.getMessage());			
		}
	}

}