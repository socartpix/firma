package pe.gob.mc.cnp.model.factory;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pe.gob.mc.cnp.model.factory.FactoryConstants;
import pe.gob.mc.cnp.model.service.BienService;
import pe.gob.mc.cnp.model.service.DetalleBienService;
import pe.gob.mc.cnp.model.service.DocumentoService;
import pe.gob.mc.cnp.model.service.ExpedienteService;
import pe.gob.mc.cnp.model.service.FichaService;
import pe.gob.mc.cnp.model.service.ObservacionHistoricoService;

public class Factory {
	
	private static Factory singleton = null;
	private static final String XML1 = "pe/gob/mc/cnp/model/config/applicationContext.xml";
	ApplicationContext ctx;

	private Factory() {
		ctx = new ClassPathXmlApplicationContext(XML1);
	}

	public static synchronized Factory getInstance() {
		if (singleton == null) {
			singleton = new Factory();
		} 
		return singleton;
	}
	
	
	public DataSource getConnectionDb(){
		return (DataSource) ctx.getBean("dataSource");
	}
	
	
	/**
	 *  Se ordena alfabeticamente para facilitar ubicacion de variables.
	 */	
	
	public BienService getBienBO() {
		return (BienService) ctx.getBean(FactoryConstants.BIEN_BO);
		}
	
	public DetalleBienService getDetalleBienBO() {
		return (DetalleBienService) ctx.getBean(FactoryConstants.DETALLEBIEN_BO);
		}
	
	public DocumentoService getDocumentoBO() {
		return (DocumentoService) ctx.getBean(FactoryConstants.DOCUMENTO_BO);
		}
	
	public ExpedienteService getExpedienteBO() {
		return (ExpedienteService) ctx.getBean(FactoryConstants.EXPEDIENTE_BO);
		}
	
	public FichaService getFichaBO() {
		return (FichaService) ctx.getBean(FactoryConstants.FICHA_BO);
		}
	
	public ObservacionHistoricoService getObservacionHistoricoBO() {
		return (ObservacionHistoricoService) ctx.getBean(FactoryConstants.OBSERVACIONHISTORICO_BO);
		}
	
}
