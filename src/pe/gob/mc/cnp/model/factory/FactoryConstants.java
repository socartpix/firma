package pe.gob.mc.cnp.model.factory;


public class FactoryConstants {	
		
	/**
	 *  Se ordena alfabeticamente para facilitar ubicacion de variables.
	 */
	
	public static final String BIEN_BO = "bienServicio";
	public static final String DETALLEBIEN_BO = "detalleBienServicio";
	public static final String DOCUMENTO_BO = "documentoServicio";
	public static final String EXPEDIENTE_BO = "expedienteServicio";
	public static final String FICHA_BO = "fichaServicio";
	public static final String OBSERVACIONHISTORICO_BO = "observacionHistoricoServicio";
	
}


