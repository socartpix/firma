package pe.gob.mc.cnp.model.dao.iface;

import java.util.Map;

import pe.gob.mc.cnp.model.domain.ObservacionHistorico;

public interface ObservacionHistoricoMapper {

	Boolean buscarObservacionHistoricoXFicha(Map<String, Object> parameter);
	 
//	Boolean leerObservacionHistorico(Map<String, Object> parameter);
	
	void insertarObservacionHistorico(ObservacionHistorico oObservacionHistorico);
	
	void modificarObservacionHistorico(ObservacionHistorico oObservacionHistorico);
	
	void modificarEstadoObservacionHistorico(ObservacionHistorico oObservacionHistorico);
	
	void modificarEstadoObservacionHistoricoActivo(ObservacionHistorico oObservacionHistorico);
		
}
