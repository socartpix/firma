package pe.gob.mc.cnp.model.dao.iface;

import java.util.Map;

import pe.gob.mc.cnp.model.domain.Bien;

public interface BienMapper {
	
	Boolean buscarBienesXFicha(Map<String, Object> parameter);
	 
	Boolean leerBien(Map<String, Object> parameter);
	
	void insertarBien(Bien oBien);
	
	void modificarBien(Bien oBien);
	
	void modificarBienActivo(Bien oBien);
	
}
