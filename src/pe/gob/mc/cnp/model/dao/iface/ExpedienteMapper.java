package pe.gob.mc.cnp.model.dao.iface;

import java.util.Map;

import pe.gob.mc.cnp.model.domain.Expediente;

public interface ExpedienteMapper {

	Boolean buscarExpedienteTramiteDocumentario(Map<String, Object> parameter);
	 
	Boolean buscarExpedienteXFicha(Map<String, Object> parameter);
	
	Boolean buscarExpedienteXNroExpediente(Map<String, Object> parameter);	
	
	void insertarExpediente(Expediente oExpediente);
	
	void modificarExpediente(Expediente oExpediente);
	
	
}
