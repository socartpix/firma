package pe.gob.mc.cnp.model.dao.iface;

import java.util.Map;

import pe.gob.mc.cnp.model.domain.DetalleBien;

public interface DetalleBienMapper {

	Boolean buscarDetalleBienesXNumero(Map<String, Object> parameter);
	
	Boolean buscarDetalleBienesXCodigoFicha(Map<String, Object> parameter);	
	
	Boolean buscarDetalleBienesXCodigoBien(Map<String, Object> parameter); 
	 	
	void insertarDetalleBien(DetalleBien oDetalleBien);
		
	void modificarDetalleBienActivoXBien(DetalleBien oDetalleBien);
	
}
