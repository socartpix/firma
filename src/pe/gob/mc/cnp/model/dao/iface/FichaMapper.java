package pe.gob.mc.cnp.model.dao.iface;

import java.util.Map;

import pe.gob.mc.cnp.model.domain.CodigoHabilitado;
import pe.gob.mc.cnp.model.domain.Ficha;

public interface FichaMapper {

	Boolean buscarFichaXCriterio(Map<String, Object> parameter);
	 
	Boolean leerFicha(Map<String, Object> parameter);
	
    Boolean buscarUsuariosRegistradoresFicha(Map<String, Object> parameter);	

    Boolean buscarHorarios(Map<String, Object> parameter);	
    
	void insertarFicha(Ficha oFicha);
	
	void modificarFicha(Ficha oFicha);
	
	void modificarFichaAsignacion(Ficha oFicha);
	
	void generarNumeroCertificado(Ficha oFicha);
	
	void modificarFichaActivo(Ficha oFicha);	
	
	void cambiarEstadoFicha(Ficha oFicha);	
	
	void supervizarFicha(Ficha oFicha);
	
	void aprobarFicha(Ficha oFicha);
	
	void actualizarObservacionFicha(Ficha oFicha);	
	
	void insertarCodigoHabilitado(CodigoHabilitado oCodigoHabilitado);
	
	void eliminarCodigoHabilitadoPorCodigo(CodigoHabilitado oCodigoHabilitado);
	
    Boolean buscarCodigoBienDisponibleXFicha(Map<String, Object> parameter);	
    
    void modificarCantidadAutorizada(Ficha oFicha);    
    
    void modificarRegistroNoAtencion(Ficha oFicha); 
        
    Boolean buscarCodigoBienDisponibleXCodigoFicha(Map<String, Object> parameter);
    
    void eliminarTodaAsignacionXFicha(CodigoHabilitado oCodigoHabilitado);
    
    Boolean buscarCodigoBienDisponibleParaAsignar(Map<String, Object> parameter);
    
    void actualizarModoUtilizacionDelNumeroBien(CodigoHabilitado oCodigoHabilitado);
    
    Boolean buscarConsultaFichaPorPerfil(Map<String, Object> parameter);	
    
    
}
