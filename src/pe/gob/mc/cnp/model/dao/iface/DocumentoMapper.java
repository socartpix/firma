package pe.gob.mc.cnp.model.dao.iface;

import java.util.Map;

import pe.gob.mc.cnp.model.domain.Documento;

public interface DocumentoMapper {

	Boolean doListaArchivosTramite(Map<String, Object> parameter);
	
	Boolean buscarDocumentoXFicha(Map<String, Object> parameter);
	
	Boolean obtenerSiglasPersona(Map<String, Object> parameter);	
	 
	Boolean leerDocumento(Map<String, Object> parameter);
	
	Boolean buscarDocumentoXNumeroAnio(Map<String, Object> parameter);	
	
	void insertarDocumento(Documento oDocumento);
	
	void modificarDocumento(Documento oDocumento);
	
	void modificarDocumentoActivo(Documento oDocumento);
	
	void modificarPrincipalDocumento(Documento oDocumento);
	
	void insertarCertificado(Documento oDocumento);	

	Boolean buscarCertificadoXFicha(Map<String, Object> parameter);
	
	void modificarCertificadoActivo(Documento oDocumento);	
	
	void modificarCertificadoActivo_firma(Documento oDocumento);	
}
