package pe.gob.mc.cnp.model.domain;

import java.util.Date;
import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Clase Base.
 */
public class Base implements Serializable {

	private static final long serialVersionUID = 5142356345801522293L;
	
	private String 	nro;
	private String 	item;
	private String  activo;
	//private String  estado;
	private Date    fechaCreacion;
	private Date    fechaEdicion;
	private Integer sesionCreacion;
	private Integer sesionEdicion;
	private Integer estadoOut;
  	private String  mensajeOut;
  	private String codigoAnterior;
  	
	private Date    fechaValida;
	private Integer usuarioValida;
	private String  nombreUsuarioValida;
	
	//--
	//ACCION PAGINACION
	private String paginacion;
	private Integer paginaInicial;
	private Integer paginaFinal; 
	Boolean primero, anterior, siguiente, ultimo;
	
	//PAGINACION DB	
	private Integer totalRegistros;	
	private Integer registroInicial;
	private Integer registroFinal;
	//--
	
	public String getNombreUsuarioValida() {
		return nombreUsuarioValida;
	}

	public void setNombreUsuarioValida(String nombreUsuarioValida) {
		this.nombreUsuarioValida = nombreUsuarioValida;
	}

	public Date getFechaValida() {
		return fechaValida;
	}

	public void setFechaValida(Date fechaValida) {
		this.fechaValida = fechaValida;
	}

	public Integer getUsuarioValida() {
		return usuarioValida;
	}

	public void setUsuarioValida(Integer usuarioValida) {
		this.usuarioValida = usuarioValida;
	}

	public String getCodigoAnterior() {
		return codigoAnterior;
	}

	public void setCodigoAnterior(String codigoAnterior) {
		this.codigoAnterior = codigoAnterior;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	/*public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}*/

	public String getNro() {
		return nro;
	}

	public void setNro(String nro) {
		this.nro = nro;
	}
	
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
	public String getActivo() {
		return activo;
	}
	
	public void setActivo(String activo) {
		this.activo = activo;
	}
	
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	public Date getFechaEdicion() {
		return fechaEdicion;
	}
	
	public void setFechaEdicion(Date fechaEdicion) {
		this.fechaEdicion = fechaEdicion;
	}
	
	public Integer getSesionCreacion() {
		return sesionCreacion;
	}
	
	public void setSesionCreacion(Integer sesionCreacion) {
		this.sesionCreacion = sesionCreacion;
	}
	
	public Integer getSesionEdicion() {
		return sesionEdicion;
	}
	
	public void setSesionEdicion(Integer sesionEdicion) {
		this.sesionEdicion = sesionEdicion;
	}
	
	public Integer getEstadoOut() {
		return estadoOut;
	}

	public void setEstadoOut(Integer estadoOut) {
		this.estadoOut = estadoOut;
	}
	
	public String getMensajeOut() {
		return mensajeOut;
	}

	public void setMensajeOut(String mensajeOut) {
		this.mensajeOut = mensajeOut;
	}
	
	
	//--
	public String getPaginacion() {
		return paginacion;
	}

	public void setPaginacion(String paginacion) {
		this.paginacion = paginacion;
	}

	public Integer getPaginaInicial() {
		return paginaInicial;
	}

	public void setPaginaInicial(Integer paginaInicial) {
		this.paginaInicial = paginaInicial;
	}

	public Integer getPaginaFinal() {
		return paginaFinal;
	}

	public void setPaginaFinal(Integer paginaFinal) {
		this.paginaFinal = paginaFinal;
	}

	public Boolean getPrimero() {
		return primero;
	}

	public void setPrimero(Boolean primero) {
		this.primero = primero;
	}

	public Boolean getAnterior() {
		return anterior;
	}

	public void setAnterior(Boolean anterior) {
		this.anterior = anterior;
	}

	public Boolean getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(Boolean siguiente) {
		this.siguiente = siguiente;
	}

	public Boolean getUltimo() {
		return ultimo;
	}

	public void setUltimo(Boolean ultimo) {
		this.ultimo = ultimo;
	}

	public Integer getTotalRegistros() {
		return totalRegistros;
	}

	public void setTotalRegistros(Integer totalRegistros) {
		this.totalRegistros = totalRegistros;
	}

	public Integer getRegistroInicial() {
		return registroInicial;
	}

	public void setRegistroInicial(Integer registroInicial) {
		this.registroInicial = registroInicial;
	}

	public Integer getRegistroFinal() {
		return registroFinal;
	}

	public void setRegistroFinal(Integer registroFinal) {
		this.registroFinal = registroFinal;
	}	
	
		
}
