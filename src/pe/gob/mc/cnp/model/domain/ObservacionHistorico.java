package pe.gob.mc.cnp.model.domain;

import java.io.Serializable;
import java.util.Date;

public class ObservacionHistorico extends Base implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Integer  codigoObservacion;  
	private Integer  codigoFicha;
    private String descripcion; 
	private Date fechaObservacion;
	private Integer codigoUsuario;	
	private String usuarioObserva;
	private String estadoObservacion;
	
	
	/**
	 * General
	 */
	public ObservacionHistorico() {
		super();
	}
	
	/**
	 * Nuevo
	 */
	public ObservacionHistorico( Integer codigoFicha, String descripcion, Date fechaObservacion,  Integer codigoUsuario, String usuarioObserva) {
		super();	
		this.codigoFicha     = codigoFicha;
		this.descripcion         = descripcion;
		this.fechaObservacion         = fechaObservacion;
		this.codigoUsuario         = codigoUsuario;
		this.usuarioObserva         = usuarioObserva;
	}
	
	
	/**
	 * Modificación Descripción
	 */
	public ObservacionHistorico(Integer codigoObservacion,  String descripcion, Integer codigoUsuario) {
		super();	
		this.codigoObservacion     = codigoObservacion;
		this.descripcion     = descripcion;		
		this.codigoUsuario         = codigoUsuario;
	}
	
	
	/**
	 * Modificación Estado
	 */
	public ObservacionHistorico(Integer codigoObservacion,  String estadoObservacion ) {
		super();	
		this.codigoObservacion     = codigoObservacion;
		this.estadoObservacion     = estadoObservacion;		
	}

	public Integer getCodigoObservacion() {
		return codigoObservacion;
	}

	public void setCodigoObservacion(Integer codigoObservacion) {
		this.codigoObservacion = codigoObservacion;
	}

	public Integer getCodigoFicha() {
		return codigoFicha;
	}

	public void setCodigoFicha(Integer codigoFicha) {
		this.codigoFicha = codigoFicha;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaObservacion() {
		return fechaObservacion;
	}

	public void setFechaObservacion(Date fechaObservacion) {
		this.fechaObservacion = fechaObservacion;
	}

	public Integer getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(Integer codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	public String getUsuarioObserva() {
		return usuarioObserva;
	}

	public void setUsuarioObserva(String usuarioObserva) {
		this.usuarioObserva = usuarioObserva;
	}

	public String getEstadoObservacion() {
		return estadoObservacion;
	}

	public void setEstadoObservacion(String estadoObservacion) {
		this.estadoObservacion = estadoObservacion;
	}
	
	
	
	
	
	public String getDescripcionEstadoObservacion() {
		String d = "";
		
		if(this.estadoObservacion!=null){		
			switch((char)(this.estadoObservacion).charAt(0)) {
				case 'P':
					d = "Pendiente";
					break;
				case 'A':
					d = "Atendido";				
					break;
				/*case 'S':
					d = "Supervizado";				
					break;
				case 'O':
					d = "Observado";				
					break;
				case 'A':
					d = "Aprobado";				
					break;*/
				
			}		
		}
		
		return d;
	}

}
