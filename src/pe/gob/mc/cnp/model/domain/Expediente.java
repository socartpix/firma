package pe.gob.mc.cnp.model.domain;

import java.io.Serializable;
import java.util.Date;


public class Expediente extends Base implements Serializable {
	

	private static final long serialVersionUID = 1L;	
	  
	private Integer  codigoExpediente;  
	private Integer  codigoFicha;
    private String numeroExpediente; 
	private String nombresApellidos;
	private String anio;	
	private String tipoDocumento;
	private String numeroIdentidad;
	private String domicilio;
	private Integer  codigoPais;
	private Integer  cantidadSolicitada;
	private   Date  fechaSolicitud;
	
	/**
	 * General
	 */
	public Expediente() {
		super();
	}
	
	/**
	 * Nuevo
	 */
	public Expediente( Integer codigoFicha, String numeroExpediente, String nombresApellidos, String anio,  String tipoDocumento, String numeroIdentidad, String domicilio, Integer codigoPais, Integer cantidadSolicitada, Date  fechaSolicitud ) {
		super();	
		this.codigoFicha     = codigoFicha;
		this.numeroExpediente         = numeroExpediente;
		this.nombresApellidos         = nombresApellidos;
		this.anio         = anio;
		this.tipoDocumento         = tipoDocumento;
		this.numeroIdentidad         = numeroIdentidad;		
		this.domicilio         = domicilio;		
		this.codigoPais         = codigoPais;		
		this.cantidadSolicitada =cantidadSolicitada;
		this.fechaSolicitud =fechaSolicitud;
		
	}
	
	
	/**
	 * Modificación
	 */
	public Expediente(Integer codigoExpediente, Integer codigoFicha, String numeroExpediente, String nombresApellidos, String anio,  String tipoDocumento, String numeroIdentidad, String domicilio, Integer codigoPais, Integer cantidadSolicitada , Date  fechaSolicitud) {
		super();	
		this.codigoExpediente     = codigoExpediente;
		this.codigoFicha     = codigoFicha;
		this.numeroExpediente         = numeroExpediente;
		this.nombresApellidos         = nombresApellidos;
		this.anio         = anio;
		this.tipoDocumento         = tipoDocumento;
		this.numeroIdentidad         = numeroIdentidad;		
		this.domicilio         = domicilio;		
		this.codigoPais         = codigoPais;		
		this.cantidadSolicitada =cantidadSolicitada;
		this.fechaSolicitud =fechaSolicitud;
	}
	
	
	
	
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public Integer getCodigoFicha() {
		return codigoFicha;
	}
	public void setCodigoFicha(Integer codigoFicha) {
		this.codigoFicha = codigoFicha;
	}

	public Integer getCodigoExpediente() {
		return codigoExpediente;
	}

	public void setCodigoExpediente(Integer codigoExpediente) {
		this.codigoExpediente = codigoExpediente;
	}

	public String getNumeroExpediente() {
		return numeroExpediente;
	}

	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}

	public String getNombresApellidos() {
		return nombresApellidos;
	}

	public void setNombresApellidos(String nombresApellidos) {
		this.nombresApellidos = nombresApellidos;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroIdentidad() {
		return numeroIdentidad;
	}

	public void setNumeroIdentidad(String numeroIdentidad) {
		this.numeroIdentidad = numeroIdentidad;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public Integer getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(Integer codigoPais) {
		this.codigoPais = codigoPais;
	}

	public Integer getCantidadSolicitada() {
		return cantidadSolicitada;
	}

	public void setCantidadSolicitada(Integer cantidadSolicitada) {
		this.cantidadSolicitada = cantidadSolicitada;
	}
	
	
	
}
