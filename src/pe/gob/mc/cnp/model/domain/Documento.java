package pe.gob.mc.cnp.model.domain;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;

public class Documento  extends Base implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/*17-10-2019*/
	
	private Integer  codigoDocumento;  
	private Integer  codigoFicha;
    private String numeroDocumento; 
    private String prefijoDocumento;
    private String sufijoDocumento;
	private String siglasDocumento;
	private String referencia;
	private String nombreArchivo;
	private String rutaArchivo;
	/*17-10-2019*/
	private String nombreArchivo_firma;
	private String rutaArchivo_firma;
	
	private Integer codigoUsuarioRegistra;
	private String usuarioRegistra;
	
	@SuppressWarnings("unused")
	private String rutaAnexoDocumento;
	
	/*070519*/
	private String flagDocumento;
	/*23-10-2019*/
	private String nuIdArchivo;
	private String nuAnio;
	private String vcNroEmision;
	private String vcCodTipoDocumento;
	private String vcTipoDocumento;
	private String dtEmision;
	private String vcNroDocumento;
	private String vcDe_ruta_origen;
	private String vcNroExpediente;
	public String getVcNroEmision() {
		return vcNroEmision;
	}

	public void setVcNroEmision(String vcNroEmision) {
		this.vcNroEmision = vcNroEmision;
	}

	public String getVcNroExpediente() {
		return vcNroExpediente;
	}

	public void setVcNroExpediente(String vcNroExpediente) {
		this.vcNroExpediente = vcNroExpediente;
	}



	private byte[] vcContenidoDoc;
	
	/*23-10-2019*/
	
	
	
	
	/*28-10-2019*/
	
	
	
	public String getNuIdArchivo() {
		return nuIdArchivo;
	}

	public String getVcDe_ruta_origen() {
		return vcDe_ruta_origen;
	}

	public void setVcDe_ruta_origen(String vcDe_ruta_origen) {
		this.vcDe_ruta_origen = vcDe_ruta_origen;
	}

	public void setNuIdArchivo(String nuIdArchivo) {
		this.nuIdArchivo = nuIdArchivo;
	}

	public String getNuAnio() {
		return nuAnio;
	}

	public void setNuAnio(String nuAnio) {
		this.nuAnio = nuAnio;
	}

	public String getVcCodTipoDocumento() {
		return vcCodTipoDocumento;
	}

	public void setVcCodTipoDocumento(String vcCodTipoDocumento) {
		this.vcCodTipoDocumento = vcCodTipoDocumento;
	}

	public String getVcTipoDocumento() {
		return vcTipoDocumento;
	}

	public void setVcTipoDocumento(String vcTipoDocumento) {
		this.vcTipoDocumento = vcTipoDocumento;
	}

	public String getDtEmision() {
		return dtEmision;
	}

	public void setDtEmision(String dtEmision) {
		this.dtEmision = dtEmision;
	}

	public String getVcNroDocumento() {
		return vcNroDocumento;
	}

	public void setVcNroDocumento(String vcNroDocumento) {
		this.vcNroDocumento = vcNroDocumento;
	}

	public byte[] getVcContenidoDoc() {
		return vcContenidoDoc;
	}

	public void setVcContenidoDoc(byte[] vcContenidoDoc) {
		this.vcContenidoDoc = vcContenidoDoc;
	}

	public void setRutaAnexoDocumento(String rutaAnexoDocumento) {
		this.rutaAnexoDocumento = rutaAnexoDocumento;
	}

	/**
	 * General
	 */
	public Documento() {
		super();
	}
	
	/**
	 * Nuevo
	 */
	public Documento( Integer codigoFicha, String numeroDocumento, String prefijoDocumento,  String sufijoDocumento, String siglasDocumento, String referencia, String nombreArchivo,String rutaArchivo, Integer codigoUsuarioRegistra, String flagDocumento) {
		super();	
		this.codigoFicha        = codigoFicha;
		this.numeroDocumento     = numeroDocumento; 
		this.prefijoDocumento     = prefijoDocumento; 
		this.sufijoDocumento     = sufijoDocumento;
		this.siglasDocumento         = siglasDocumento;
		this.referencia         = referencia;
		this.nombreArchivo         = nombreArchivo;
		this.rutaArchivo         = rutaArchivo;
		this.codigoUsuarioRegistra=codigoUsuarioRegistra;
		this.flagDocumento        = flagDocumento;
	}
	
	
	/**
	 * Modificación 
	 */
	public Documento(Integer codigoDocumento, Integer codigoFicha, String numeroDocumento, String prefijoDocumento,  String sufijoDocumento, String siglasDocumento, String referencia, String nombreArchivo,String rutaArchivo, String flagDocumento) {
		super();	
		this.codigoDocumento     = codigoDocumento;
		this.codigoFicha     = codigoFicha;		
		this.numeroDocumento=numeroDocumento;
		this.prefijoDocumento=prefijoDocumento;
		this.sufijoDocumento=sufijoDocumento;
		this.siglasDocumento=siglasDocumento;
		this.referencia=referencia;
		this.nombreArchivo=nombreArchivo;
		this.rutaArchivo=rutaArchivo;
		this.flagDocumento        = flagDocumento;
	}
	
	
	public String getFlagDocumento() {
		return flagDocumento;
	}

	public void setFlagDocumento(String flagDocumento) {
		this.flagDocumento = flagDocumento;
	}

	public Integer getCodigoUsuarioRegistra() {
		return codigoUsuarioRegistra;
	}

	public void setCodigoUsuarioRegistra(Integer codigoUsuarioRegistra) {
		this.codigoUsuarioRegistra = codigoUsuarioRegistra;
	}

	public String getUsuarioRegistra() {
		return usuarioRegistra;
	}

	public void setUsuarioRegistra(String usuarioRegistra) {
		this.usuarioRegistra = usuarioRegistra;
	}

	public Integer getCodigoDocumento() {
		return codigoDocumento;
	}

	public void setCodigoDocumento(Integer codigoDocumento) {
		this.codigoDocumento = codigoDocumento;
	}

	public Integer getCodigoFicha() {
		return codigoFicha;
	}

	public void setCodigoFicha(Integer codigoFicha) {
		this.codigoFicha = codigoFicha;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	
	
	public String getNumeroDocumentoFormato() {
		
		
		String sigla="";
		String letra="";
		System.out.println("Flag");
		System.out.println(flagDocumento);
		/*25-10-2019 este if no existe en el original*/
		if(flagDocumento!=null){
		/*25-10-2019 este if no existe en el original*/			
			
			if(flagDocumento.equals("D")){
				 sigla=(siglasDocumento!=null)?("DRE-"+siglasDocumento+"/MC"):("DRE/MC");
				 letra="D";
			}else{
				 sigla=(siglasDocumento!=null)?(siglasDocumento+"-DRE-DGDP-VMPCIC/MC"):("DRE-DGDP/MC");
			}
			
		/*25-10-2019 este if no existe en el original*/		
		}
		/*25-10-2019 este if no existe en el original*/		
		System.out.println(numeroDocumento.charAt(0));
		System.out.println(siglasDocumento+" "+numeroDocumento+" "+sufijoDocumento+" sigla:"+sigla);
		
		return letra+numeroDocumento+"-"+sufijoDocumento+"-"+sigla ;
	}
	
	public String getPrefijoDocumento() {
		return prefijoDocumento;
	}

	public void setPrefijoDocumento(String prefijoDocumento) {
		this.prefijoDocumento = prefijoDocumento;
	}

	public String getSufijoDocumento() {
		return sufijoDocumento;
	}

	public void setSufijoDocumento(String sufijoDocumento) {
		this.sufijoDocumento = sufijoDocumento;
	}

	public String getSiglasDocumento() {
		return siglasDocumento;
	}

	public void setSiglasDocumento(String siglasDocumento) {
		this.siglasDocumento = siglasDocumento;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getRutaArchivo() {
		return rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}
	
	
	public String getRutaAnexoDocumento() {
		String ruta="";
		
		if(nombreArchivo!=null){
			
			ruta= rutaArchivo+nombreArchivo;
			
		}
		
		return ruta;
	}
	
	public String getNombreAnexoDocumentoSGD() {
		String nombrearchivo="";
		
	
			
			nombrearchivo= this.vcNroEmision+"-"+this.nuAnio+"-"+"-DRE-DGDP-VMPCIC/MC";
		

		
		return nombrearchivo;
	}
	
	
	
	/*17-10-2019*/
	public String getNombreArchivo_firma() {
		return nombreArchivo_firma;
	}

	public void setNombreArchivo_firma(String nombreArchivo_firma) {
		this.nombreArchivo_firma = nombreArchivo_firma;
	}

	public String getRutaArchivo_firma() {
		return rutaArchivo_firma;
	}

	public void setRutaArchivo_firma(String rutaArchivo_firma) {
		this.rutaArchivo_firma = rutaArchivo_firma;
	}
	/*17-10-2019*/



	private boolean flagSeleccion;
	private String principal;
	
	
	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public boolean isFlagSeleccion() {
		
		boolean seleccion =false;
		
		if(this.principal!=null){		
			switch((char)(this.principal).charAt(0)) {
				case 'S':
					seleccion=true;
					break;
				case 'N':
					seleccion=false;				
					break;				
			}		
		}
		
		return seleccion;
	}



	public void setFlagSeleccion(boolean flagSeleccion) {
		this.flagSeleccion = flagSeleccion;
	}
	
    public void doDownload() {
    		
    	String vcDe_ruta_origen=this.vcDe_ruta_origen;
    	String vcDe_ruta_origen_ext=FilenameUtils.getExtension(vcDe_ruta_origen);

        byte[] csvData =this.vcContenidoDoc;

        FacesContext context = FacesContext.getCurrentInstance();

        HttpServletResponse response = (HttpServletResponse)           FacesContext.getCurrentInstance().getExternalContext()

                .getResponse();

        response.setHeader("Content-disposition","attachment; filename= "+vcDe_ruta_origen);

        response.setContentLength(csvData.length);

      //  response.setContentType(selectedUploadItem.getContentType());

        try {

            response.getOutputStream().write(csvData);

            response.getOutputStream().flush();

            response.getOutputStream().close();

            context.responseComplete();

        } catch (IOException e) {

            e.printStackTrace();

        }

     }

	

}
