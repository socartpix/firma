 	package pe.gob.mc.cnp.model.domain;

import java.io.Serializable;
import java.util.Date;


public class Ficha extends Expediente implements Serializable {
	

	private static final long serialVersionUID = 1L;
	
	
	private Integer  codigoFicha;
	private Integer  cantidadAutorizada;
	private Date  fechaExpedicion;
	//private Date  fechaSolicitud;
	private Integer numeroCertificado;
    private String observacion; 
	private String usuarioRegistrador;
	private Integer codigoUsuarioRegistrador;
	private Integer codigoUsuarioAdministrativo;
	private String usuarioEvaluador;
	private Integer codigoUsuarioEvaluador;
	private String usuarioDirector;
	private Integer codigoUsuarioDirector;
	private String estadoFicha;
	private Date  fechaEvaluacion;
	private Date  fechaAprobacion;
	private Date  fechaAsignacionRegistrador;
	private String numeroFichaTexto;	
	private String  tipoPersona;
	private String  nacionalidad;
	private String descripcionInicial;
	private String descripcionFinal;
	private Integer codigoPerfil;
	private String vigente;

	
	public String getVigente() {
		return vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

	public Integer getCodigoPerfil() {
		return codigoPerfil;
	}

	public void setCodigoPerfil(Integer codigoPerfil) {
		this.codigoPerfil = codigoPerfil;
	}

	/**
	 * General
	 */
	public Ficha() {
		super();
	}
	
	/**
	 * Nuevo
	 */
	public Ficha(Integer cantidadAutorizada,/* Date fechaSolicitud, */String observacion, Integer codigoUsuarioRegistrador,  String usuarioRegistrador, String tipoPersona  , String nacionalidad , Integer codigoUsuarioAdministrativo) {
		super();	
		this.cantidadAutorizada     = cantidadAutorizada;
		//this.fechaSolicitud         = fechaSolicitud;
		this.observacion         = observacion;
		this.codigoUsuarioRegistrador         = codigoUsuarioRegistrador;
		this.usuarioRegistrador         = usuarioRegistrador;
		this.tipoPersona         = tipoPersona;
		this.nacionalidad         = nacionalidad;
		this.codigoUsuarioAdministrativo         = codigoUsuarioAdministrativo; 
	}
	
	
	/**
	 * Modificación
	 */
	public Ficha(Integer codigoFicha,Integer cantidadAutorizada, /*Date fechaSolicitud,*/ String observacion, Integer codigoUsuarioRegistrador,  String usuarioRegistrador , String tipoPersona  , String nacionalidad ) {
		super();	
		this.codigoFicha         = codigoFicha;
		this.cantidadAutorizada     = cantidadAutorizada;
		//this.fechaSolicitud         = fechaSolicitud;
		this.observacion         = observacion;
		this.codigoUsuarioRegistrador         = codigoUsuarioRegistrador;
		this.usuarioRegistrador         = usuarioRegistrador;	
		this.tipoPersona         = tipoPersona;
		this.nacionalidad         = nacionalidad;

		
	}
	/**
	 * Modificación Observar
	 */
	public Ficha(Integer codigoFicha) {
		super();	
		this.codigoFicha         = codigoFicha;
	
	}

	/**
	 * Modificación Asignación
	 */
	public Ficha(Integer codigoFicha, Integer codigoUsuarioRegistrador,  String usuarioRegistrador ) {
		super();	
		this.codigoFicha         = codigoFicha;
		this.codigoUsuarioRegistrador         = codigoUsuarioRegistrador;
		this.usuarioRegistrador         = usuarioRegistrador;
	//	this.fechaAsignacionRegistrador         = fechaAsignacionRegistrador;
		
	}

	
	
	
	public String getDescripcionInicial() {
		return descripcionInicial;
	}

	public void setDescripcionInicial(String descripcionInicial) {
		this.descripcionInicial = descripcionInicial;
	}

	public String getDescripcionFinal() {
		return descripcionFinal;
	}

	public void setDescripcionFinal(String descripcionFinal) {
		this.descripcionFinal = descripcionFinal;
	}

	public Integer getCodigoUsuarioAdministrativo() {
		return codigoUsuarioAdministrativo;
	}

	public void setCodigoUsuarioAdministrativo(Integer codigoUsuarioAdministrativo) {
		this.codigoUsuarioAdministrativo = codigoUsuarioAdministrativo;
	}

	public Date getFechaAsignacionRegistrador() {
		return fechaAsignacionRegistrador;
	}

	public void setFechaAsignacionRegistrador(Date fechaAsignacionRegistrador) {
		this.fechaAsignacionRegistrador = fechaAsignacionRegistrador;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public Date getFechaEvaluacion() {
		return fechaEvaluacion;
	}

	public void setFechaEvaluacion(Date fechaEvaluacion) {
		this.fechaEvaluacion = fechaEvaluacion;
	}

	public Date getFechaAprobacion() {
		return fechaAprobacion;
	}

	public void setFechaAprobacion(Date fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}

	public Integer getCodigoFicha() {
		return codigoFicha;
	}
	public void setCodigoFicha(Integer codigoFicha) {
		this.codigoFicha = codigoFicha;
	}
	public Integer getCantidadAutorizada() {
		return cantidadAutorizada;
	}
	public void setCantidadAutorizada(Integer cantidadAutorizada) {
		this.cantidadAutorizada = cantidadAutorizada;
	}
	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}
	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}
	/*public Date getFechaSolicitud() {
		return fechaSolicitud;
	}
	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}*/
	public Integer getNumeroCertificado() {
		return numeroCertificado;
	}
	public void setNumeroCertificado(Integer numeroCertificado) {
		this.numeroCertificado = numeroCertificado;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getUsuarioRegistrador() {
		return usuarioRegistrador;
	}
	public void setUsuarioRegistrador(String usuarioRegistrador) {
		this.usuarioRegistrador = usuarioRegistrador;
	}
	public Integer getCodigoUsuarioRegistrador() {
		return codigoUsuarioRegistrador;
	}
	public void setCodigoUsuarioRegistrador(Integer codigoUsuarioRegistrador) {
		this.codigoUsuarioRegistrador = codigoUsuarioRegistrador;
	}
	public String getUsuarioEvaluador() {
		return usuarioEvaluador;
	}
	public void setUsuarioEvaluador(String usuarioEvaluador) {
		this.usuarioEvaluador = usuarioEvaluador;
	}
	public Integer getCodigoUsuarioEvaluador() {
		return codigoUsuarioEvaluador;
	}
	public void setCodigoUsuarioEvaluador(Integer codigoUsuarioEvaluador) {
		this.codigoUsuarioEvaluador = codigoUsuarioEvaluador;
	}
	public String getUsuarioDirector() {
		return usuarioDirector;
	}
	public void setUsuarioDirector(String usuarioDirector) {
		this.usuarioDirector = usuarioDirector;
	}
	public Integer getCodigoUsuarioDirector() {
		return codigoUsuarioDirector;
	}
	public void setCodigoUsuarioDirector(Integer codigoUsuarioDirector) {
		this.codigoUsuarioDirector = codigoUsuarioDirector;
	}
	public String getEstadoFicha() {
		return estadoFicha;
	}
	public void setEstadoFicha(String estadoFicha) {
		this.estadoFicha = estadoFicha;
	}	
	public String getNumeroCertificadoFormato() {
		  String d = "";
	      if(this.numeroCertificado!=null){
	    	  d = String.format("%06d", this.numeroCertificado);
		  }
		  return d;
		}	
	
	
	public String getDescripcionEstadoFicha() {
		String d = "";
		
		if(this.estadoFicha!=null){		
			switch((char)(this.estadoFicha).charAt(0)) {
				case 'C':
					d = "Nuevo";
					break;
				case 'T':
					d = "Asignado a Técnico";				
					break;
				case 'E':
					d = "Enviado a Supervisor";				
					break;
				case 'S':
					d = "Enviado a Director";				
					break;
				case 'O':
					d = "Observado";				
					break;
				case 'A':
					d = "Aprobado";				
					break;					
				case 'D':
					d = "Denegado";				
					break;
				case 'B':
					d = "Abandono";				
					break;
			}		
		}
		
		return d;
	}
		
	
	private String motivoNoAtencion;
	
	private Date fechaNoAtencion;
	
	
	
	public Date getFechaNoAtencion() {
		return fechaNoAtencion;
	}

	public void setFechaNoAtencion(Date fechaNoAtencion) {
		this.fechaNoAtencion = fechaNoAtencion;
	}

	public String getMotivoNoAtencion() {
		return motivoNoAtencion;
	}

	public void setMotivoNoAtencion(String motivoNoAtencion) {
		this.motivoNoAtencion = motivoNoAtencion;
	}

	public String getDescripcionNoAtencion() {
		String d = "";
		
		if(this.motivoNoAtencion!=null){		
			switch((char)(this.motivoNoAtencion).charAt(0)) {
				case 'A':
					d = "Abandono del trámite";
					break;
				case 'D':
					d = "Denegación de todos los bienes solicitados para exportación";				
					break;				
			}		
		}
		
		return d;
	}
	
	
	private String nombreArchivo;
	private String rutaArchivo;	
    private String rutaCertificadoDocumento;

    private String rutaCertificadoDocumentoFirma;
	// 16-10-2019
	private String nombreArchivo_firma;
	private String rutaArchivo_firma;
	private String codcert;
	
	public String getCodcert() {
		return codcert;
	}

	public void setCodcert(String codcert) {
		this.codcert = codcert;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getRutaArchivo() {
		return rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}

	public String getRutaCertificadoDocumento() {
		String ruta="";
		
		if(nombreArchivo!=null){
			
			ruta= rutaArchivo+nombreArchivo;
			
		}
		
		return ruta;
	}


	public String getRutaCertificadoDocumentoFirma() {
		String ruta="";
		
		if(nombreArchivo_firma!=null){
			
			ruta= rutaArchivo_firma+nombreArchivo_firma;
			
		}
		
		return ruta;
	}
	
	
	public String getNombreArchivo_firma() {
		return nombreArchivo_firma;
	}

	public void setNombreArchivo_firma(String nombreArchivo_firma) {
		this.nombreArchivo_firma = nombreArchivo_firma;
	}

	public String getRutaArchivo_firma() {
		return rutaArchivo_firma;
	}

	public void setRutaArchivo_firma(String rutaArchivo_firma) {
		this.rutaArchivo_firma = rutaArchivo_firma;
	}
	
	//17-10-2019
	
	
		
}
