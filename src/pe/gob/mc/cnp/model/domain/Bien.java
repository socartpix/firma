package pe.gob.mc.cnp.model.domain;

import java.io.Serializable;

public class Bien  extends Base implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Integer  codigoBien;  
	private Integer  codigoFicha;
    private String descripcion;
    private String conjunto;
	private String nombreFolder;
	private String nombreArchivo;
	
	private String rutaImagenBien;
	
	/**
	 * General
	 */
	public Bien() {
		super();
	}
	
	/**
	 * Nuevo
	 */
	public Bien( Integer codigoFicha, String descripcion, String conjunto,  String nombreFolder, String nombreArchivo) {
		super();	
		this.codigoFicha     = codigoFicha;
		this.descripcion     = descripcion; 
		this.conjunto     = conjunto; 
		this.nombreFolder     = nombreFolder;
		this.nombreArchivo         = nombreArchivo;
	}
	
	
	/**
	 * Modificación 
	 */
	public Bien(Integer codigoBien, Integer codigoFicha, String descripcion, String conjunto,  String nombreFolder, String nombreArchivo) {
		super();	
		this.codigoBien=codigoBien;
		this.codigoFicha     = codigoFicha;
		this.descripcion     = descripcion; 
		this.conjunto     = conjunto; 
		this.nombreFolder     = nombreFolder;
		this.nombreArchivo         = nombreArchivo;
	}

	public Integer getCodigoBien() {
		return codigoBien;
	}

	public void setCodigoBien(Integer codigoBien) {
		this.codigoBien = codigoBien;
	}

	public Integer getCodigoFicha() {
		return codigoFicha;
	}

	public void setCodigoFicha(Integer codigoFicha) {
		this.codigoFicha = codigoFicha;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getConjunto() {
		return conjunto;
	}

	public void setConjunto(String conjunto) {
		this.conjunto = conjunto;
	}

	public String getNombreFolder() {
		return nombreFolder;
	}

	public void setNombreFolder(String nombreFolder) {
		this.nombreFolder = nombreFolder;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	

	public String getRutaImagenBien() {
		String ruta="";
		
		if(nombreArchivo!=null){
			
			ruta= nombreFolder+nombreArchivo;
			
		}
		
		return ruta;
	}

	
	
}
