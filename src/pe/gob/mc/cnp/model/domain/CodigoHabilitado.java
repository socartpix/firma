package pe.gob.mc.cnp.model.domain;

import java.io.Serializable;

public class CodigoHabilitado  extends Base implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Integer  codigoHabilitado;  
	private Integer  codigoFicha;
    private Integer numeroBien;
    private String utilizado;
    private String numeroExpediente;
	
	/**
	 * General
	 */
	public CodigoHabilitado() {
		super();
	}
	
	/**
	 * Nuevo
	 */
	public CodigoHabilitado( Integer codigoFicha, Integer numeroBien) {
		super();	
		this.codigoFicha     = codigoFicha;
		this.numeroBien     = numeroBien; 
	}
	
	
	/**
	 * Modificación Estado
	 */
	public CodigoHabilitado(Integer codigoFicha) {
		super();	
		this.codigoFicha     = codigoFicha;
	}


	
	
	
	public String getNumeroExpediente() {
		return numeroExpediente;
	}

	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}

	public Integer getCodigoHabilitado() {
		return codigoHabilitado;
	}

	public void setCodigoHabilitado(Integer codigoHabilitado) {
		this.codigoHabilitado = codigoHabilitado;
	}

	public Integer getCodigoFicha() {
		return codigoFicha;
	}

	public void setCodigoFicha(Integer codigoFicha) {
		this.codigoFicha = codigoFicha;
	}

	public Integer getNumeroBien() {
		return numeroBien;
	}

	public void setNumeroBien(Integer numeroBien) {
		this.numeroBien = numeroBien;
	}

	public String getUtilizado() {
		return utilizado;
	}

	public void setUtilizado(String utilizado) {
		this.utilizado = utilizado;
	}
	
	
	
	
}
