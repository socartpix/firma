package pe.gob.mc.cnp.model.domain;

import java.io.Serializable;

public class DetalleBien  extends Base implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Integer  codigoDetalleBien;  
	private Integer  codigoBien;
    private Integer numeroBien;
	
	
	/**
	 * General
	 */
	public DetalleBien() {
		super();
	}
	
	/**
	 * Nuevo
	 */
	public DetalleBien( Integer codigoBien, Integer numeroBien) {
		super();	
		this.codigoBien     = codigoBien;
		this.numeroBien     = numeroBien; 
	}
	
	
	/**
	 * Modificación Estado
	 */
	public DetalleBien(Integer codigoBien) {
		super();	
		this.codigoBien     = codigoBien;
	}

	public Integer getCodigoDetalleBien() {
		return codigoDetalleBien;
	}

	public void setCodigoDetalleBien(Integer codigoDetalleBien) {
		this.codigoDetalleBien = codigoDetalleBien;
	}

	public Integer getCodigoBien() {
		return codigoBien;
	}

	public void setCodigoBien(Integer codigoBien) {
		this.codigoBien = codigoBien;
	}

	public Integer getNumeroBien() {
		return numeroBien;
	}

	public void setNumeroBien(Integer numeroBien) {
		this.numeroBien = numeroBien;
	}
	
	
	
	
}
