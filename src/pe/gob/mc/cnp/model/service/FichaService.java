package pe.gob.mc.cnp.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.mc.cnp.model.dao.iface.FichaMapper;
import pe.gob.mc.cnp.model.domain.CodigoHabilitado;
import pe.gob.mc.cnp.model.domain.DetalleBien;
import pe.gob.mc.cnp.model.domain.Expediente;
import pe.gob.mc.cnp.model.domain.Ficha;
import pe.gob.mc.cnp.model.domain.ObservacionHistorico;
import pe.gob.mc.cnp.model.factory.Factory; 
import pe.gob.mc.msag.model.domain.Usuario;
import pe.gob.mc.msag.model.factory.FactoryBase;

@Service
public class FichaService {
	
	@Autowired
	private FichaMapper fichaMapper;
	
	public void setFichaMapper(FichaMapper oFichaMapper) {
		this.fichaMapper = oFichaMapper;
	}
		
	public Boolean buscarFichaXCriterio(Map<String, Object> parameter) {		
		return fichaMapper.buscarFichaXCriterio(parameter);
	}
	
	public Boolean leerFicha(Map<String, Object> parameter) {		
		return fichaMapper.leerFicha(parameter);
	}
	
	public Boolean buscarUsuariosRegistradoresFicha(Map<String, Object> parameter) {		
		return fichaMapper.buscarUsuariosRegistradoresFicha(parameter);
	}	
	public Boolean buscarHorarios(Map<String, Object> parameter) {		
		return fichaMapper.buscarHorarios(parameter);
	}	
	
	public Boolean buscarCodigoBienDisponibleXFicha(Map<String, Object> parameter) {		
		return fichaMapper.buscarCodigoBienDisponibleXFicha(parameter);
	}	
	
	public Boolean buscarCodigoBienDisponibleXCodigoFicha(Map<String, Object> parameter) {		
		return fichaMapper.buscarCodigoBienDisponibleXCodigoFicha(parameter);
	}	
	
	public Boolean buscarCodigoBienDisponibleParaAsignar(Map<String, Object> parameter) {		
		return fichaMapper.buscarCodigoBienDisponibleParaAsignar(parameter);
	}	
	
		
	@Transactional(rollbackFor = Exception.class)
	public void insertarFicha(Ficha oFicha, Expediente oExpediente , Integer numeroSesion)   throws Exception{
		
		//Inserta
		oFicha.setSesionCreacion(numeroSesion);
		oFicha.setMensajeOut(null);
		fichaMapper.insertarFicha(oFicha);
		 if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());

		 
		 oExpediente.setCodigoFicha(oFicha.getCodigoFicha());
		 Factory.getInstance().getExpedienteBO().insertarExpediente(oExpediente, numeroSesion);	
		 
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void modificarFicha(Ficha oFicha, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setMensajeOut(null);
		fichaMapper.modificarFicha(oFicha);
		if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());
	    
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void modificarFichaAsignacion(Ficha oFicha, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setMensajeOut(null);
		fichaMapper.modificarFichaAsignacion(oFicha);
		if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());
	    
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void generarNumeroCertificado(Ficha oFicha, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setMensajeOut(null);
		fichaMapper.generarNumeroCertificado(oFicha);
		if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());
	    
	}	
	
	
	@Transactional(rollbackFor = Exception.class)
	public void enviarCertificadoRegistrador(Ficha oFicha, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setMensajeOut(null);
		
		
		System.out.println("--> oFicha "+oFicha);
		
	  if(oFicha.getEstadoFicha().equals("T")  &&  oFicha.getNumeroCertificado()==null ){
			fichaMapper.generarNumeroCertificado(oFicha);
			if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());
	     }
	    	oFicha.setEstadoFicha("E");	    	 
	 		fichaMapper.cambiarEstadoFicha(oFicha);
			if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());    
	   //  }
		
	
			//atender observaciones pendientes
			Map<String, Object> parameter = new HashMap<String, Object>();
			parameter.put("codigoFicha",oFicha.getCodigoFicha());
			List<ObservacionHistorico> listObservacionHistorico= new ArrayList<ObservacionHistorico>(); 
			System.out.println("parameter--> "+parameter);
			Factory.getInstance().getObservacionHistoricoBO().buscarObservacionHistoricoXFicha(parameter);
			
			listObservacionHistorico=(List<ObservacionHistorico>)parameter.get("resultList");
			
			System.out.println("listObservacionHistorico.size() "+listObservacionHistorico.size());
			if(listObservacionHistorico.size()>0){
					
				for( ObservacionHistorico obs    : listObservacionHistorico)    {
					System.out.println("obs-> "+obs);
					 if( obs.getEstadoObservacion().equals("P")  ){
						 ObservacionHistorico oObservacionHistorico= new ObservacionHistorico(obs.getCodigoObservacion(), "A");
						 System.out.println("oObservacionHistorico "+oObservacionHistorico);
						 Factory.getInstance().getObservacionHistoricoBO().modificarEstadoObservacionHistorico(oObservacionHistorico, numeroSesion);
					 }
				}
			}
	}	
	
	@Transactional(rollbackFor = Exception.class)
	public void enviarCertificadoSupervisor(Ficha oFicha, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setMensajeOut(null);
	    	oFicha.setEstadoFicha("S");	    	 
	 		fichaMapper.cambiarEstadoFicha(oFicha);
			if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());    
	     
			System.out.println("oFicha.getMotivoNoAtencion() "+oFicha.getMotivoNoAtencion());
			
			if(oFicha.getMotivoNoAtencion()==null  ){
				
				oFicha.setSesionEdicion(numeroSesion);
				oFicha.setMensajeOut(null);	    	 
			 		fichaMapper.supervizarFicha(oFicha);
					if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());    
			    	
			}
			
			     
	             
	}	
	
	@Transactional(rollbackFor = Exception.class)
	public void enviarADirectorDenegarCertificado(Ficha oFicha, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setMensajeOut(null);
//denegacion
		if(oFicha.getMotivoNoAtencion().equals("D")	){
		    oFicha.setEstadoFicha("D");
		}else{//abandono
			oFicha.setEstadoFicha("B");
		}
		
	    	System.out.println("estado: "+oFicha.getEstadoFicha() );	    	 
	 		fichaMapper.cambiarEstadoFicha(oFicha);
			if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());    
	     
		         //liberar nro de bienes
			
			   CodigoHabilitado	oCodigoHabilitado = new CodigoHabilitado();
			   oCodigoHabilitado.setCodigoFicha(oFicha.getCodigoFicha());
				oCodigoHabilitado.setActivo("N");
				oCodigoHabilitado.setSesionEdicion(numeroSesion);
				oCodigoHabilitado.setMensajeOut(null);
				Factory.getInstance().getFichaBO().eliminarTodaAsignacionXFicha(oCodigoHabilitado, numeroSesion);
				if (oCodigoHabilitado.getMensajeOut() != null) throw new Exception(oCodigoHabilitado.getMensajeOut());  
	}	
	
	
	@Transactional(rollbackFor = Exception.class)
	public void observarCertificado(Ficha oFicha, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setMensajeOut(null);
	    	oFicha.setEstadoFicha("O");	    	 
	 		fichaMapper.cambiarEstadoFicha(oFicha);
			if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());    
	     
		
	
	}	
	
	@Transactional(rollbackFor = Exception.class)
	public void aprobarCertificado(Ficha oFicha, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setMensajeOut(null);
	    	oFicha.setEstadoFicha("A");	    	 
	 		fichaMapper.cambiarEstadoFicha(oFicha);
			if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());    
	     
			oFicha.setSesionEdicion(numeroSesion);
			oFicha.setMensajeOut(null);	    	 
		 		fichaMapper.aprobarFicha(oFicha);
				if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());    
	
				
				///////
				/*oFicha.setSesionEdicion(numeroSesion);
				oFicha.setMensajeOut(null);	 
				fichaMapper.generarNumeroCertificado(oFicha);
				if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());*/
				///////
				
				//Habilitar los nro de bienes no utilizados
				
				  Map<String, Object> parameter = new HashMap<String, Object>();
					parameter.put("codigoFicha",oFicha.getCodigoFicha());
				   Factory.getInstance().getFichaBO().buscarCodigoBienDisponibleXCodigoFicha(parameter);
				  List<CodigoHabilitado> listadoCodigos= (List<CodigoHabilitado>)parameter.get("resultList"); 
				  
				  CodigoHabilitado oCodigoHabilitado=null;
				  for(CodigoHabilitado ch : listadoCodigos){
					  if(ch.getUtilizado().equals("N")){
						  //procederemos a anular el numero para que quede habilitado y se pueda utilizar
						  oCodigoHabilitado= new CodigoHabilitado();
						  oCodigoHabilitado.setCodigoHabilitado(ch.getCodigoHabilitado());
						  oCodigoHabilitado.setActivo("N");
							Factory.getInstance().getFichaBO().eliminarCodigoHabilitadoPorCodigo(oCodigoHabilitado, numeroSesion);
							if (oCodigoHabilitado.getMensajeOut() != null) throw new Exception(oCodigoHabilitado.getMensajeOut()); 
					  }
				  }
				
	}	
	
	@Transactional(rollbackFor = Exception.class)
	public void levantarObservacionCertificado(Ficha oFicha, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setMensajeOut(null);
	    	oFicha.setEstadoFicha("C");	    	 
	 		fichaMapper.cambiarEstadoFicha(oFicha);
			if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());    
	     
		
	
	}	
	
	@Transactional(rollbackFor = Exception.class)
    public void eliminarFicha(Ficha oFicha, Integer numeroSesion)   throws Exception{
		
		//Actualiza
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setActivo("N");
		oFicha.setMensajeOut(null);
	    fichaMapper.modificarFichaActivo(oFicha);
	    if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());
	
		
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void supervizarFicha(Ficha oFicha, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setMensajeOut(null);    	 
	 		fichaMapper.supervizarFicha(oFicha);
			if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());    
	     
	}	
	 
	@Transactional(rollbackFor = Exception.class)
	public void aprobarFicha(Ficha oFicha, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setMensajeOut(null);    	 
	 		fichaMapper.aprobarFicha(oFicha);
			if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());    
	     
	}	
	
	
	@Transactional(rollbackFor = Exception.class)
	public void actualizarObservacionFicha(Ficha oFicha, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setMensajeOut(null);    	 
	 		fichaMapper.actualizarObservacionFicha(oFicha);
			if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());    
	     
	}	
	
	
	@Transactional(rollbackFor = Exception.class)
	public void actualizarModoUtilizacionDelNumeroBien(CodigoHabilitado oCodigoHabilitado, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oCodigoHabilitado.setSesionEdicion(numeroSesion);
		oCodigoHabilitado.setMensajeOut(null);    	 
	 		fichaMapper.actualizarModoUtilizacionDelNumeroBien(oCodigoHabilitado);
			if (oCodigoHabilitado.getMensajeOut() != null) throw new Exception(oCodigoHabilitado.getMensajeOut());    
	     
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void insertarCodigoHabilitado(CodigoHabilitado oCodigoHabilitado, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oCodigoHabilitado.setSesionEdicion(numeroSesion);
		oCodigoHabilitado.setMensajeOut(null);    	 
	 		fichaMapper.insertarCodigoHabilitado(oCodigoHabilitado);
			if (oCodigoHabilitado.getMensajeOut() != null) throw new Exception(oCodigoHabilitado.getMensajeOut());    
	     
	}	
	
	
	
	@Transactional(rollbackFor = Exception.class)
	public void eliminarCodigoHabilitadoPorCodigo(CodigoHabilitado oCodigoHabilitado, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oCodigoHabilitado.setSesionEdicion(numeroSesion);
		oCodigoHabilitado.setActivo("N");
		oCodigoHabilitado.setMensajeOut(null);    	 
	 		fichaMapper.eliminarCodigoHabilitadoPorCodigo(oCodigoHabilitado);
			if (oCodigoHabilitado.getMensajeOut() != null) throw new Exception(oCodigoHabilitado.getMensajeOut());    
	     
	}	

	@Transactional(rollbackFor = Exception.class)
	public void eliminarTodaAsignacionXFicha(CodigoHabilitado oCodigoHabilitado, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oCodigoHabilitado.setSesionEdicion(numeroSesion);
		oCodigoHabilitado.setActivo("N");
		oCodigoHabilitado.setMensajeOut(null);    	 
	 		fichaMapper.eliminarTodaAsignacionXFicha(oCodigoHabilitado);
			if (oCodigoHabilitado.getMensajeOut() != null) throw new Exception(oCodigoHabilitado.getMensajeOut());    
	     
	}		
	
	
	@Transactional(rollbackFor = Exception.class)
	public void insertarListadoCodigoHabilitado( Integer codigoFicha, Integer codigoUsuarioRegistrador,  List<Integer> listaNumeros /*DetalleBien oDetalleBien*/, Integer numeroSesion)   throws Exception{
		
		//Inserta		
		CodigoHabilitado	oCodigoHabilitado= null;			
		System.out.println("Hara separacion de: "+listaNumeros.size());
		for(Integer num:  listaNumeros){
		  oCodigoHabilitado =  new CodigoHabilitado(codigoFicha, num);			
		  oCodigoHabilitado.setSesionCreacion(numeroSesion);
		  oCodigoHabilitado.setMensajeOut(null);
		  System.out.println("oCodigoHabilitado1 "+oCodigoHabilitado);
		  fichaMapper.insertarCodigoHabilitado(oCodigoHabilitado);
		  System.out.println("oCodigoHabilitado2 "+oCodigoHabilitado);
		  if (oCodigoHabilitado.getMensajeOut() != null) throw new Exception(oCodigoHabilitado.getMensajeOut());

		}
		
		
		Map<String, Object> par = new HashMap<String, Object>();
		par.put("codigoUsuario",codigoUsuarioRegistrador);
		FactoryBase.getInstance().getUsuarioBO().leerUsuarioPorCodigo(par);
		List<Usuario>lista= (List<Usuario>)par.get("resultList"); 	
		
		Ficha oFicha= new Ficha(codigoFicha,codigoUsuarioRegistrador, lista.get(0).getLogin() );
		
		Factory.getInstance().getFichaBO().modificarFichaAsignacion(oFicha, numeroSesion);
		
		//cambiar de estado a Asignado a tecnico "T"  solo si esta en estado C  Nuevo
		
		 par = new HashMap<String, Object>();
		par.put("codigoFicha",codigoFicha);
		Factory.getInstance().getFichaBO().leerFicha(par);
		List<Ficha>list= (List<Ficha>)par.get("resultList"); 	
		
		Ficha oFichaObj = list.get(0);
		
		
		if(oFichaObj.getEstadoFicha().equals("C")){
			//codigoFichad
			oFichaObj.setEstadoFicha("T");
			oFichaObj.setSesionEdicion(numeroSesion);
			oFichaObj.setMensajeOut(null);
			fichaMapper.cambiarEstadoFicha(oFichaObj);
		 if (oFichaObj.getMensajeOut() != null) throw new Exception(oFichaObj.getMensajeOut());
	
		}
		
		//Fin
		
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void modificarCantidadAutorizada(Ficha oFicha, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setMensajeOut(null);
		fichaMapper.modificarCantidadAutorizada(oFicha);
		if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());
	    
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void modificarRegistroNoAtencion(Ficha oFicha, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oFicha.setSesionEdicion(numeroSesion);
		oFicha.setMensajeOut(null);
		fichaMapper.modificarRegistroNoAtencion(oFicha);
		if (oFicha.getMensajeOut() != null) throw new Exception(oFicha.getMensajeOut());
	    
	}
	
	public Boolean buscarConsultaFichaPorPerfil(Map<String, Object> parameter) {		
		return fichaMapper.buscarConsultaFichaPorPerfil(parameter);
	} 
	
}