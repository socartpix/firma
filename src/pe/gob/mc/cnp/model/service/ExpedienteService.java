package pe.gob.mc.cnp.model.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.mc.cnp.model.dao.iface.ExpedienteMapper;
import pe.gob.mc.cnp.model.domain.Expediente;

public class ExpedienteService {
	
	@Autowired
	private ExpedienteMapper expedienteMapper;
	
	public void setExpedienteMapper(ExpedienteMapper oExpedienteMapper) {
		this.expedienteMapper = oExpedienteMapper;
	}
			
	public Boolean buscarExpedienteTramiteDocumentario(Map<String, Object> parameter) {		
		return expedienteMapper.buscarExpedienteTramiteDocumentario(parameter);
	}
		
	public Boolean buscarExpedienteXFicha(Map<String, Object> parameter) {		
		return expedienteMapper.buscarExpedienteXFicha(parameter);
	}
	
	public Boolean buscarExpedienteXNroExpediente(Map<String, Object> parameter) {		
		return expedienteMapper.buscarExpedienteXNroExpediente(parameter);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void insertarExpediente(Expediente oExpediente, Integer numeroSesion)   throws Exception{
		
		//Inserta
		oExpediente.setSesionCreacion(numeroSesion);
		oExpediente.setMensajeOut(null);
		expedienteMapper.insertarExpediente(oExpediente);
		 if (oExpediente.getMensajeOut() != null) throw new Exception(oExpediente.getMensajeOut());

	}
	
	@Transactional(rollbackFor = Exception.class)
	public void modificarExpediente(Expediente oExpediente, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oExpediente.setSesionEdicion(numeroSesion);
		oExpediente.setMensajeOut(null);
		expedienteMapper.modificarExpediente(oExpediente);
		if (oExpediente.getMensajeOut() != null) throw new Exception(oExpediente.getMensajeOut());
	    
	}
	
	/*@Transactional(rollbackFor = Exception.class)
    public void eliminarExpediente(Expediente oExpediente, Integer numeroSesion)   throws Exception{
		
		//Actualiza
		oExpediente.setSesionEdicion(numeroSesion);
		oExpediente.setActivo("N");
		oExpediente.setMensajeOut(null);
	    expedienteMapper.modificarExpedienteActivo(oExpediente);
	    if (oExpediente.getMensajeOut() != null) throw new Exception(oExpediente.getMensajeOut());
	
		
	}*/
	 
	
}