package pe.gob.mc.cnp.model.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.mc.cnp.model.dao.iface.DocumentoMapper;
import pe.gob.mc.cnp.model.domain.Documento;
import pe.gob.mc.cnp.model.factory.Factory;

@Service
public class DocumentoService {
	
	@Autowired
	private DocumentoMapper documentoMapper;
	
	public void setDocumentoMapper(DocumentoMapper oDocumentoMapper) {
		this.documentoMapper = oDocumentoMapper;
	}
		
	public Boolean buscarDocumentoXFicha(Map<String, Object> parameter) {		
		return documentoMapper.buscarDocumentoXFicha(parameter);
	}
	
	public Boolean doListaArchivosTramite(Map<String, Object> parameter) {		
		return documentoMapper.doListaArchivosTramite(parameter);
	}
	
	public Boolean obtenerSiglasPersona(Map<String, Object> parameter) {		
		return documentoMapper.obtenerSiglasPersona(parameter);
	}
	
	public Boolean leerDocumento(Map<String, Object> parameter) {		
		return documentoMapper.leerDocumento(parameter);
	}
	
	public Boolean buscarDocumentoXNumeroAnio(Map<String, Object> parameter) {		
		return documentoMapper.buscarDocumentoXNumeroAnio(parameter);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void insertarDocumento(Documento oDocumento, Integer numeroSesion)   throws Exception{
		
		//Inserta
		oDocumento.setSesionCreacion(numeroSesion);
		oDocumento.setMensajeOut(null);
		documentoMapper.insertarDocumento(oDocumento);
		 if (oDocumento.getMensajeOut() != null) throw new Exception(oDocumento.getMensajeOut());

		 
//
		  Map<String, Object> parame = new HashMap<String, Object>();
		    parame.put("codigoFicha",oDocumento.getCodigoFicha());
		    Factory.getInstance().getDocumentoBO().buscarDocumentoXFicha(parame);
		   List<Documento> listaDoc3=   (List<Documento>)parame.get("resultList"); 
		    
		   Documento oDocu= new Documento();
		   boolean principal =false;
		   int contador=1;
		    for(Documento oDoc1: listaDoc3){
		    	
		    	if(contador==1){
		    		oDocu= oDoc1;
		    	}
		    	
				    if(oDoc1.getPrincipal().equals("S")){
				    	principal=true;
				    	break;
				    }
				    
				    contador+=1;
			    }
		    
		    if(!principal){
		    	 //Actualiza
		    	oDocu.setSesionEdicion(numeroSesion);		 
		    	oDocu.setMensajeOut(null);
		    	oDocu.setPrincipal("S");
				    documentoMapper.modificarPrincipalDocumento(oDocu);
				    if (oDocu.getMensajeOut() != null) throw new Exception(oDocu.getMensajeOut());
		    }
		 
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void modificarDocumento(Documento oDocumento, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oDocumento.setSesionEdicion(numeroSesion);
		oDocumento.setMensajeOut(null);
		documentoMapper.modificarDocumento(oDocumento);
		if (oDocumento.getMensajeOut() != null) throw new Exception(oDocumento.getMensajeOut());
	    
	}
	
	@Transactional(rollbackFor = Exception.class)
    public void eliminarDocumento(Documento oDocumento, Integer numeroSesion)   throws Exception{
		
		 Map<String, Object> parameter = new HashMap<String, Object>();
		    parameter.put("codigoFicha",oDocumento.getCodigoFicha());
		    Factory.getInstance().getDocumentoBO().buscarDocumentoXFicha(parameter);
		   List<Documento> listaDoc2=   (List<Documento>)parameter.get("resultList"); 
		   System.out.println("cant doc "+listaDoc2.size());
		if(listaDoc2.size()>1){
			
			if(oDocumento.getPrincipal().equals("S")){
				 
			    oDocumento.setSesionEdicion(numeroSesion);
				oDocumento.setActivo("N");
				oDocumento.setMensajeOut(null);
			    documentoMapper.modificarDocumentoActivo(oDocumento);
			    if (oDocumento.getMensajeOut() != null) throw new Exception(oDocumento.getMensajeOut());
			    
			    Map<String, Object> parame = new HashMap<String, Object>();
			    parame.put("codigoFicha",oDocumento.getCodigoFicha());
			    Factory.getInstance().getDocumentoBO().buscarDocumentoXFicha(parame);
			   List<Documento> listaDoc3=   (List<Documento>)parame.get("resultList"); 
			    
			    for(Documento oDoc1: listaDoc3){
					 //Actualiza
					   oDoc1.setSesionEdicion(numeroSesion);		 
					   oDoc1.setMensajeOut(null);
					   oDoc1.setPrincipal("S");
					    documentoMapper.modificarPrincipalDocumento(oDoc1);
					    if (oDoc1.getMensajeOut() != null) throw new Exception(oDoc1.getMensajeOut());
					    
					    break;
				    } 
			 	}else{
			 		
			 		//Actualiza
					oDocumento.setSesionEdicion(numeroSesion);
					oDocumento.setActivo("N");
					oDocumento.setMensajeOut(null);
				    documentoMapper.modificarDocumentoActivo(oDocumento);
				    if (oDocumento.getMensajeOut() != null) throw new Exception(oDocumento.getMensajeOut());
			 		
			 	}
			
			
		}else{
			
			//Actualiza
			oDocumento.setSesionEdicion(numeroSesion);
			oDocumento.setActivo("N");
			oDocumento.setMensajeOut(null);
		    documentoMapper.modificarDocumentoActivo(oDocumento);
		    if (oDocumento.getMensajeOut() != null) throw new Exception(oDocumento.getMensajeOut());
		 } 
		
	}
	 
	@Transactional(rollbackFor = Exception.class)
    public void modificarPrincipalDocumento(Documento oDocumento, Integer numeroSesion)   throws Exception{
	 	//Actualiza
		oDocumento.setSesionEdicion(numeroSesion);		 
		oDocumento.setMensajeOut(null);
	    documentoMapper.modificarPrincipalDocumento(oDocumento);
	    if (oDocumento.getMensajeOut() != null) throw new Exception(oDocumento.getMensajeOut());
	 }
	
	
	@Transactional(rollbackFor = Exception.class)
    public void establecerEstadoPrincipalDocumento(Documento oDocumento, Integer numeroSesion)   throws Exception{
	 	
		// setear a N a los principales actuales
		 Map<String, Object> parameter = new HashMap<String, Object>();
		    parameter.put("codigoFicha",oDocumento.getCodigoFicha());
		    Factory.getInstance().getDocumentoBO().buscarDocumentoXFicha(parameter);
		   List<Documento> listaDoc2=   (List<Documento>)parameter.get("resultList"); 	
		   
		   for(Documento oDoc: listaDoc2){
			 //Actualiza
			   oDoc.setSesionEdicion(numeroSesion);		 
			   oDoc.setMensajeOut(null);
			   oDoc.setPrincipal("N");
			    documentoMapper.modificarPrincipalDocumento(oDoc);
			    if (oDoc.getMensajeOut() != null) throw new Exception(oDoc.getMensajeOut());
		   }
		   
		   
		//Actualiza
		oDocumento.setSesionEdicion(numeroSesion);		 
		oDocumento.setMensajeOut(null);
		oDocumento.setPrincipal("S");
	    documentoMapper.modificarPrincipalDocumento(oDocumento);
	    if (oDocumento.getMensajeOut() != null) throw new Exception(oDocumento.getMensajeOut());
	    
	    
	 }
	
	
	
	@Transactional(rollbackFor = Exception.class)
	public void insertarCertificado(Documento oDocumento, Integer numeroSesion)   throws Exception{
		Documento oCertificadoAnular = new Documento();;
	
//
		  Map<String, Object> parame = new HashMap<String, Object>();
		    parame.put("codigoFicha",oDocumento.getCodigoFicha());
		    Factory.getInstance().getDocumentoBO().buscarCertificadoXFicha(parame);
		   List<Documento> listaDoc3=   (List<Documento>)parame.get("resultList"); 
		   System.out.println("cantoda "+listaDoc3.size()); 
		   
		   if(listaDoc3.size()>0){			 
			   //elimino
			   for (Documento documento : listaDoc3) {
				   oCertificadoAnular.setCodigoDocumento(documento.getCodigoDocumento());
				   oCertificadoAnular.setSesionCreacion(numeroSesion);
				   oCertificadoAnular.setMensajeOut(null);
				   oCertificadoAnular.setActivo("N");
					documentoMapper.modificarCertificadoActivo(oCertificadoAnular);
					 if (oCertificadoAnular.getMensajeOut() != null) throw new Exception(oCertificadoAnular.getMensajeOut());	
			}
			   
		   }
		   
			//Inserta
			oDocumento.setSesionCreacion(numeroSesion);
			oDocumento.setMensajeOut(null);
			documentoMapper.insertarCertificado(oDocumento);
			 if (oDocumento.getMensajeOut() != null) throw new Exception(oDocumento.getMensajeOut());	   
				 
	}

	public Boolean buscarCertificadoXFicha(Map<String, Object> parameter) {		
		return documentoMapper.buscarCertificadoXFicha(parameter);
	}
 
	
	public void setFirmaDocumento(Documento oDocumentoMapper) {
		documentoMapper.modificarCertificadoActivo_firma(oDocumentoMapper);
	}
	
	
}