package pe.gob.mc.cnp.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.mc.cnp.model.dao.iface.BienMapper;
import pe.gob.mc.cnp.model.domain.Bien;
import pe.gob.mc.cnp.model.domain.CodigoHabilitado;
import pe.gob.mc.cnp.model.domain.DetalleBien;
import pe.gob.mc.cnp.model.factory.Factory;

@Service
public class BienService  {
	
	@Autowired
	private BienMapper bienMapper;
	
	public void setBienMapper(BienMapper oBienMapper) {
		this.bienMapper = oBienMapper;
	}
		
	public Boolean buscarBienesXFicha(Map<String, Object> parameter) {		
		return bienMapper.buscarBienesXFicha(parameter);
	}
	
	public Boolean leerBien(Map<String, Object> parameter) {		
		return bienMapper.leerBien(parameter);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void insertarBien(Bien oBien, Integer numeroSesion)   throws Exception{
		
		//Inserta
		oBien.setSesionCreacion(numeroSesion);
		oBien.setMensajeOut(null);
		bienMapper.insertarBien(oBien);
		 if (oBien.getMensajeOut() != null) throw new Exception(oBien.getMensajeOut());

	}
	
	@Transactional(rollbackFor = Exception.class)
	public void modificarBien(Bien oBien, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oBien.setSesionEdicion(numeroSesion);
		oBien.setMensajeOut(null);
		bienMapper.modificarBien(oBien);
		if (oBien.getMensajeOut() != null) throw new Exception(oBien.getMensajeOut());
	    
	}
	
	@Transactional(rollbackFor = Exception.class)
    public void eliminarBien(Bien oBien, Integer numeroSesion)   throws Exception{
		
	
	    
	    ///////////
	    
		Map<String, Object> parameter = null;// new HashMap<String, Object>();
		List<DetalleBien> listDetalleBien; 
		 	parameter = new HashMap<String, Object>();
			parameter.put("codigoBien",oBien.getCodigoBien());
			System.out.println("parameter codigoficha-> "+parameter);
			Factory.getInstance().getDetalleBienBO().buscarDetalleBienesXCodigoBien(parameter) ;
			listDetalleBien= (List<DetalleBien>)parameter.get("resultList"); 
			System.out.println("tam listDetalleBien "+listDetalleBien.size());
			CodigoHabilitado oCodigoHabilitado=null;
		for(DetalleBien detBien:  listDetalleBien){
			
			 oCodigoHabilitado= new CodigoHabilitado(oBien.getCodigoFicha(), detBien.getNumeroBien());
			 oCodigoHabilitado.setSesionEdicion(numeroSesion);
			 oCodigoHabilitado.setUtilizado("N");
			 oCodigoHabilitado.setMensajeOut(null);
				Factory.getInstance().getFichaBO().actualizarModoUtilizacionDelNumeroBien(oCodigoHabilitado, numeroSesion) ;
				 if (oCodigoHabilitado.getMensajeOut() != null) throw new Exception(oCodigoHabilitado.getMensajeOut());
				 
		}
		
		///////////////////
	    
		//Actualiza
		oBien.setSesionEdicion(numeroSesion);
		oBien.setActivo("N");
		oBien.setMensajeOut(null);
	    bienMapper.modificarBienActivo(oBien);
	    if (oBien.getMensajeOut() != null) throw new Exception(oBien.getMensajeOut());
	
	    DetalleBien  oDetalleBien= new DetalleBien(oBien.getCodigoBien());
	    oDetalleBien.setActivo("N");
	    Factory.getInstance().getDetalleBienBO().modificarDetalleBienActivoXBien(oDetalleBien, numeroSesion) ;
	 
	    
	}
	 
	
}