package pe.gob.mc.cnp.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.mc.cnp.model.dao.iface.DetalleBienMapper;
import pe.gob.mc.cnp.model.domain.CodigoHabilitado;
import pe.gob.mc.cnp.model.domain.DetalleBien;
import pe.gob.mc.cnp.model.domain.Ficha;
import pe.gob.mc.cnp.model.factory.Factory;

@Service
public class DetalleBienService  {
	
	@Autowired
	private DetalleBienMapper detalleBienMapper;
	
	public void setDetalleBienMapper(DetalleBienMapper oDetalleBienMapper) {
		this.detalleBienMapper = oDetalleBienMapper;
	}
		
	public Boolean buscarDetalleBienesXNumero(Map<String, Object> parameter) {		
		return detalleBienMapper.buscarDetalleBienesXNumero(parameter);
	}
	
	public Boolean buscarDetalleBienesXCodigoFicha(Map<String, Object> parameter) {		
		return detalleBienMapper.buscarDetalleBienesXCodigoFicha(parameter);
	}	
		
	public Boolean buscarDetalleBienesXCodigoBien(Map<String, Object> parameter) {		
		return detalleBienMapper.buscarDetalleBienesXCodigoBien(parameter);
	}	
	
	@Transactional(rollbackFor = Exception.class)
	public void insertarDetalleBien( Integer codigoFicha , Integer codigoBien,   List<Integer> listaNumeros /*DetalleBien oDetalleBien*/, Integer numeroSesion)   throws Exception{
		
	   //Actualizar la cantidad Autorizada
				Map<String, Object> parameter = null;// new HashMap<String, Object>();
				List<DetalleBien> listDetalleBien; 
				 	parameter = new HashMap<String, Object>();
					parameter.put("codigoFicha",codigoFicha);
					System.out.println("parameter codigoficha-> "+parameter);
					Factory.getInstance().getDetalleBienBO().buscarDetalleBienesXCodigoFicha(parameter);
					listDetalleBien= (List<DetalleBien>)parameter.get("resultList"); 	
					
					
		
		//Inserta		
		DetalleBien	oDetalleBien= null;	
		CodigoHabilitado	oCodigoHabilitado= null;	
		
		System.out.println("insertara: "+listaNumeros.size());
		for(Integer num:  listaNumeros){
			oDetalleBien= new DetalleBien(codigoBien, num);
			oDetalleBien.setSesionCreacion(numeroSesion);
			oDetalleBien.setMensajeOut(null);
			detalleBienMapper.insertarDetalleBien(oDetalleBien);
			 if (oDetalleBien.getMensajeOut() != null) throw new Exception(oDetalleBien.getMensajeOut());
			 
			 oCodigoHabilitado= new CodigoHabilitado(codigoFicha, num);
			 oCodigoHabilitado.setSesionEdicion(numeroSesion);
			 oCodigoHabilitado.setUtilizado("S");
			 oCodigoHabilitado.setMensajeOut(null);
				Factory.getInstance().getFichaBO().actualizarModoUtilizacionDelNumeroBien(oCodigoHabilitado, numeroSesion) ;
				 if (oCodigoHabilitado.getMensajeOut() != null) throw new Exception(oCodigoHabilitado.getMensajeOut());
				 
		}
		
		
			     // listDetalleBien.size() ;
		
			 int cantidadBienes=  listDetalleBien.size()  +  listaNumeros.size();
		
		Ficha oFicha = new Ficha();
		oFicha.setCodigoFicha(codigoFicha);
		oFicha.setCantidadAutorizada(cantidadBienes);
		Factory.getInstance().getFichaBO().modificarCantidadAutorizada(oFicha, numeroSesion);
		
		
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void modificarDetalleBienActivoXBien(DetalleBien oDetalleBien, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oDetalleBien.setSesionEdicion(numeroSesion);
		oDetalleBien.setMensajeOut(null);
		detalleBienMapper.modificarDetalleBienActivoXBien(oDetalleBien);
		if (oDetalleBien.getMensajeOut() != null) throw new Exception(oDetalleBien.getMensajeOut());
	    
		
		
		
	}
	
	/*@Transactional(rollbackFor = Exception.class)
    public void eliminarDetalleBien(DetalleBien oDetalleBien, Integer numeroSesion)   throws Exception{
		
		//Actualiza
		oDetalleBien.setSesionEdicion(numeroSesion);
		oDetalleBien.setActivo("N");
		oDetalleBien.setMensajeOut(null);
	    detalleBienMapper.modificarDetalleBienActivo(oDetalleBien);
	    if (oDetalleBien.getMensajeOut() != null) throw new Exception(oDetalleBien.getMensajeOut());
	
		
	}*/
	 
	
}