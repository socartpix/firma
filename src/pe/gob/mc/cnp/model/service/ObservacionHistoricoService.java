package pe.gob.mc.cnp.model.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.mc.cnp.model.dao.iface.ObservacionHistoricoMapper;
import pe.gob.mc.cnp.model.domain.ObservacionHistorico;

public class ObservacionHistoricoService {
	
	@Autowired
	private ObservacionHistoricoMapper observacionHistoricoMapper;
	
	public void setObservacionHistoricoMapper(ObservacionHistoricoMapper oObservacionHistoricoMapper) {
		this.observacionHistoricoMapper = oObservacionHistoricoMapper;
	}
		
	public Boolean buscarObservacionHistoricoXFicha(Map<String, Object> parameter) {		
		return observacionHistoricoMapper.buscarObservacionHistoricoXFicha(parameter);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void insertarObservacionHistorico(ObservacionHistorico oObservacionHistorico, Integer numeroSesion)   throws Exception{
		
		//Inserta
		oObservacionHistorico.setSesionCreacion(numeroSesion);
		oObservacionHistorico.setMensajeOut(null);
		System.out.println("oObservacionHistorico1 "+oObservacionHistorico);
		observacionHistoricoMapper.insertarObservacionHistorico(oObservacionHistorico);
		
		System.out.println("oObservacionHistorico2 "+oObservacionHistorico);
		 if (oObservacionHistorico.getMensajeOut() != null) throw new Exception(oObservacionHistorico.getMensajeOut());

	}
	
	@Transactional(rollbackFor = Exception.class)
	public void modificarObservacionHistorico(ObservacionHistorico oObservacionHistorico, Integer numeroSesion)  throws Exception{
	 
		//Modifica
		oObservacionHistorico.setSesionEdicion(numeroSesion);
		oObservacionHistorico.setMensajeOut(null);
		observacionHistoricoMapper.modificarObservacionHistorico(oObservacionHistorico);
		if (oObservacionHistorico.getMensajeOut() != null) throw new Exception(oObservacionHistorico.getMensajeOut());
	    
	}
	
	@Transactional(rollbackFor = Exception.class)
    public void modificarEstadoObservacionHistorico(ObservacionHistorico oObservacionHistorico, Integer numeroSesion)   throws Exception{
		
		//Actualiza
		oObservacionHistorico.setSesionEdicion(numeroSesion);
		oObservacionHistorico.setMensajeOut(null);
	    observacionHistoricoMapper.modificarEstadoObservacionHistorico(oObservacionHistorico);
	    if (oObservacionHistorico.getMensajeOut() != null) throw new Exception(oObservacionHistorico.getMensajeOut());
	
		
	}
	 
	
	
	@Transactional(rollbackFor = Exception.class)
    public void eliminarObservacionHistorico(ObservacionHistorico oObservacionHistorico, Integer numeroSesion)   throws Exception{
		
		//Actualiza
		oObservacionHistorico.setSesionEdicion(numeroSesion);
		oObservacionHistorico.setActivo("N");
		oObservacionHistorico.setMensajeOut(null);
		observacionHistoricoMapper.modificarEstadoObservacionHistoricoActivo(oObservacionHistorico);
	    if (oObservacionHistorico.getMensajeOut() != null) throw new Exception(oObservacionHistorico.getMensajeOut());
	
		
	}
	
}