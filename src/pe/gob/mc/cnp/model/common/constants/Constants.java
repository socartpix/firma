package pe.gob.mc.cnp.model.common.constants;

import java.io.Serializable;

public class Constants implements Serializable{
	
	private static final long serialVersionUID = 5527254336122913293L;
	
	//Debug Mode
	public static final boolean DEBUG=true;
	
	//Principal	
	public static final String CUENTA="CUENTA";		
	public static final String ABREV_APLICACION="MINC-CNP";
	public static final String NRO_VERSION="0.0.0.1";
	public static final String TIPO_SALIDA="LOGOUT";
	public static final String NROSESION="NROSESION";
	public static final String USUARIOLOGIN="USUARIOLOGIN";
	public static final String CODPERFIL = "CODPERFIL";
	public static final String CODAPLICATIVO = "CODAPLICATIVO";
	public static final String CODDEPENDENCIA="CODDEPENDENCIA";
	public static final String NOMOFICINA="NOMOFICINA";
	public static final String CODORGANIGRAMA="CODORGANIGRAMA";
	public static final String FECHAACTUAL="FECHAACTUAL"; 
	public static final String CODIGOUSUARIO="CODIGOUSUARIO";
	public static final String DATOS_USUARIO="DATOS_USUARIO";		
	public static final String CODIGOORGANIZACION="CODIGOORGANIZACION";	
    public static final String ERROR_DE_BASE_DE_DATOS="Ocurrio un error al guardar en la Base de Datos";	
	public static final String REPORTE_SIN_DATOS="No se encontraron resultados para la Consulta";	
	public static final String ERROR_EN_REPORTE_FECHAS_PARA_CONSULTA="Intervalo de fechas Erradas";
	
	//PERFIL
	public static final Integer PERFILADMINISTRADOR= 126;
	public static final Integer PERFILCONSULTA=127 ;
	public static final Integer PERFILREGISTRADOR= 128;
	public static final Integer PERFILSUPERVISOR=129;
	public static final Integer PERFILDIRECTOR=130;
	public static final Integer PERFILADMINISTRATIVO=139;	
	public static final Integer PERFILCONSULTAPAIS=212;
	public static final Integer PERFILCONSULTAAUTORIDADNACIONAL=213;
	public static final Integer PERFILCONSULTAALTADIRECCION=214;
	
	
	
	
	
}