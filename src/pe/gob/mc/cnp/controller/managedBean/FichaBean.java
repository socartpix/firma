package pe.gob.mc.cnp.controller.managedBean;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.richfaces.component.html.HtmlDataTable;

import pe.gob.mc.cnp.model.domain.Bien;
import pe.gob.mc.cnp.model.domain.CodigoHabilitado;
import pe.gob.mc.cnp.model.domain.DetalleBien;
import pe.gob.mc.cnp.model.domain.Documento;
import pe.gob.mc.cnp.model.domain.Expediente;
import pe.gob.mc.cnp.model.domain.Ficha;
import pe.gob.mc.cnp.model.domain.ObservacionHistorico;
import pe.gob.mc.cnp.controller.managedBean.BaseBean;
import pe.gob.mc.cnp.model.factory.Factory;
import pe.gob.mc.cnp.view.reports.UtilReporte;
import pe.gob.mc.msag.model.domain.Pais;
import pe.gob.mc.msag.model.domain.Sistema;
import pe.gob.mc.msag.model.domain.Usuario;
import pe.gob.mc.msag.model.factory.FactoryBase;
import sun.security.jca.GetInstance;

public class FichaBean extends BaseBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Ficha ficha;

	private HtmlDataTable tablaImagen, tablaCertificados, tablaDocumento, tablaObservaciones, tablaCertificadosAux ,tablaDocumento_sgd;

	public HtmlDataTable getTablaDocumento_sgd() {
		return tablaDocumento_sgd;
	}

	public void setTablaDocumento_sgd(HtmlDataTable tablaDocumento_sgd) {
		this.tablaDocumento_sgd = tablaDocumento_sgd;
	}

	private List<Bien> listaImagen;

	private List<Ficha> listaCertificados;

	private List<Documento> listaDocumento;

	private List<Documento> listaDocumento_sgd;

	
	private Bien bien;

	private Documento documento;

	private UploadedFile oUpDocumento, oUpImagen, oUpCertificado;

	private String operacionEnFicha;

	private ObservacionHistorico observacion;

	private List<ObservacionHistorico> listaObservaciones;

	private HtmlDataTable tablaAsignacion;
	private List<CodigoHabilitado> listAsignacion;

	private SelectItem[] listHorarios;

	public SelectItem[] getlistHorarios() {
		return listHorarios;
	}

	public HtmlDataTable getTablaAsignacion() {
		return tablaAsignacion;
	}

	public void setTablaAsignacion(HtmlDataTable tablaAsignacion) {
		this.tablaAsignacion = tablaAsignacion;
	}

	public List<CodigoHabilitado> getListAsignacion() {
		return listAsignacion;
	}

	public void setListAsignacion(List<CodigoHabilitado> listAsignacion) {
		this.listAsignacion = listAsignacion;
	}

	public ObservacionHistorico getObservacion() {
		return observacion;
	}

	public void setObservacion(ObservacionHistorico observacion) {
		this.observacion = observacion;
	}

	public HtmlDataTable getTablaObservaciones() {
		return tablaObservaciones;
	}

	public void setTablaObservaciones(HtmlDataTable tablaObservaciones) {
		this.tablaObservaciones = tablaObservaciones;
	}

	public List<ObservacionHistorico> getListaObservaciones() {
		// return listaObservaciones;
		List<ObservacionHistorico> lista = new ArrayList<ObservacionHistorico>();

		if (this.getCodigoFicha() != null) {
			Map<String, Object> parameter = new HashMap<String, Object>();
			parameter.put("codigoFicha", this.getCodigoFicha());
			Factory.getInstance().getObservacionHistoricoBO().buscarObservacionHistoricoXFicha(parameter);
			;
			lista = (List<ObservacionHistorico>) parameter.get("resultList");
		}

		return lista;

	}

	public void setListaObservaciones(List<ObservacionHistorico> listaObservaciones) {
		this.listaObservaciones = listaObservaciones;
	}

	public String getOperacionEnFicha() {
		return operacionEnFicha;
	}

	public void setOperacionEnFicha(String operacionEnFicha) {
		this.operacionEnFicha = operacionEnFicha;
	}

	// WYUCRA
	private List<SelectItem> listarAnios = new ArrayList<SelectItem>();

	public List<SelectItem> getListarAnios() {
		return listarAnios;
	}

	public void setListarAnios(List<SelectItem> listarAnios) {
		this.listarAnios = listarAnios;
	}
	//

	public FichaBean() {
		super();

		// this.setFechaListIni(getPrimerDiaDelMes());

		// fechaListFin= obtenerFechaActual();
		listAsignacion = new ArrayList<CodigoHabilitado>();

		if (this.getListarAnios().isEmpty())
			this.setListarAnios(oSelectItemListarAnios());

		asignarAnioActual();
	}

	public HtmlDataTable getTablaDocumento() {
		return tablaDocumento;
	}

	public void setTablaDocumento(HtmlDataTable tablaDocumento) {
		this.tablaDocumento = tablaDocumento;
	}

	public List<Documento> getListaDocumento() {
		System.out.println("getListaDocumento");
		String DNI=ficha.getNumeroIdentidad();
		System.out.println(DNI);
		String NroExpediente=ficha.getNumeroExpediente();
		System.out.println(NroExpediente);
		String co_dep_in="";
		/*
		DNI="40612555";
		NroExpediente="2019-0001611";*/
		
		
		//archivos prueba
		/*
		DNI="40612555";
		NroExpediente="2019-0001611";*/
		co_dep_in="";
		//archivos prueba
		
		// return listaDocumento;
		List<Documento> lista_sgd = new ArrayList<Documento>();
		List<Documento> lista = new ArrayList<Documento>();

		if (ficha.getCodigoFicha() != null) {
			Map<String, Object> parameter = new HashMap<String, Object>();
	
			parameter.put("codigoFicha", ficha.getCodigoFicha());
			
			Factory.getInstance().getDocumentoBO().buscarDocumentoXFicha(parameter);
			
			lista = (List<Documento>) parameter.get("resultList");
			
			System.out.println("Tama�o lista"+lista.size());
			
			/*23-10-2019*/
			Map<String, Object> parameter2 = new HashMap<String, Object>();
			System.out.println("Entrando con expediente"+NroExpediente);
			System.out.println("Entrando a lista de archivos de tramite --1");
				parameter2.put("vcNroDocumento", DNI);
				parameter2.put("vcNroExpediente", NroExpediente);
				//parameter2.put("co_dep_in", co_dep_in);
				
				System.out.println("Entrando a lista de archivos de tramite --2");
				try {
				Factory.getInstance().getDocumentoBO().doListaArchivosTramite(parameter2);	
				}
				catch(Exception e){
					System.out.println(e);
				}
				System.out.println("Entrando a lista de archivos de tramite --3");
				
				lista_sgd = (List<Documento>) parameter2.get("resultList");
       			parameter2.put("vcNroDocumento", ficha.getCodigoFicha());
	     		System.out.println("LISTA SGD");
				System.out.println(lista_sgd);
			/*23-10-2019*/
			
		
		}
		
		/*23-10-2019*/

		return lista;

	}
	
	public List<Documento> getlistaDocumento_sgd() {
		System.out.println("getListaDocumento SGD");
		
		// return listaDocumento;

		List<Documento> lista = new ArrayList<Documento>();

		if (ficha.getCodigoFicha() != null) {
			Map<String, Object> parameter = new HashMap<String, Object>();
			parameter.put("codigoFicha", ficha.getCodigoFicha());
			Factory.getInstance().getDocumentoBO().buscarDocumentoXFicha(parameter);
			/*String DNI="40612555";
			String NroExpediente="2019-0001611";
			*/
			
			String DNI=ficha.getNumeroIdentidad();
			System.out.println(DNI);
			String NroExpediente=ficha.getNumeroExpediente();
			System.out.println(NroExpediente);
			
			/**/
			
			parameter.put("vcNroDocumento", DNI);
			parameter.put("vcNroExpediente", NroExpediente);
			//parameter2.put("co_dep_in", co_dep_in);
			
			System.out.println("Entrando a lista de archivos de tramite --2");
			try {
			Factory.getInstance().getDocumentoBO().doListaArchivosTramite(parameter);	
			lista = (List<Documento>) parameter.get("resultList");
			}
			catch(Exception e){
				System.out.println(e);
			}
			
			
		}

		return lista;

	}
	

	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public UploadedFile getoUpDocumento() {
		return oUpDocumento;
	}

	public void setoUpDocumento(UploadedFile oUpDocumento) {
		this.oUpDocumento = oUpDocumento;
	}

	public UploadedFile getoUpImagen() {
		return oUpImagen;
	}

	public void setoUpImagen(UploadedFile oUpImagen) {
		this.oUpImagen = oUpImagen;
	}

	public Ficha getFicha() {
		return ficha;
	}

	public void setFicha(Ficha ficha) {
		this.ficha = ficha;
	}

	public HtmlDataTable getTablaImagen() {
		return tablaImagen;
	}

	public void setTablaImagen(HtmlDataTable tablaImagen) {
		this.tablaImagen = tablaImagen;
	}

	public HtmlDataTable getTablaCertificados() {
		return tablaCertificados;
	}

	public void setTablaCertificados(HtmlDataTable tablaCertificados) {
		this.tablaCertificados = tablaCertificados;
	}

	public List<Bien> getListaImagen() {
		// return listaImagen;

		List<Bien> lista = new ArrayList<Bien>();

		if (ficha.getCodigoFicha() != null) {
			Map<String, Object> parameter = new HashMap<String, Object>();
			parameter.put("codigoFicha", ficha.getCodigoFicha());
			Factory.getInstance().getBienBO().buscarBienesXFicha(parameter);
			lista = (List<Bien>) parameter.get("resultList");
		}

		return lista;

	}

	public void setListaImagen(List<Bien> listaImagen) {
		this.listaImagen = listaImagen;
	}

	public List<Ficha> getListaCertificados() {
		return listaCertificados;
	}

	public void setListaCertificados(List<Ficha> listaCertificados) {
		this.listaCertificados = listaCertificados;
	}

	public Bien getBien() {
		return bien;
	}

	public void setBien(Bien bien) {
		this.bien = bien;
	}

// Metodos
	/*
	 * public String[] cortarCadenaPorGuion(String cadena) { return
	 * cadena.split("\\-"); }
	 */

	private String numeroExpedienteBuscado;

	public String getNumeroExpedienteBuscado() {
		return numeroExpedienteBuscado;
	}

	public void setNumeroExpedienteBuscado(String numeroExpedienteBuscado) {
		this.numeroExpedienteBuscado = numeroExpedienteBuscado;
	}

	public int validarFormatoNroExpediente(String sTexto) {

		// Texto que vamos a buscar
		String sTextoBuscado = "-";
		// Contador de ocurrencias
		int contador = 0;

		while (sTexto.indexOf(sTextoBuscado) > -1) {
			sTexto = sTexto.substring(sTexto.indexOf(sTextoBuscado) + sTextoBuscado.length(), sTexto.length());
			contador++;
		}

		// System.out.println ("texto: "+sTexto+" contador-> "+contador);
		return contador;
	}

	public void buscarExpedienteTramiteDocumentario() {
		System.out.println("buscarExpedienteTramiteDocumentario");

		try {
			String n = null;
			String ms = "";
			// ImagenFicha fanexoInm = null;

			// if(!(ficha.getTipoPersona()!=null &&
			// !ficha.getTipoPersona().equals("")))ms+="Ingrese el Tipo de Persona. ";
			if (!(ficha.getNumeroExpediente() != null && !ficha.getNumeroExpediente().equals("")))
				ms += "Ingrese el N�mero del Expediente. ";

			if (!ms.equals("")) {
				this.msErrNeg(ms);
				return;
			}

			// validar el correcto uso del formato del expediente
			int cantidad = validarFormatoNroExpediente(ficha.getNumeroExpediente());

			if (cantidad != 1) {
				this.msErrNeg("Existe un error en el formato del N�mero de Expediente: #####-A�O");
				return;
			}

			//

			String[] arregloExpediente = cortarCadenaPorGuion(ficha.getNumeroExpediente());

			if (arregloExpediente.length != 2) {
				this.msErrNeg("El n�mero del expediente no tiene el formato adecuado");
				return;
			}

			// System.out.println("--> "+arregloExpediente[0]+" , "+arregloExpediente[1]);

			Map<String, Object> parameter = new HashMap<String, Object>();
			String anio = "";
			String expediente = "";
			try {
				expediente = Integer.parseInt(arregloExpediente[0]) + "";
				parameter.put("numeroExpediente", Integer.parseInt(arregloExpediente[0]));
				anio = arregloExpediente[1].trim();

				parameter.put("anio", anio);

			} catch (NumberFormatException enn) {
				System.out.println(enn.getMessage());
				this.msErrNeg("Debe Ingresar un valor v�lido para el n�mero de expediente");
				return;
			}

			if (anio.length() != 4) {
				this.msErrNeg("Error en formato: Debe ingresar el N� del expediente indicando el a�o YYYY");
				return;
			}

			Sistema oSistema = new Sistema();

			FactoryBase.getInstance().getSistemaBO().leerFechaActual(oSistema);

			Integer anioActual = Integer.parseInt(sdAnio.format(oSistema.getFechaActual()));

			Integer anioExpediente = Integer.parseInt(anio);

			if (anioExpediente < (anioActual - 1)) {
				this.msErrNeg("Error en formato: Debe ingresar un a�o mayor a " + anioExpediente);
				return;
			}

			ficha.setNumeroExpediente(expediente + "-" + anio);

			this.setNumeroExpedienteBuscado(expediente + "-" + anio);

			List<Expediente> listExpediente;

			// System.out.println("parameter -> "+parameter);

			Factory.getInstance().getExpedienteBO().buscarExpedienteTramiteDocumentario(parameter);
			listExpediente = (List<Expediente>) parameter.get("resultList");
			/// System.out.println("paso");

			if (listExpediente.size() == 1) {

				Expediente exp = listExpediente.get(0);
				// System.out.println("exp--> "+exp);
				// System.out.println("ficha.getTipoDocumento()-> "+ficha.getTipoDocumento());

				ficha.setNombresApellidos(exp.getNombresApellidos());
				ficha.setTipoDocumento(
						exp.getTipoDocumento().equals("NNN") ? ficha.getTipoDocumento() : exp.getTipoDocumento());
				ficha.setNumeroIdentidad(exp.getNumeroIdentidad());
				ficha.setAnio(arregloExpediente[1]);
				ficha.setFechaSolicitud(exp.getFechaSolicitud());

			} else if (listExpediente.size() == 0) {

				ficha.setNombresApellidos(null);
				ficha.setTipoDocumento(null);
				ficha.setNumeroIdentidad(null);
				ficha.setCantidadSolicitada(null);
				ficha.setDomicilio(null);
				ficha.setCodigoPais(null);

				this.msErrNeg("No se encontraron resultado para su b�squeda");
				return;
			} else {
				this.msErrNeg("El N�mero de expediente devuelve mas de un resultado :" + listExpediente.size());
				return;
			}

		} catch (Exception ex) {
			System.err.println("Error buscarExpedienteTramiteDocumentario()");
			this.msErrNeg("Error buscarExpedienteTramiteDocumentario(): " + ex.getMessage());
		}

	}

	public String validarDatosExpediente() {

		String mensaje = "";

//	System.out.println("ficha-> "+ficha);

		if (!(ficha.getTipoPersona() != null && !ficha.getTipoPersona().equals(""))) {
			mensaje += "EL Tipo de Persona es requerido \n";
		}

		if (ficha.getTipoPersona() != null && ficha.getTipoPersona().equals("P")) {

			if (ficha.getNacionalidad() == null) {
				mensaje += "La Nacionalidad es requerido \n";
			} else {
				if (this.ficha.getNacionalidad() != null && (this.ficha.getNacionalidad().trim()).equals("")) {
					mensaje += "La Nacionalidad es requerido \n";
				}

			}

		}

		if (ficha.getCodigoPais() == null) {
			mensaje += "EL PA�S es requerido \n";
		}

		if (this.ficha.getTipoDocumento() == null) {
			mensaje += "EL Tipo de Documento es requerido \n";
		}

		if (this.ficha.getNumeroIdentidad() == null) {
			mensaje += "EL N�mero de Documento es requerido \n";
		}
		if (this.ficha.getNumeroIdentidad() != null && (this.ficha.getNumeroIdentidad().trim()).equals("")) {
			mensaje += "EL N�mero de Documento es requerido \n";
		}

		if (!mensaje.equals("")) {
			return mensaje;
		}

		if (this.ficha.getNumeroIdentidad() != null) {

			if (this.ficha.getTipoDocumento().equals("DNI") && this.ficha.getNumeroIdentidad().length() != 8) {

				mensaje += "El DNI debe tener 8 d�gitos \n";
			} else if (this.ficha.getTipoDocumento().equals("RUC") && this.ficha.getNumeroIdentidad().length() != 11) {

				mensaje += "El RUC debe tener 11 d�gitos \n";
			} else if (this.ficha.getTipoDocumento().equals("CEE") && this.ficha.getNumeroIdentidad().length() < 7) {

				mensaje += "El Carne de Extranjer�a debe tener m�nimo 7 d�gitos \n";
			} else if (this.ficha.getTipoDocumento().equals("PAS") && this.ficha.getNumeroIdentidad().length() < 6) {

				mensaje += "El Pasaporte debe tener m�nimo 6 d�gitos \n";
			}

		}

		if (ficha.getNumeroExpediente() == null) {
			mensaje += "El N�MERO DE EXPEDIENTE es requerido \n";
		} else {
			if ((ficha.getNumeroExpediente().trim()).equals("")) {
				mensaje += "El N�MERO DE EXPEDIENTE es requerido \n";
			}
		}

		if (ficha.getNombresApellidos() == null) {
			mensaje += "El NOMBRE Y APELLIDO es requerido \n";
		} else {
			if ((ficha.getNombresApellidos().trim()).equals("")) {
				mensaje += "El NOMBRE Y APELLIDO es requerido \n";
			}
		}

		if (ficha.getDomicilio() == null || ficha.getDomicilio().trim().equals("")) {
			mensaje += "El DOMICILIO es requerido  \n";
		}

		if (!mensaje.equals("")) {
			return mensaje;
		}

		if (!this.getSesCodigoPerfil().equals(this.getCtePerfilAdministrativo())) {
			if ((ficha.getCantidadAutorizada() != null && ficha.getCantidadSolicitada() != null)
					&& ficha.getCantidadSolicitada() > ficha.getCantidadAutorizada()) {
				if (this.ficha.getObservacion() == null || this.ficha.getObservacion().equals("")) {
					mensaje += "La OBSERVACI�N es requerido  \n";
				}
			}
		}

		if (!mensaje.equals("")) {
			return mensaje;
		}

		///////////////////////////// validar solo cuando no tengas los nuevos estados

		// System.out.println("this.ficha.getMotivoNoAtencion()->
		// "+this.ficha.getMotivoNoAtencion());

		if (this.ficha.getMotivoNoAtencion() == null
				&& !this.getSesCodigoPerfil().equals(this.getCtePerfilAdministrativo())) {

//	 System.out.println(ficha.getNumeroExpediente()+" / "+this.getNumeroExpedienteBuscado());

			if (!ficha.getNumeroExpediente().equals(this.getNumeroExpedienteBuscado())) {
				mensaje += "Error: Se ha detectado una modificaci�n en el N�MERO DE EXPEDIENTE no coincide con el Original \n";
			}

			/*
			 * if( ( ficha.getCantidadAutorizada()!=null &&
			 * ficha.getCantidadSolicitada()!=null ) && ficha.getCantidadAutorizada() >
			 * ficha.getCantidadSolicitada()){ mensaje+
			 * ="La CANTIDAD AUTORIZADA no debe ser mayor a la CANTIDAD SOLICITADA  \n"; }
			 */

			if ((ficha.getCantidadSolicitada() != null) && ficha.getCantidadSolicitada() < 1) {
				mensaje += "La CANTIDAD SOLICITADA debe ser mayor que 0 \n";
			}

			/*
			 * if( ( ficha.getCantidadAutorizada()!=null ) &&
			 * ficha.getCantidadAutorizada()<1){
			 * mensaje+="La CANTIDAD AUTORIZADA debe ser mayor 0 \n"; }
			 */

			if (ficha.getCantidadSolicitada() == null) {
				mensaje += "La CANTIDAD SOLICITADA es requerido \n";
			}

			// System.out.println("antes de comp");
			// System.out.println("sesion "+ getSesCodigoPerfil()+"
			// "+getCtePerfilRegistrador());

			if (!mensaje.equals("")) {
				return mensaje;
			}

			if (getSesCodigoPerfil().equals(getCtePerfilRegistrador())
					|| getSesCodigoPerfil().equals(getCtePerfilDirector())) {

				Map<String, Object> parameter = new HashMap<String, Object>();
				parameter.put("codigoFicha", ficha.getCodigoFicha());
				Factory.getInstance().getDocumentoBO().buscarDocumentoXFicha(parameter);
				List<Documento> listaDoc = (List<Documento>) parameter.get("resultList");

				if (listaDoc.size() == 0) {
					mensaje += "El INFORME T�CNICO es requerido \n";
				}

				if (!mensaje.equals("")) {
					return mensaje;
				}

				parameter = new HashMap<String, Object>();
				parameter.put("codigoFicha", ficha.getCodigoFicha());
				Factory.getInstance().getBienBO().buscarBienesXFicha(parameter);
				List<Bien> listaBien = (List<Bien>) parameter.get("resultList");

				if (listaBien.size() == 0) {
					mensaje += "El DETALLE DEL BIEN es requerido \n";
				}

				if (!mensaje.equals("")) {
					return mensaje;
				}

				boolean encontrado = false;

				for (Bien oBien : listaBien) {
					// buscar si hay un conjunto
					// System.out.println("buscar: "+oBien.getConjunto());
					if (oBien.getConjunto().indexOf("-") > 0) {
						encontrado = true;
						// System.out.println("encontrado intervalo ");
					} else if (oBien.getConjunto().indexOf(",") > 0) {
						encontrado = true;
						// System.out.println("encontrado intervalo 2");
					}

					if (encontrado) {

						if (this.ficha.getObservacion() == null || this.ficha.getObservacion().equals("")) {
							mensaje += "La OBSERVACI�N es requerido";
						}
						break;
					}
				}

			}

			///////////////////

		}

		return mensaje;
	}

	public String validarDatosBienes() {
		System.out.println("validarDatosBienes");

		String mensaje = "";

		if (bien.getConjunto() == null) {
			mensaje += "El CONJUNTO DE C�DIGOS es requerido \n";
		} else {
			if ((bien.getConjunto().trim()).equals("")) {
				mensaje += "El CONJUNTO DE C�DIGOS es requerido \n";
			}
		}

		if (bien.getDescripcion() == null) {
			mensaje += "La DESCRIPCI�N es requerido \n";
		} else {
			if ((bien.getDescripcion().trim()).equals("")) {
				mensaje += "La DESCRIPCI�N es requerido \n";
			}
		}

// validar IMAGEN ADJUNTO

		/*
		 * if(oUpImagen==null){ mensaje+="La IMAGEN es requerida \n"; }
		 */

		if (oUpImagen != null) {

			String contextoAnexo = oUpImagen.getContentType().toUpperCase();

			if ((contextoAnexo).indexOf("JPG") != -1 || (contextoAnexo).indexOf("JPEG") != -1
					|| (contextoAnexo).indexOf("PNG") != -1) {

			} else {
				mensaje = "Solo se permite subir archivos con extensi�n .jpg , .jpeg , .png ";
				oUpImagen = null;

			}

		}

		return mensaje;

	}

	public void agregarBienImagenes() {
		System.out.println("agregarBienImagenes");

		// Guardar expediente
		String mensaje = "";
		mensaje = validarDatosBienes();
		if (!mensaje.equals("")) {
			this.msErrNeg(mensaje);
			return;
		}

		List<Integer> listadoDeCodigo = new ArrayList<Integer>();

		String nombreArchivo = null;
		try {

			// validar cantidad de bienes a ingresar size + cantidad de biebens actuales

			int cantidadDetalleBienes = 0;

			Map<String, Object> parameter = null;// new HashMap<String, Object>();
			List<DetalleBien> listDetalleBien;
			parameter = new HashMap<String, Object>();
			parameter.put("codigoFicha", ficha.getCodigoFicha());
			System.out.println("parameter codigoficha-> " + parameter);
			Factory.getInstance().getDetalleBienBO().buscarDetalleBienesXCodigoFicha(parameter);
			listDetalleBien = (List<DetalleBien>) parameter.get("resultList");
			cantidadDetalleBienes += listDetalleBien.size();

			if (this.getDeshabilitarCodigosBienes().equals("N")) {

				// Inicio validar ingreso de conjunto de bienes ////////////////////

				String[] cadenasGuion = cortarCadenaPorComas(bien.getConjunto());

				for (int i = 0; i < cadenasGuion.length; i++) {
					// System.out.println("conGuion "+cadenasGuion[i]);
					String[] valortexto = cortarCadenaPorGuion(cadenasGuion[i]);

					if (valortexto.length == 1) {
						listadoDeCodigo.add(Integer.parseInt(valortexto[0]));

						cantidadDetalleBienes += 1;
						System.out.println("cantidadDetalleBienes2-> " + cantidadDetalleBienes);
					} else if (valortexto.length == 2) {
						int intervaloInicio = Integer.parseInt(valortexto[0]);
						int intervaloFinal = Integer.parseInt(valortexto[1]);

						if (intervaloInicio >= intervaloFinal) {
							this.msErrNeg("Revise los intervalos de los c�digos de bienes: " + intervaloInicio
									+ " no es menor que " + intervaloFinal);
							return;
						}

						cantidadDetalleBienes += (intervaloFinal - intervaloInicio + 1);

						for (int ini = intervaloInicio; intervaloInicio <= intervaloFinal; ini++) {
							listadoDeCodigo.add(ini);
							intervaloInicio += 1;
						}

					} else {
						this.msErrNeg("Formato incorrecto de bienes");
						return;
					}
				}

				// Validando la cantidad de detallebienes supera a la cantidad solicitada
				/*
				 * if(cantidadDetalleBienes>this.getFicha().getCantidadAutorizada()){
				 * this.msErrNeg("La cantidad de bienes ingresados ser�a ("
				 * +cantidadDetalleBienes+") y supera a la cantidad autorizada de bienes ("+this
				 * .getFicha().getCantidadAutorizada()+")"); return; }
				 */

				// Fin

				/// Consultar que no esten registrador en la base de datos

				// Map<String, Object> parameter = null;// new HashMap<String, Object>();
				// List<DetalleBien> listDetalleBien;

				List<Integer> listAuxiliar = null;
				listAuxiliar = listadoDeCodigo;
				System.out.println("cantidaDES: " + listAuxiliar.size() + "   " + listadoDeCodigo.size());
				for (Integer numABuscar : listadoDeCodigo) {
					int contador = 0;
					for (Integer numABuscado : listAuxiliar) {

						if (numABuscar.equals(numABuscado)) {
							contador++;
						}

						if (contador > 1) {
							this.msErrNeg("Error en el ingreso de los C�digos de Bienes. Codigo de bien duplicado ");
							return;
						}
					}
				}

				for (Integer num : listadoDeCodigo) {
					parameter = new HashMap<String, Object>();
					parameter.put("numeroBien", num);
					Factory.getInstance().getDetalleBienBO().buscarDetalleBienesXNumero(parameter);
					listDetalleBien = (List<DetalleBien>) parameter.get("resultList");
					if (listDetalleBien.size() > 0) {
						this.msErrNeg(
								"Error en el ingreso de los C�digos de Bienes. ya se encuentra registrado el # " + num);
						return;
					}
				}

			}

			// validar si los nuemros ingresados estan disponibles o no para el expediente

			for (Integer numABuscar : listadoDeCodigo) {

				parameter = new HashMap<String, Object>();
				parameter.put("numeroBien", numABuscar);
				parameter.put("codigoFicha", ficha.getCodigoFicha());
				Factory.getInstance().getFichaBO().buscarCodigoBienDisponibleXFicha(parameter);
				List<CodigoHabilitado> listCodigoHabilitado = (List<CodigoHabilitado>) parameter.get("resultList");

				if (listCodigoHabilitado.size() == 0) {
					this.msErrNeg("Error numero de bien (" + numABuscar + ") no esta disponible para este expediente");
					return;
				}

			}

			///////////////////////////////////////////////////////////////////////////////////////

			if (oUpImagen != null) {
				nombreArchivo = getArchivos().tomahawkListenerIMG(oUpImagen);
			} else {
				nombreArchivo = bien.getNombreArchivo();
			}

			System.out.println("ver nombreArchivo-> " + nombreArchivo);

			Bien oBien = new Bien(ficha.getCodigoFicha(), bien.getDescripcion(), bien.getConjunto(),
					((nombreArchivo != null) ? getArchivos().getRutaWebFotos() : null), nombreArchivo);

			// Registro de bien
			if (bien.getCodigoBien() != null) {
				oBien.setCodigoBien(bien.getCodigoBien());
				Factory.getInstance().getBienBO().modificarBien(oBien, this.getSesNroSesion());
				this.msOutDBOK("Se modific� los datos satisfactoriamente. ");

			} else {
				Factory.getInstance().getBienBO().insertarBien(oBien, this.getSesNroSesion());
				bien.setCodigoBien(oBien.getCodigoBien());
				// Insertar detalle
				Factory.getInstance().getDetalleBienBO().insertarDetalleBien(ficha.getCodigoFicha(),
						bien.getCodigoBien(), listadoDeCodigo, this.getSesNroSesion());

				this.msOutDBOK("Se guard� los datos satisfactoriamente. ");
			}

			//////////

			parameter = new HashMap<String, Object>();
			parameter.put("codigoFicha", ficha.getCodigoFicha());
			System.out.println("parameter codigoficha-> " + parameter);
			Factory.getInstance().getDetalleBienBO().buscarDetalleBienesXCodigoFicha(parameter);
			listDetalleBien = (List<DetalleBien>) parameter.get("resultList");
			this.ficha.setCantidadAutorizada(listDetalleBien.size());

			/////////

			this.setDeshabilitarCodigosBienes("N");

			limpiarBien();

		} catch (Exception e) {
			this.msErrNeg("Error al guardar los datos del formulario de Bienes.");

			System.out.println("Error al guardar los datos del formulario de Bienes. " + e.getMessage());
		}

	}

	public void limpiarBien() {
		bien = new Bien();
	}

	public void limpiarDocumento() {
		documento = new Documento();
	}

	public void eliminarImagenFicha() {

		System.out.println("eliminarImagenFicha");

		try {
			Bien oFormasOcupacion = new Bien();

			oFormasOcupacion = (Bien) tablaImagen.getRowData();
			oFormasOcupacion.setActivo("N");
			Factory.getInstance().getBienBO().eliminarBien(oFormasOcupacion, this.getSesNroSesion());

///
			Map<String, Object> parameter = new HashMap<String, Object>();
			List<DetalleBien> listDetalleBien;
			parameter = new HashMap<String, Object>();
			parameter.put("codigoFicha", ficha.getCodigoFicha());
			System.out.println("parameter codigoficha-> " + parameter);
			Factory.getInstance().getDetalleBienBO().buscarDetalleBienesXCodigoFicha(parameter);
			listDetalleBien = (List<DetalleBien>) parameter.get("resultList");
			this.ficha.setCantidadAutorizada(listDetalleBien.size());

///

			this.msOutDBOK("Registro Eliminado");
		} catch (Exception ex) {
			System.err.println("Error eliminar Lista Imagen Ficha ");
			this.msErrNeg("Error eliminar Lista Imagen Ficha " + ex.getMessage());
		}

	}

	public void guardarExpediente() {
		System.out.println("guardarExpediente");

		// Guardar expediente
		String mensaje = "";
		mensaje = validarDatosExpediente();
		if (!mensaje.equals("")) {
			this.msErrNeg(mensaje);
			return;
		}

		// verificar que el expediente no se encuentre registrado en otro certificado

		Map<String, Object> parameter = new HashMap<String, Object>();

		parameter.put("numeroExpediente", ficha.getNumeroExpediente());
		parameter.put("codigoFicha", ficha.getCodigoFicha());

		Factory.getInstance().getExpedienteBO().buscarExpedienteXNroExpediente(parameter);

		List<Expediente> list = (List<Expediente>) parameter.get("resultList");

		if (list.size() > 2) {

			this.msErrNeg("El N�mero de expediente ya se encuentra en tres Certificados");
			return;

		}

		///////////
		/*
		 * Map<String, Object> param = null;// new HashMap<String, Object>();
		 * List<DetalleBien> listDetalleBien; param = new HashMap<String, Object>();
		 * param.put("codigoFicha",ficha.getCodigoFicha());
		 * System.out.println("param codigoficha-> "+param);
		 * Factory.getInstance().getDetalleBienBO().buscarDetalleBienesXCodigoFicha(
		 * param); listDetalleBien= (List<DetalleBien>)param.get("resultList"); int
		 * cantidadDetalleBienes= listDetalleBien.size() ;
		 * 
		 * if(cantidadDetalleBienes<this.ficha.getCantidadAutorizada()){
		 * 
		 * }
		 */

		///////////

		try {
			// Ingresar Ficha

			if (ficha.getCodigoFicha() == null) {

				Ficha oFicha = new Ficha(ficha.getCantidadAutorizada(), null, null /* this.getSesCodUsuario() */,
						null/* this.getSesUsuarioLogin() */, ficha.getTipoPersona(), ficha.getNacionalidad(),
						this.getSesCodUsuario());
				Expediente oExpediente = new Expediente(ficha.getCodigoFicha(), ficha.getNumeroExpediente(),
						ficha.getNombresApellidos(), ficha.getAnio(), ficha.getTipoDocumento(),
						ficha.getNumeroIdentidad(), ficha.getDomicilio(), ficha.getCodigoPais(),
						ficha.getCantidadSolicitada(), ficha.getFechaSolicitud());

				Factory.getInstance().getFichaBO().insertarFicha(oFicha, oExpediente, this.getSesNroSesion());
				ficha.setCodigoFicha(oFicha.getCodigoFicha());

				ficha.setEstadoFicha("C");
				// System.out.println("REG oFicha "+oFicha);

				if (oFicha.getEstadoOut() != 1) {
					this.msErrNeg("Error al Registrar:" + oFicha.getMensajeOut());
					return;
				}

				this.msOutDBOK("Se guard� los datos satisfactoriamente. ");

			} else { // Modificar

				System.out.println("MODIF oFicha " + ficha);

				// registrar expediente
				Expediente oExpediente = new Expediente(ficha.getCodigoExpediente(), ficha.getCodigoFicha(),
						ficha.getNumeroExpediente(), ficha.getNombresApellidos(), ficha.getAnio(),
						ficha.getTipoDocumento(), ficha.getNumeroIdentidad(), ficha.getDomicilio(),
						ficha.getCodigoPais(), ficha.getCantidadSolicitada(), ficha.getFechaSolicitud());

				Factory.getInstance().getExpedienteBO().modificarExpediente(oExpediente, this.getSesNroSesion());

				// Verificar y cambiar de estados segun sea

				// Ficha oFicha = new Ficha(ficha.getCodigoFicha());

				/*
				 * if( this.ficha.getEstadoFicha().equals("O") ){
				 * Factory.getInstance().getFichaBO().levantarObservacionCertificado(oFicha,
				 * this.getSesNroSesion()); }
				 */

				Factory.getInstance().getFichaBO().modificarFicha(ficha, this.getSesNroSesion());

				this.msOutDBOK("Se actualiz� los datos satisfactoriamente. ");

			}

		} catch (Exception e) {
			this.msErrNeg("Error al guardar los datos del formulario.");

			System.out.println("Error al guardar los datos del formulario. " + e.getMessage());
		}

	}

	public void inicializarNuevoRegistro() {
		System.out.println("inicializarNuevoRegistro");

		listaDocumento = new ArrayList<Documento>();

		bien = new Bien();
		documento = new Documento();
		ficha = new Ficha();

		this.setOperacionEnFicha("N");

		this.setNumeroExpedienteBuscado(null);

		this.setDeshabilitarCodigosBienes("N");

		modoEditable = true;

		asignarAnioActual();

	}

	public void asignarAnioActual() {

		if (documento != null) {
			Sistema oSistema = new Sistema();
			FactoryBase.getInstance().getSistemaBO().leerFechaActual(oSistema);
			Integer anioActual = Integer.parseInt(sdAnio.format(oSistema.getFechaActual()));
			documento.setSufijoDocumento(anioActual + "");
		}
	}

	public String validarDatosDocumento() {

		System.out.println("validarDatosDocumento");
		String mensaje = "";

		if (documento.getNumeroDocumento() == null) {
			mensaje += "El N�MERO DE INFORME es requerido \n";
		} else {
			if ((documento.getNumeroDocumento().trim()).equals("")) {
				mensaje += "El N�MERO DE INFORME es requerido \n";
			}
		}

		// validar DOCUMENTO ADJUNTO

		/*
		 * if(oUpDocumento==null){ mensaje+="El DOCUMENTO es requerido \n"; }
		 */

		if (oUpDocumento != null) {

			String contextoAnexo = oUpDocumento.getContentType().toUpperCase();

			if ((contextoAnexo).indexOf("PDF") != -1 || (contextoAnexo).indexOf("OFFICEDOCUMENT") != -1) {

			} else {
				mensaje = "Solo se permite subir archivos con extensi�n .pdf , .docx";
				oUpDocumento = null;

			}

		}

		return mensaje;

	}

	public void agregarDatosAnexo() {

		System.out.println("agregarDatosAnexo2");

		// validar que el Docuemnto no exista

		if (documento.getCodigoDocumento() == null) {
			System.out.println("agregarDatosAnexo3  :" + documento.getCodigoDocumento());

			Map<String, Object> parameter = new HashMap<String, Object>();

			parameter.put("numeroDocumento", documento.getNumeroDocumento());
			parameter.put("anio", documento.getSufijoDocumento());
			parameter.put("siglasDocumento", documento.getSiglasDocumento());

			parameter.put("flagDocumento", documento.getFlagDocumento());

			System.out.println("numeroDocumento: " + documento.getNumeroDocumento());
			System.out.println("anio: " + documento.getSufijoDocumento());
			System.out.println("siglasDocumento: " + documento.getSiglasDocumento());
			System.out.println("codigoFicha: " + documento.getCodigoFicha());
			System.out.println("flagDocumento: " + documento.getFlagDocumento());

			Factory.getInstance().getDocumentoBO().buscarDocumentoXNumeroAnio(parameter);

			List<Documento> list = (List<Documento>) parameter.get("resultList");

			if (list.size() > 0) {

				parameter = new HashMap<String, Object>();
				parameter.put("codigoFicha", (list.get(0)).getCodigoFicha());

				Factory.getInstance().getFichaBO().leerFicha(parameter);

				List<Ficha> listFicha = (List<Ficha>) parameter.get("resultList");

				String nroCert = listFicha.get(0).getNumeroCertificadoFormato();

				this.msErrNeg("El N�mero de informe ya se encuentra en un Certificado " + nroCert);
				return;
			}

			String mensaje = "";
			mensaje = validarDatosDocumento();
			if (!mensaje.equals("")) {
				this.msErrNeg(mensaje);
				return;
			}

		}

		String nombreArchivo = null;
		try {

			System.out.println("okokko: ");

			if (oUpDocumento != null) {
				nombreArchivo = getArchivos().tomahawkListener(oUpDocumento);
			} else {
				nombreArchivo = documento.getNombreArchivo();
			}

			// if(oUpDocumento!=null){
			// nombreArchivo = getArchivos().tomahawkListenerIMG(oUpDocumento);
			// }

			if (documento.getCodigoDocumento() != null) { // modificadndo

				Documento oDocumento = new Documento(documento.getCodigoDocumento(), ficha.getCodigoFicha(),
						documento.getNumeroDocumento(), documento.getPrefijoDocumento(), documento.getSufijoDocumento(),
						documento.getSiglasDocumento(), documento.getReferencia(), nombreArchivo,
						(nombreArchivo != null) ? getArchivos().getRutaWebAnexos() : null,
						documento.getFlagDocumento());

				Factory.getInstance().getDocumentoBO().modificarDocumento(oDocumento, this.getSesNroSesion());

				System.out.println("oDocumentoM--> " + oDocumento);

			} else {// nuevo

				Documento oDocumento = new Documento(ficha.getCodigoFicha(), documento.getNumeroDocumento(),
						documento.getPrefijoDocumento(), documento.getSufijoDocumento(), documento.getSiglasDocumento(),
						documento.getReferencia(), nombreArchivo,
						(nombreArchivo != null) ? getArchivos().getRutaWebAnexos() : null, this.getSesCodUsuario(),
						documento.getFlagDocumento());

				Factory.getInstance().getDocumentoBO().insertarDocumento(oDocumento, this.getSesNroSesion());

				System.out.println("oDocumentoN--> " + oDocumento);

			}

			this.msOutDBOK("Se guard� el documento ");

			limpiarDocumento();

		} catch (Exception e) {
			this.msErrNeg("Error al guardar los datos del Documento.");

			System.out.println("Error al guardar los datos del Documento. " + e.getMessage());
		}

	}

	private String deshabilitarCodigosBienes;

	public String getDeshabilitarCodigosBienes() {
		return deshabilitarCodigosBienes;
	}

	public void setDeshabilitarCodigosBienes(String deshabilitarCodigosBienes) {
		this.deshabilitarCodigosBienes = deshabilitarCodigosBienes;
	}

	public void selecImgBien() {
		bien = (Bien) tablaImagen.getRowData();
		oUpImagen = null;

		this.setDeshabilitarCodigosBienes("S");
	}

	public void cancelarModificarBienImagenes() {

		bien = new Bien();
		oUpImagen = null;
		this.setDeshabilitarCodigosBienes("N");
	}

	public void selecAnexoDocumento() {
		documento = (Documento) tablaDocumento.getRowData();
		oUpDocumento = null;
		System.out.println("documento---> " + documento);
	}

	public void cancelarModificarAnexoDocumento() {

		documento = new Documento();
		oUpDocumento = null;
		this.setDeshabilitarCodigosBienes("N");
	}

	public void eliminarAnexoDocumento() {
		System.out.println("eliminarAnexoDocumento");
		try {

			Documento oDocumento = (Documento) tablaDocumento.getRowData();
			oDocumento.setActivo("N");
			System.out.println("eliminar oDocumento-> " + oDocumento);
			Factory.getInstance().getDocumentoBO().eliminarDocumento(oDocumento, this.getSesNroSesion());

		} catch (Exception ex) {
			System.err.println("Error eliminarAnexoDocumento: " + ex.getMessage());
			this.msErrNeg("Error eliminarAnexoDocumento: " + ex.getMessage());
		}

		oUpDocumento = null;
	}

	// BANDEJA DE BUSQUEDA
	// Variables:

	private Integer numeroCertificado;
	private String codigoEstadoCertificado;
	private Integer codigoUsuarioRegistro;
	private Date fechaListIni;
	private Date fechaListFin;
	private Integer cantidadElementosPaginado;

	private String numeroExpediente;
	private String nombreAdministrado;
	private String descripcionBienBusq;
	private String codigoBienBusq;
	private String numeroInformeTecnicoBusq;
	private String anioInformeBusq;
	private Integer paisBusq;

	public Integer getPaisBusq() {
		return paisBusq;
	}

	public void setPaisBusq(Integer paisBusq) {
		this.paisBusq = paisBusq;
	}

	public String getAnioInformeBusq() {
		return anioInformeBusq;
	}

	public void setAnioInformeBusq(String anioInformeBusq) {
		this.anioInformeBusq = anioInformeBusq;
	}

	public String getDescripcionBienBusq() {
		return descripcionBienBusq;
	}

	public void setDescripcionBienBusq(String descripcionBienBusq) {
		this.descripcionBienBusq = descripcionBienBusq;
	}

	public String getCodigoBienBusq() {
		return codigoBienBusq;
	}

	public void setCodigoBienBusq(String codigoBienBusq) {
		this.codigoBienBusq = codigoBienBusq;
	}

	public String getNumeroInformeTecnicoBusq() {
		return numeroInformeTecnicoBusq;
	}

	public void setNumeroInformeTecnicoBusq(String numeroInformeTecnicoBusq) {
		this.numeroInformeTecnicoBusq = numeroInformeTecnicoBusq;
	}

	public String getNumeroExpediente() {
		return numeroExpediente;
	}

	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}

	public String getNombreAdministrado() {
		return nombreAdministrado;
	}

	public void setNombreAdministrado(String nombreAdministrado) {
		this.nombreAdministrado = nombreAdministrado;
	}

	public Integer getCantidadElementosPaginado() {
		return cantidadElementosPaginado;
	}

	public void setCantidadElementosPaginado(Integer cantidadElementosPaginado) {
		this.cantidadElementosPaginado = cantidadElementosPaginado;
	}

	public Integer getNumeroCertificado() {
		return numeroCertificado;
	}

	public void setNumeroCertificado(Integer numeroCertificado) {
		this.numeroCertificado = numeroCertificado;
	}

	public String getCodigoEstadoCertificado() {
		return codigoEstadoCertificado;
	}

	public void setCodigoEstadoCertificado(String codigoEstadoCertificado) {
		this.codigoEstadoCertificado = codigoEstadoCertificado;
	}

	public Integer getCodigoUsuarioRegistro() {
		return codigoUsuarioRegistro;
	}

	public void setCodigoUsuarioRegistro(Integer codigoUsuarioRegistro) {
		this.codigoUsuarioRegistro = codigoUsuarioRegistro;
	}

	public Date getFechaListIni() {
		return fechaListIni;
	}

	public void setFechaListIni(Date fechaListIni) {
		this.fechaListIni = fechaListIni;
	}

	public Date getFechaListFin() {
		return fechaListFin;
	}

	public void setFechaListFin(Date fechaListFin) {
		this.fechaListFin = fechaListFin;
	}

	private boolean modoEditable;

	public boolean isModoEditable() {
		return modoEditable;
	}

	public void setModoEditable(boolean modoEditable) {
		this.modoEditable = modoEditable;
	}

	public void llenarSiglasPersona() {

		Map<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("usuarioRegistra", this.getSesCodUsuario());
		Factory.getInstance().getDocumentoBO().obtenerSiglasPersona(parameter);
		List<Documento> list = (List<Documento>) parameter.get("resultList");
		String siglas = "";
		if (list.size() == 1) {
			siglas = list.get(0).getSiglasDocumento();
			documento.setSiglasDocumento(siglas);
		}
	}

	private Documento certificadoIMG;

	private String verCertificadoImg;

	public String getVerCertificadoImg() {
		return verCertificadoImg;
	}

	public void setVerCertificadoImg(String verCertificadoImg) {
		this.verCertificadoImg = verCertificadoImg;
	}

	public HtmlDataTable getTablaCertificadosAux() {
		return tablaCertificadosAux;
	}

	public void setTablaCertificadosAux(HtmlDataTable tablaCertificadosAux) {
		this.tablaCertificadosAux = tablaCertificadosAux;
	}

	public void seleccionarCertificado() {
		System.out.println("seleccionarCertificado");

		this.setOperacionEnFicha("V");
		modoEditable = false;
		ficha = (Ficha) tablaCertificados.getRowData();
		// ficha=(Ficha) tablaCertificadosAux.getRowData();

		FacesContext context = FacesContext.getCurrentInstance();
		Map params = context.getExternalContext().getRequestParameterMap();
		String soloVerCertificado = (String) params.get("soloVerCertificado");

		this.setVerCertificadoImg(soloVerCertificado);

		if (this.getVerCertificadoImg().equals("N")) {
			certificadoIMG = new Documento();
			System.out.println(ficha.getCodigoFicha());
			System.out.println(ficha.getNombreArchivo_firma());
			certificadoIMG.setCodigoFicha(ficha.getCodigoFicha());
			certificadoIMG.setNombreArchivo_firma(ficha.getNombreArchivo_firma());
			certificadoIMG.setNombreArchivo_firma(ficha.getNombreArchivo_firma());
			certificadoIMG.setUsuarioRegistra(this.getSesUsuarioLogin());
			certificadoIMG.setCodigoUsuarioRegistra(this.getSesCodUsuario());
		}

		///////

	}

	public void seleccionarListaCertificado() {
		System.out.println("seleccionarListaCertificado");

		this.setOperacionEnFicha("M");

		this.setDeshabilitarCodigosBienes("N");

		modoEditable = true;

		ficha = (Ficha) tablaCertificados.getRowData();

		this.setNumeroExpedienteAsignado(ficha.getNumeroExpediente());

		bien = new Bien();

		documento = new Documento();
		// Llenar siglas de la Persona
		llenarSiglasPersona();

		///////

		asignarAnioActual();

		if (ficha.getNumeroExpediente() != null) {

			String[] arregloExpediente = cortarCadenaPorGuion(ficha.getNumeroExpediente());

			String anio = "";
			String expediente = "";
			try {
				expediente = Integer.parseInt(arregloExpediente[0]) + "";
				anio = arregloExpediente[1].trim();

			} catch (NumberFormatException enn) {
				System.out.println(enn.getMessage());
				this.msErrNeg("Debe Ingresar un valor valido para el n�mero de expediente");
				return;
			}
			this.setNumeroExpedienteBuscado(expediente + "-" + anio);

		}

	}

	public void salirFormulario() {
		System.out.println("salirFormulario " + this.getOperacionEnFicha());

		if (this.getOperacionEnFicha().equals("M")) {
			listarBandejaCertificados();
		} else if (!this.getOperacionEnFicha().equals("V")) {
			listarBandejaCertificados();
		}

	}

	public void seleccionarListaVerCertificado() {
		System.out.println("seleccionarListaVerCertificado");

		this.setOperacionEnFicha("V");

		modoEditable = false;

		ficha = (Ficha) tablaCertificados.getRowData();

		bien = new Bien();

		documento = new Documento();

	}

	public void limpiarBandejaCertificados() {
		System.out.println("limpiarBandejaCertificados");

		this.setNumeroCertificado(null);
		this.setFechaListIni(null);
		this.setFechaListFin(null);

		this.setCodigoEstadoCertificado(null);
		this.setCodigoUsuarioRegistro(null);

		this.setNumeroExpediente(null);
		this.setNombreAdministrado(null);
		// WYUCRA16092018
		this.setDescripcionBienBusq(null);
		this.setCodigoBienBusq(null);
		this.setNumeroInformeTecnicoBusq(null);
		this.setAnioInformeBusq(null);
		this.setPaisBusq(null);

	}

	public void listarBandejaCertificados() {

		System.out.println("listarBandejaCertificados");

		this.setCantidadElementosPaginado(0);

		Map<String, Object> parameter = new HashMap<String, Object>();

		int cont = 0;

		if (this.getNumeroCertificado() != null) {
			parameter.put("numeroCertificado", this.getNumeroCertificado());
		} else {
			cont++;
		}

		if (this.getCodigoEstadoCertificado() != null) {
			if (!this.getCodigoEstadoCertificado().equals("")) {
				parameter.put("estadoFicha", this.getCodigoEstadoCertificado().trim());
			} else {
				cont++;
			}
		} else {
			cont++;
		}

		if (this.getCodigoUsuarioRegistro() != null) {
			if (this.getCodigoUsuarioRegistro() != 999) {
				parameter.put("codigoUsuarioRegistrador", this.getCodigoUsuarioRegistro());
			}
		} else {
			cont++;
		}

		///////////////// WYUCRA
		if (this.getSesCodigoPerfil().equals(this.getCtePerfilRegistrador())) {
			parameter.put("codigoUsuarioRegistrador", this.getSesCodUsuario());
		}

		if (this.getNumeroExpediente() != null && !this.getNumeroExpediente().trim().equals("")) {
			parameter.put("numeroExpediente", this.getNumeroExpediente());
		}

		if (this.getNombreAdministrado() != null && !this.getNombreAdministrado().trim().equals("")) {
			parameter.put("nombreAdministrado", this.getNombreAdministrado());
		}

		if (this.getDescripcionBienBusq() != null && !this.getDescripcionBienBusq().trim().equals("")) {
			parameter.put("descripcionBien", this.getDescripcionBienBusq());
		}
		if (this.getCodigoBienBusq() != null && !this.getCodigoBienBusq().trim().equals("")) {
			parameter.put("codigoBien", this.getCodigoBienBusq());
		}
		if (this.getNumeroInformeTecnicoBusq() != null && !this.getNumeroInformeTecnicoBusq().trim().equals("")) {
			parameter.put("numeroInformeTecnico", this.getNumeroInformeTecnicoBusq());
		}
		if (this.getAnioInformeBusq() != null && !this.getAnioInformeBusq().trim().equals("")) {
			parameter.put("anioInforme", this.getAnioInformeBusq());
		}

		if (this.getPaisBusq() != null) {
			parameter.put("codigoPais", this.getPaisBusq());
		}

		////////////////////////

		if (this.getFechaListIni() == null && this.getFechaListFin() == null) {
			cont++; // cad="Las FECHA INICIO - FIN";
		} else {
			if (this.getFechaListIni() != null && this.getFechaListFin() == null) {
				this.msErrNeg("Debe Ingresar la Fecha Fin del expediente");
				return;

			}
			if (this.getFechaListFin() != null && this.getFechaListIni() == null) {
				this.msErrNeg("Debe Ingresar la Fecha Fin del expediente");
				return;

			}

			parameter.put("fechaInicioExpedicion", this.getFechaListIni());
			parameter.put("fechaFinExpedicion", this.getFechaListFin());
		}

		/*
		 * if( this.getNumeroPaisajeBusq() !=null &&
		 * !this.getNumeroPaisajeBusq().trim().equals("")){ try {
		 * parameter.put("numeroFicha",
		 * Integer.parseInt(this.getNumeroPaisajeBusq().trim()) ); }catch
		 * (NumberFormatException enn) { System.out.println(enn.getMessage());
		 * this.msErrNeg("Debe Ingresar un valor Entero en el Numero de Paisaje");
		 * return; }
		 * 
		 * }else{ cont++; }
		 */

		if (cont == 4) {
			this.msErrNeg("No se ha Ingresado los datos para la B�squeda");
			return;
		}

		Factory.getInstance().getFichaBO().buscarFichaXCriterio(parameter);

		// List<BienInmueble> list;
		listaCertificados = (List<Ficha>) parameter.get("resultList");

		if (listaCertificados.size() == 0) {
			this.msErrNeg("No hay resultado para su b�squeda.");
			return;
		}

		this.setCantidadElementosPaginado(listaCertificados.size());

	}

	private String codigoBienesAsignacion;
	private Integer codigoUsuarioRegistroAsignacion;
	private Integer cantidaSolicitadaAsignada;
	private String numeroExpedienteAsignado;

	public String getNumeroExpedienteAsignado() {
		return numeroExpedienteAsignado;
	}

	public void setNumeroExpedienteAsignado(String numeroExpedienteAsignado) {
		this.numeroExpedienteAsignado = numeroExpedienteAsignado;
	}

	public Integer getCantidaSolicitadaAsignada() {
		return cantidaSolicitadaAsignada;
	}

	public void setCantidaSolicitadaAsignada(Integer cantidaSolicitadaAsignada) {
		this.cantidaSolicitadaAsignada = cantidaSolicitadaAsignada;
	}

	public String getCodigoBienesAsignacion() {
		return codigoBienesAsignacion;
	}

	public void setCodigoBienesAsignacion(String codigoBienesAsignacion) {
		this.codigoBienesAsignacion = codigoBienesAsignacion;
	}

	public Integer getCodigoUsuarioRegistroAsignacion() {
		return codigoUsuarioRegistroAsignacion;
	}

	public void setCodigoUsuarioRegistroAsignacion(Integer codigoUsuarioRegistroAsignacion) {
		this.codigoUsuarioRegistroAsignacion = codigoUsuarioRegistroAsignacion;
	}

	public void asignarExpedienteaTecnico() {

		String mensaje = "";

		if (this.getCodigoBienesAsignacion() == null) {
			mensaje += "El CONJUNTO DE C�DIGOS es requerido \n";
		} else {
			if ((this.getCodigoBienesAsignacion().trim()).equals("")) {
				mensaje += "El CONJUNTO DE C�DIGOS es requerido \n";
			}
		}

		if (this.getCodigoUsuarioRegistroAsignacion() == null) {
			mensaje += "El USUARIO A ASIGNAR EL EXPEDIENTE es requerido \n";
		}

		if (!mensaje.equals("")) {
			this.msErrNeg(mensaje);
			return;
		}

		this.setCodigoBienesAsignacion(this.getCodigoBienesAsignacion().trim());

		// inicio de biens

		// validar cantidad de bienes a ingresar size + cantidad de biebens actuales

		int cantidadDetalleBienes = 0;

		List<Integer> listadoDeCodigo = new ArrayList<Integer>();

		Map<String, Object> parameter = null;// new HashMap<String, Object>();
		List<DetalleBien> listDetalleBien;
		/*
		 * parameter = new HashMap<String, Object>();
		 * parameter.put("codigoFicha",ficha.getCodigoFicha());
		 * System.out.println("parameter codigoficha-> "+parameter);
		 * Factory.getInstance().getDetalleBienBO().buscarDetalleBienesXCodigoFicha(
		 * parameter); listDetalleBien= (List<DetalleBien>)parameter.get("resultList");
		 * cantidadDetalleBienes+= listDetalleBien.size() ;
		 */

		// Obtener acntidad de Codigo X ficha Actual

		parameter = new HashMap<String, Object>();
		parameter.put("codigoFicha", ficha.getCodigoFicha());
		System.out.println("parameter codigoficha-> " + parameter);
		Factory.getInstance().getFichaBO().buscarCodigoBienDisponibleXCodigoFicha(parameter);
		List<CodigoHabilitado> listCodigoBienesActuales = (List<CodigoHabilitado>) parameter.get("resultList");
		cantidadDetalleBienes += listCodigoBienesActuales.size();

		System.out.println("cantidadDetalleBienes1-> " + cantidadDetalleBienes);
		System.out.println("this.getDeshabilitarCodigosBienes()-> " + this.getDeshabilitarCodigosBienes());

		// Inicio validar ingreso de conjunto de bienes ////////////////////

		String[] cadenasGuion = cortarCadenaPorComas(this.getCodigoBienesAsignacion());

		for (int i = 0; i < cadenasGuion.length; i++) {
			// System.out.println("conGuion "+cadenasGuion[i]);
			String[] valortexto = cortarCadenaPorGuion(cadenasGuion[i]);
			System.out.println("cant valortexto: " + valortexto.length);
			if (valortexto.length == 1) {
				listadoDeCodigo.add(Integer.parseInt(valortexto[0]));

				cantidadDetalleBienes += 1;
				System.out.println("cantidadDetalleBienes2-> " + cantidadDetalleBienes);
			} else if (valortexto.length == 2) {
				int intervaloInicio = Integer.parseInt(valortexto[0]);
				int intervaloFinal = Integer.parseInt(valortexto[1]);

				if (intervaloInicio >= intervaloFinal) {
					this.msErrNeg("Revise los intervalos de los c�digos de bienes: " + intervaloInicio
							+ " no es menor que " + intervaloFinal);
					return;
				}

				cantidadDetalleBienes += (intervaloFinal - intervaloInicio + 1);

				for (int ini = intervaloInicio; intervaloInicio <= intervaloFinal; ini++) {
					listadoDeCodigo.add(ini);
					intervaloInicio += 1;
				}

				System.out.println("cantidadDetalleBienes3-> " + cantidadDetalleBienes);
			} else {
				this.msErrNeg("Formato incorrecto de bienes");
				return;
			}
		}

		System.out.println("this.getFicha().getCantidadSolicitada()-> " + this.getFicha().getCantidadSolicitada());
		// Validando la cantidad de detallebienes supera a la cantidad solicitada
		if (cantidadDetalleBienes != this.getFicha().getCantidadSolicitada()) {
			this.msErrNeg("La cantidad de bienes ingresados ser�a (" + cantidadDetalleBienes
					+ ") y debe ser la misma que la cantidad solicitada de bienes ("
					+ this.getFicha().getCantidadSolicitada() + ")");
			return;
		}

		// Fin

		/// Consultar que no esten registrador en la base de datos

		// Map<String, Object> parameter = null;// new HashMap<String, Object>();
		// List<DetalleBien> listDetalleBien;

		System.out.println("buscar numeros repetidos!!!");

		List<Integer> listAuxiliar = null;
		listAuxiliar = listadoDeCodigo;
		System.out.println("cantidaDES: " + listAuxiliar.size() + "   " + listadoDeCodigo.size());
		for (Integer numABuscar : listadoDeCodigo) {
			int contador = 0;
			for (Integer numABuscado : listAuxiliar) {

				if (numABuscar.equals(numABuscado)) {
					contador++;
				}

				if (contador > 1) {
					this.msErrNeg("Error en el ingreso de los C�digos de Bienes. Codigo de bien duplicado ");
					return;
				}
			}
		}

		// Buscar que el codigo no se encuentre asignado a un expediente
		for (Integer num : listadoDeCodigo) {
			parameter = new HashMap<String, Object>();
			parameter.put("numeroBien", num);
			Factory.getInstance().getFichaBO().buscarCodigoBienDisponibleParaAsignar(parameter);
			List<CodigoHabilitado> listaCodigos = (List<CodigoHabilitado>) parameter.get("resultList");
			if (listaCodigos.size() > 0) {
				this.msErrNeg(
						"Error en el ingreso de los C�digos de Bienes. ya se encuentra asignado el C�digo de Bien N� "
								+ num + " en el Expediente N� " + listaCodigos.get(0).getNumeroExpediente());
				return;
			}
			/*
			 * Factory.getInstance().getDetalleBienBO().buscarDetalleBienesXNumero(parameter
			 * ); listDetalleBien= (List<DetalleBien>)parameter.get("resultList");
			 * if(listDetalleBien.size()>0){ this.
			 * msErrNeg("Error en el ingreso de los C�digos de Bienes. ya se encuentra registrado el C�digo de Bien # "
			 * +num); return; }
			 */
		}

		// ESTA VALIUDACIONQUITAR

		for (Integer num : listadoDeCodigo) {
			parameter = new HashMap<String, Object>();
			parameter.put("numeroBien", num);
			Factory.getInstance().getDetalleBienBO().buscarDetalleBienesXNumero(parameter);
			listDetalleBien = (List<DetalleBien>) parameter.get("resultList");
			if (listDetalleBien.size() > 0) {
				this.msErrNeg(
						"Error en el ingreso de los C�digos de Bienes. ya se encuentra registrado el C�digo de Bien # "
								+ num);
				return;
			}
		}

		///////////////////////////////////////////////////////////////////////////////////////
		try {

			// Factory.getInstance().getDetalleBienBO().insertarDetalleBien(bien.getCodigoBien(),
			// listadoDeCodigo, this.getSesNroSesion());

			Factory.getInstance().getFichaBO().insertarListadoCodigoHabilitado(ficha.getCodigoFicha(),
					this.getCodigoUsuarioRegistroAsignacion(), listadoDeCodigo, this.getSesNroSesion());

			listarBandejaCertificados();

			listarBandejaAsignacion(ficha.getCodigoFicha());

			this.msOutDBOK("Se realiz� la asignaci�n del expediente N� " + this.getNumeroExpedienteAsignado());

		} catch (Exception e) {
			this.msErrNeg("Error al guardar los datos del formulario de Bienes.");

			System.out.println("Error al guardar los datos del formulario de Bienes. " + e.getMessage());
		}

	}

	public void listarBandejaAsignacion(Integer codigoFicha) {
		System.out.println("listarBandejaAsignacion");

		Map<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("codigoFicha", codigoFicha);
		Factory.getInstance().getFichaBO().buscarCodigoBienDisponibleXCodigoFicha(parameter);
		listAsignacion = (List<CodigoHabilitado>) parameter.get("resultList");

	}

	public void listarBandejaCertificadosTodos() {

		System.out.println("listarBandejaCertificadosTodos");

		this.setCantidadElementosPaginado(0);

		Map<String, Object> parameter = new HashMap<String, Object>();

		int cont = 0;

		if (this.getNumeroCertificado() != null) {
			parameter.put("numeroCertificado", this.getNumeroCertificado());
		} else {
			cont++;
		}

		if (this.getCodigoEstadoCertificado() != null) {
			if (!this.getCodigoEstadoCertificado().equals("")) {
				parameter.put("estadoFicha", this.getCodigoEstadoCertificado().trim());
			} else {
				cont++;
			}
		} else {
			cont++;
		}

		if (this.getCodigoUsuarioRegistro() != null) {
			if (this.getCodigoUsuarioRegistro() != 999) {
				parameter.put("codigoUsuarioRegistrador", this.getCodigoUsuarioRegistro());
			}

		} else {
			cont++;
		}

		if (this.getFechaListIni() == null && this.getFechaListFin() == null) {
			cont++; // cad="Las FECHA INICIO - FIN";
		} else {
			if (this.getFechaListIni() != null && this.getFechaListFin() == null) {
				this.msErrNeg("Debe Ingresar la Fecha Fin del expediente");
				return;

			}
			if (this.getFechaListFin() != null && this.getFechaListIni() == null) {
				this.msErrNeg("Debe Ingresar la Fecha Fin del expediente");
				return;

			}

			parameter.put("fechaInicioExpedicion", this.getFechaListIni());
			parameter.put("fechaFinExpedicion", this.getFechaListFin());
		}

		/*
		 * if(cont==4){ this.msErrNeg("No se ha Ingresado los datos para la B�squeda");
		 * return; }
		 */

		System.out.println("parameter--> " + parameter);
		Factory.getInstance().getFichaBO().buscarFichaXCriterio(parameter);

		// List<BienInmueble> list;
		listaCertificados = (List<Ficha>) parameter.get("resultList");

		if (listaCertificados.size() == 0) {
			this.msErrNeg("No hay resultado para su b�squeda.");
			return;
		}

		this.setCantidadElementosPaginado(listaCertificados.size());

	}

	public void enviarCertificado() {
		System.out.println("enviarCertificado");

		ficha = (Ficha) tablaCertificados.getRowData();

		System.out.println("ficha-> " + ficha);

		try {

			if (ficha.getCantidadAutorizada() == null) {
				this.msErrNeg("Faltan ingresar el detalle de los Bienes del Certificado");
				return;
			}

			// Validando cantidad de Biens a enviar

			int cantidadDetalleBienes = 0;
			Map<String, Object> parameter = null;// new HashMap<String, Object>();
			List<DetalleBien> listDetalleBien;
			parameter = new HashMap<String, Object>();
			parameter.put("codigoFicha", ficha.getCodigoFicha());
			System.out.println("parameter codigoficha-> " + parameter);
			Factory.getInstance().getDetalleBienBO().buscarDetalleBienesXCodigoFicha(parameter);
			listDetalleBien = (List<DetalleBien>) parameter.get("resultList");
			cantidadDetalleBienes += listDetalleBien.size();

//			    	if(cantidadDetalleBienes< ficha.getCantidadAutorizada()){
			if (cantidadDetalleBienes < ficha.getCantidadSolicitada()) {
				if (ficha.getObservacion() == null) {
					this.msErrNeg("Falta ingresar la Observaci�n");
					return;
				} else {
					if (ficha.getObservacion().length() == 0) {
						this.msErrNeg("Falta ingresar la Observaci�n");
						return;
					}
				}

			}

			// validar si se ha registrados los bienes q contendran el certificado
			if (listDetalleBien.size() < 1) {
				this.msErrNeg("Falta ingresar los bienes del certificado");
				return;
			}

			/////////////// verificar que se haya ingreasdo la onservacion

			parameter = new HashMap<String, Object>();
			parameter.put("codigoFicha", ficha.getCodigoFicha());
			Factory.getInstance().getBienBO().buscarBienesXFicha(parameter);
			List<Bien> listaBien = (List<Bien>) parameter.get("resultList");

			if (listaBien.size() == 0) {
				this.msErrNeg("Falta ingresar los bienes del certificado");
				return;
			}

			boolean encontrado = false;

			for (Bien oBien : listaBien) {
				// buscar si hay un conjunto
				System.out.println("buscar: " + oBien.getConjunto());
				if (oBien.getConjunto().indexOf("-") > 0) {
					encontrado = true;
					System.out.println("encontrado intervalo ");
				} else if (oBien.getConjunto().indexOf(",") > 0) {
					encontrado = true;
					System.out.println("encontrado intervalo 2");
				}

				if (encontrado) {

					if (this.ficha.getObservacion() == null || this.ficha.getObservacion().equals("")) {

						this.msErrNeg("Falta ingresar la OBSERVACI�N del certificado");
						return;
					}
					break;
				}
			}

			/////

			Map<String, Object> param = new HashMap<String, Object>();
			param.put("codigoFicha", ficha.getCodigoFicha());
			Factory.getInstance().getDocumentoBO().buscarDocumentoXFicha(param);
			List<Documento> listaDoc = (List<Documento>) param.get("resultList");

			if (listaDoc.size() == 0) {
				this.msErrNeg("El Informe T�cnico es requerido \n");
				return;
			}

			String mensaje = "";
			mensaje = validarDatosExpedienteAEnviar();
			if (!mensaje.equals("")) {
				this.msErrNeg(mensaje);
				return;
			}

			// validar si no tiene observaciones por levantar

			Factory.getInstance().getFichaBO().enviarCertificadoRegistrador(ficha, this.getSesNroSesion());

			// System.out.println("Nro cert "+ficha.getNumeroCertificadoFormato());

			this.msOutDBOK("Se envi� al Supervidor el Certificado N�: " + ficha.getNumeroCertificadoFormato());

			listarBandejaCertificados();

		} catch (Exception ex) {
			System.err.println("Error enviarCertificado()");
			this.msErrNeg("Error enviarCertificado(): " + ex.getMessage());
		}

	}

	public String validarDatosExpedienteAEnviar() {

		String mensaje = "";

		System.out.println("ficha-> " + ficha);

		if (ficha.getTipoPersona() != null && ficha.getTipoPersona().equals("P")) {

			if (ficha.getNacionalidad() == null) {
				mensaje += "La Nacionalidad es requerido \n";
			}

			if (ficha.getNacionalidad() != null && (ficha.getNacionalidad().trim()).equals("")) {
				mensaje += "La Nacionalidad es requerido \n";
			}

		}

		if (ficha.getDomicilio() == null) {
			mensaje += "EL Domicilio es requerido \n";
		}

		return mensaje;
	}

	public void enviarCertificadoSupervisor() {
		System.out.println("enviarCertificadoSupervisor");

		ficha = (Ficha) tablaCertificados.getRowData();

		try {

			// verificar que no tenga observaciones pendientes

			if (ficha.getCodigoFicha() != null) {
				Map<String, Object> parameter = new HashMap<String, Object>();
				parameter.put("codigoFicha", ficha.getCodigoFicha());
				Factory.getInstance().getObservacionHistoricoBO().buscarObservacionHistoricoXFicha(parameter);
				List<ObservacionHistorico> lista = (List<ObservacionHistorico>) parameter.get("resultList");

				//
				boolean encontrado = false;

				for (ObservacionHistorico olista : lista) {
					// buscar si hay un conjunto
					// System.out.println("buscar: "+olista.getEstadoObservacion());
					if (olista.getEstadoObservacion().equals("P")) {
						encontrado = true;
						System.out.println("tiene Observacion pendiente por atender ");
					}

					if (encontrado) {
						this.msErrNeg("El expediente Tiene OBSERVACIONES pendientes por atender");
						return;
					}
				}

				//
			}
			//

			ficha.setUsuarioEvaluador(this.getSesUsuarioLogin());
			ficha.setCodigoUsuarioEvaluador(this.getSesCodUsuario());

			Factory.getInstance().getFichaBO().enviarCertificadoSupervisor(ficha, this.getSesNroSesion());

			System.out.println("Nro cert " + ficha.getNumeroCertificadoFormato());

			this.msOutDBOK("Se envi� al Director el Certificado N�: " + ficha.getNumeroCertificadoFormato());

			listarBandejaCertificados();

		} catch (Exception ex) {
			System.err.println("Error enviarCertificadoSupervisor()");
			this.msErrNeg("Error enviarCertificadoSupervisor(): " + ex.getMessage());
		}

	}

	public void aprobarCertificadoDirector() {
		System.out.println("aprobarCertificadoDirector");

		ficha = (Ficha) tablaCertificados.getRowData();

		try {

			ficha.setUsuarioDirector(this.getSesUsuarioLogin());
			ficha.setCodigoUsuarioDirector(this.getSesCodUsuario());

			Factory.getInstance().getFichaBO().aprobarCertificado(ficha, this.getSesNroSesion());

			System.out.println("Nro cert " + ficha.getNumeroCertificadoFormato());

			this.msOutDBOK("Se aprob� el Certificado N�: " + ficha.getNumeroCertificadoFormato());

			listarBandejaCertificados();

		} catch (Exception ex) {
			System.err.println("Error enviarCertificadoSupervisor()");
			this.msErrNeg("Error enviarCertificadoSupervisor(): " + ex.getMessage());
		}

	}

	// private List<TipoTaller> lSoloUsuariosRegistro = new ArrayList<TipoTaller>();

	@SuppressWarnings("unchecked")
	public SelectItem[] getListarSoloUsuariosRegistro() {

		Map<String, Object> parameter = new HashMap<String, Object>();

		Factory.getInstance().getFichaBO().buscarUsuariosRegistradoresFicha(parameter);
		List<Ficha> lista;
		lista = (List<Ficha>) parameter.get("resultList");

		SelectItem[] items = null;

		int i = 0;
		if (lista.size() > 0) {

			int tam = (lista.size() + 2);

			items = new SelectItem[tam];
			i = 2;
			items[0] = new SelectItem("", "Seleccionar");
			items[1] = new SelectItem(999, "TODOS");
		} else {
			int tam = (lista.size() + 1);

			items = new SelectItem[tam];
			i = 1;
			items[0] = new SelectItem("", "Seleccionar");
		}

		for (Ficha tt : lista) {
			items[i++] = new SelectItem(tt.getCodigoUsuarioRegistrador(), tt.getUsuarioRegistrador());
		}

		return items;
	}

	@SuppressWarnings("unchecked")
	public SelectItem[] getListarSoloUsuariosRegistroAsignacion() {

		Map<String, Object> parameter = new HashMap<String, Object>();

		Factory.getInstance().getFichaBO().buscarUsuariosRegistradoresFicha(parameter);
		List<Ficha> lista;
		lista = (List<Ficha>) parameter.get("resultList");

		SelectItem[] items = null;

		int i = 0;
		/*
		 * if(lista.size()>0){
		 * 
		 * int tam= (lista.size()+2);
		 * 
		 * items = new SelectItem[tam]; i = 2; items[0] = new
		 * SelectItem("","Seleccionar"); items[1] = new SelectItem(999,"TODOS"); }else{
		 */
		int tam = (lista.size() + 1);

		items = new SelectItem[tam];
		i = 1;
		items[0] = new SelectItem("", "Seleccionar");
		// }

		for (Ficha tt : lista) {
			items[i++] = new SelectItem(tt.getCodigoUsuarioRegistrador(), tt.getUsuarioRegistrador());
		}

		return items;
	}

	@SuppressWarnings("unchecked")
	public SelectItem[] getListarHorarios(Integer ncodusuario) {
		// SelectItem[]
		System.out.println("COdigo de usuario ->>>>>>>>>>>" + ncodusuario);
		Map<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("ncodusuario", ncodusuario);
		Factory.getInstance().getFichaBO().buscarHorarios(parameter);
		List<Ficha> lista;
		lista = (List<Ficha>) parameter.get("resultList");

		SelectItem[] items = null;

		int i = 0;
		/*
		 * if(lista.size()>0){
		 * 
		 * int tam= (lista.size()+2);
		 * 
		 * items = new SelectItem[tam]; i = 2; items[0] = new
		 * SelectItem("","Seleccionar"); items[1] = new SelectItem(999,"TODOS"); }else{
		 */
		int tam = (lista.size() + 1);

		items = new SelectItem[tam];
		i = 1;
		items[0] = new SelectItem("", "Seleccionar");
		// }

		for (Ficha tt : lista) {
			items[i++] = new SelectItem(tt.getCodigoUsuarioRegistrador(), tt.getUsuarioRegistrador());
		}
		listHorarios = items;

		return items;
	}

	@SuppressWarnings("unchecked")
	public static SelectItem[] getListarSoloHorarios() {

		Map<String, Object> parameter = new HashMap<String, Object>();

		Factory.getInstance().getFichaBO().buscarUsuariosRegistradoresFicha(parameter);
		List<Ficha> lista;
		lista = (List<Ficha>) parameter.get("resultList");

		SelectItem[] items = null;

		int i = 0;
		/*
		 * if(lista.size()>0){
		 * 
		 * 
		 * int tam= (lista.size()+2);
		 * 
		 * items = new SelectItem[tam]; i = 2; items[0] = new
		 * SelectItem("","Seleccionar"); items[1] = new SelectItem(999,"TODOS"); }else{
		 */
		int tam = (lista.size() + 1);

		items = new SelectItem[tam];
		i = 1;
		items[0] = new SelectItem("", "Seleccionar");
		// }

		for (Ficha tt : lista) {
			items[i++] = new SelectItem(tt.getCodigoUsuarioRegistrador(), tt.getUsuarioRegistrador());
		}

		return items;
	}

	public static HttpServletResponse getResponse() {
		return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
	}

	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}

	public static FacesContext getContext() {
		return FacesContext.getCurrentInstance();
	}

	public void inicializarMostrarObservaciones() {
		observacion = new ObservacionHistorico();

		this.setCodigoFicha(((Ficha) tablaCertificados.getRowData()).getCodigoFicha());

		this.setMostrarBotonModificarObservacion("N");

		this.setMostrarAgregarObservaciones("S");
	}

	private String mostrarAgregarObservaciones;

	public String getMostrarAgregarObservaciones() {
		return mostrarAgregarObservaciones;
	}

	public void setMostrarAgregarObservaciones(String mostrarAgregarObservaciones) {
		this.mostrarAgregarObservaciones = mostrarAgregarObservaciones;
	}

	public void verObservaciones() {
		observacion = new ObservacionHistorico();

		this.setCodigoFicha(((Ficha) tablaCertificados.getRowData()).getCodigoFicha());

		this.setMostrarBotonModificarObservacion("N");

		this.setMostrarAgregarObservaciones("N");

	}

	public void generarReporteCertificadoFicha() throws IOException {

		Map<String, Object> parameter = new HashMap<String, Object>();
		HttpServletResponse response = FichaBean.getResponse();
		HttpServletRequest request = FichaBean.getRequest();

		FacesContext context = FacesContext.getCurrentInstance();
		Map params = context.getExternalContext().getRequestParameterMap();

		Integer codigoFicha = new Integer((String) params.get("codigoFicha"));
		Integer cantidadSolicitada = new Integer((String) params.get("cantidadSolicitada"));
		Integer cantidadAutorizada = new Integer((String) params.get("cantidadAutorizada"));
		String tipoPersona = (String) params.get("tipoPersona");

		parameter = new HashMap<String, Object>();

		// parameter.put("numeroCertificado", nroCertificado);

		try {
			/*
			 * try{
			 * 
			 * Factory.getInstance().getFichaGeneralBO().buscarFichaGeneralPorNroCertificado
			 * (parameter);
			 * 
			 * }catch(Exception e){ System.out.println(e.getStackTrace()); }
			 */

			// List<Map<String,Object>> list = (List<Map<String,
			// Object>>)parameter.get("resultList");

			/*
			 * if(list.size()<=0){ // System.out.println("no hay resultados pal reporte");
			 * FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,
			 * Constants.REPORTE_SIN_DATOS, null);
			 * FacesContext.getCurrentInstance().addMessage(null, facesMessage); return; }
			 */
			// parameter.put("P_NRODOCUMENTO", numeroIdentificacion);

			// String cantidadSolicitadaTexto= convertirALetras(cantidadSolicitada);

			// System.out.println("textocantidadSolicitada
			// "+convertirALetras(cantidadSolicitada) );
			String cantidadSolicitadaTexto = convertirNumeroALetras(cantidadSolicitada);
			cantidadSolicitadaTexto = cantidadSolicitadaTexto.toLowerCase();
			System.out.println("cantidadSolicitadaTexto--> " + cantidadSolicitadaTexto);

			// La cadena que queremos transformar
			String mayusculaCS = cantidadSolicitadaTexto.charAt(0) + "";
			// Obtenemos el primer caracteres y concatenamos "" para que se transforme el
			// char en String
			mayusculaCS = mayusculaCS.toUpperCase();// Lue... lo transformamos en may�scula
			cantidadSolicitadaTexto = cantidadSolicitadaTexto.replaceFirst(cantidadSolicitadaTexto.charAt(0) + "",
					mayusculaCS);
			//
			// String cantidadAutorizadaTexto= convertirALetras(cantidadAutorizada);

			// System.out.println("textocantidadAutorizada
			// "+convertirALetras(cantidadAutorizada) );
			String cantidadAutorizadaTexto = convertirNumeroALetras(cantidadAutorizada);
			cantidadAutorizadaTexto = cantidadAutorizadaTexto.toLowerCase();
			System.out.println("cantidadAutorizadaTexto--> " + cantidadAutorizadaTexto);

			// La cadena que queremos transformar
			String mayusculaCA = cantidadAutorizadaTexto.charAt(0) + "";
			// Obtenemos el primer caracteres y concatenamos "" para que se transforme el
			// char en String
			mayusculaCA = mayusculaCA.toUpperCase();// Lue... lo transformamos en may�scula
			cantidadAutorizadaTexto = cantidadAutorizadaTexto.replaceFirst(cantidadAutorizadaTexto.charAt(0) + "",
					mayusculaCA);

			List<Bien> lista = new ArrayList<Bien>();
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("codigoFicha", codigoFicha);
			Factory.getInstance().getBienBO().buscarBienesXFicha(param);
			lista = (List<Bien>) param.get("resultList");

			Integer cantFotografias = lista.size();
			Integer cantFolios = 0;
			if (cantFotografias % 2 == 0) {// par
				cantFolios = cantFotografias / 2;
			} else {// impar
				cantFolios = (cantFotografias / 2) + 1;
			}

			// String cantFotografiasTexto= convertirALetras(cantFotografias);

			String cantFotografiasTexto = convertirNumeroALetras(cantFotografias);
			cantFotografiasTexto = cantFotografiasTexto.toLowerCase();
			// System.out.println("cantFotografiasTexto--> "+cantFotografiasTexto);

			// La cadena que queremos transformar
			String mayusculaFoto = cantFotografiasTexto.charAt(0) + "";
			// Obtenemos el primer caracteres y concatenamos "" para que se transforme el
			// char en String
			mayusculaFoto = mayusculaFoto.toUpperCase();// Lue... lo transformamos en may�scula
			cantFotografiasTexto = cantFotografiasTexto.replaceFirst(cantFotografiasTexto.charAt(0) + "",
					mayusculaFoto);
			//
			// System.out.println("texto "+convertirALetras(cantFolios) );
			String cantFoliosTexto = convertirNumeroALetras(cantFolios);
			cantFoliosTexto = cantFoliosTexto.toLowerCase();
			System.out.println("cantFoliosTexto--> " + cantFoliosTexto);
			// La cadena que queremos transformar
			/*
			 * String mayusculaFolio=cantFoliosTexto.charAt(0)+""; //Obtenemos el primer
			 * caracteres y concatenamos "" para que se transforme el char en String
			 * mayusculaFolio=mayusculaFolio.toUpperCase();//Lue... lo transformamos en
			 * may�scula cantFoliosTexto=cantFoliosTexto.replaceFirst
			 * (cantFoliosTexto.charAt(0)+"", mayusculaFolio);
			 */

			parameter.put("PI_CANTIDADFOTOGRAFIA", cantFotografias);
			parameter.put("PI_CANTIDADFOLIOS", cantFolios);
			parameter.put("PI_TXTCANTIDADFOTOGRAFIA", cantFotografiasTexto);
			parameter.put("PI_TXTCANTIDADFOLIOS", cantFoliosTexto);

			parameter.put("PI_SCANTIDADSOLICITADA", cantidadSolicitadaTexto);
			parameter.put("PI_SCANTIDADAUTORIZADA", cantidadAutorizadaTexto);
			parameter.put("P_CODIGOFICHA", codigoFicha);
			// parameter.put("P_IMAGENBLANCO",
			// request.getRequestURL().substring(0,(request.getRequestURL().lastIndexOf("/"))).concat("../../../common/images/"));
			parameter.put("P_DIRECTORIO_IMAGENES", request.getRequestURL()
					.substring(0, (request.getRequestURL().lastIndexOf("/"))).concat("../../../common/images/"));
			// System.out.println(""+this.getSesCodigoPerfil()+" "+
			// this.getSesCodigoPerfil());
			// parameter.put("P_TIPOREPORTE", tipoReporte);//solo si es usuario master pasar
			// O
			// parameter.put("P_FECHA", fecha);
			// parameter.put("P_HORA", hora);
			System.out.println("mandadno -> " + parameter + " --  ");

			try {
				ServletContext application = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
						.getContext();
				parameter.put("SUBREPORT_DIR", application.getRealPath("") + "\\reports\\");

				System.out.println("tipoPersona --> " + tipoPersona);
				/*
				 * if(tipoPersona.equals("P")){
				 * UtilReporte.getInstance().imprimir("reportCertificadoCultural",1,
				 * parameter,application, response,1); }else{
				 * UtilReporte.getInstance().imprimir("reportCertificadoCulturalPJ",1,
				 * parameter,application, response,1); }
				 */

				UtilReporte.getInstance().imprimir("reportBasePN", 1, parameter, application, response, 1);

			} catch (Exception e) {
				e.notify();
				System.out.println("Busq caida reporte " + e.getMessage());
			}
			// Reporte.vistaPreviaWeb("reportCertificadoCultural.jrxml",
			// "CertificadoCultural",list, parameter, response,"1");
		} catch (Exception e) {
			e.notify();

		}

		FichaBean.getContext().responseComplete();

	}

	public void seleccionarListaObservacion() {
		System.out.println("seleccionarListaObservacion");

		// ObservacionHistorico obs =null;

		if (tablaObservaciones.isRowAvailable()) {
			observacion = (ObservacionHistorico) tablaObservaciones.getRowData();
		}

		System.out.println("seleccionarPatrimonioTangible " + observacion);

		this.setMostrarBotonModificarObservacion("S");

	}

	public void eliminarListaObservacion() {
		System.out.println("eliminarListaObservacion");

		ObservacionHistorico obs = null;
		try {
			if (tablaObservaciones.isRowAvailable()) {
				obs = (ObservacionHistorico) tablaObservaciones.getRowData();
			}

			obs.setActivo("N");

			Factory.getInstance().getObservacionHistoricoBO().eliminarObservacionHistorico(obs, this.getSesNroSesion());

			System.out.println("obs " + obs);

		} catch (Exception ex) {
			System.err.println("Error eliminarListaObservacion " + ex.getMessage() + " obs-> " + obs);
			this.msErrNeg("Error eliminarListaObservacion ");
		}

	}

	public String validarFichasObservaciones() {

		String mensaje = "";

		if (observacion.getDescripcion() == null) {
			mensaje += "La DESCRIPCI�N DE LA OBSERVACI�N es requerido \n";
		} else {
			if ((observacion.getDescripcion().trim()).equals("")) {
				mensaje += "La DESCRIPCI�N DE LA OBSERVACI�N es requerido \n";
			}
		}

		return mensaje;
	}

	public void guardarFichasObservaciones() {
		System.out.println("guardarFichasObservaciones");

		String mensaje = "";
		mensaje = validarFichasObservaciones();
		if (!mensaje.equals("")) {
			this.msErrNeg(mensaje);
			return;
		}
		Ficha oFicha = null;
		// System.out.println("dcfdf "+this.getCodigoFicha());

		// System.out.println("paso validadiocj "+observacion);
		try {

			ObservacionHistorico oObservacion = new ObservacionHistorico(this.getCodigoFicha(),
					observacion.getDescripcion(), observacion.getFechaObservacion(), this.getSesCodUsuario(),
					this.getSesUsuarioLogin());
			// System.out.println("oObservacion11111111 + "+oObservacion);
			Factory.getInstance().getObservacionHistoricoBO().insertarObservacionHistorico(oObservacion,
					this.getSesNroSesion());
			// System.out.println("oObservacion + "+oObservacion);
			// oFicha= new Ficha(this.getCodigoFicha());
			// Factory.getInstance().getFichaBO().observarCertificado(oFicha,
			// this.getSesNroSesion());

			observacion = new ObservacionHistorico();

			// listarBandejaCertificadosTodos();

			this.msOutDBOK("Se registr� satisfactoriamente");

		} catch (Exception ex) {
			System.err.println("Error guardarFichasObservaciones " + ex.getMessage() + "oFicha-> " + oFicha);
			this.msErrNeg("Error guardarFichasObservaciones ");
		}

	}

	public void enviarFichasObservaciones() {
		Ficha oFicha = new Ficha(this.getCodigoFicha());

		try {

			if (this.getCodigoFicha() != null) {
				Map<String, Object> parameter = new HashMap<String, Object>();
				parameter.put("codigoFicha", this.getCodigoFicha());
				Factory.getInstance().getObservacionHistoricoBO().buscarObservacionHistoricoXFicha(parameter);
				;
				List<ObservacionHistorico> lista = (List<ObservacionHistorico>) parameter.get("resultList");

				//
				boolean encontrado = false;

				for (ObservacionHistorico olista : lista) {
					// buscar si hay un conjunto
					// System.out.println("buscar: "+olista.getEstadoObservacion());
					if (olista.getEstadoObservacion().equals("P")) {
						encontrado = true;
						System.out.println("tiene Observacion pendiente por atender ");
					}
				}

				if (!encontrado) {
					this.msErrNeg(
							"No se puede pasar a OBSERVADO, porque el expediente no Tiene OBSERVACIONES por atender");
					return;
				}
				//
			}

			Factory.getInstance().getFichaBO().observarCertificado(oFicha, this.getSesNroSesion());

			observacion = new ObservacionHistorico();

			this.msOutDBOK("EL Expediente paso a OBSERVADO y se env�o al T�CNICO responsable");

			listarBandejaCertificadosTodos();

		} catch (Exception e) {
			// TODO: handle exception

			System.out.println("ERROR " + e.getMessage());
		}

	}

	public void modificarFichasObservaciones() {
		System.out.println("modificarFichasObservaciones");

		String mensaje = "";
		mensaje = validarFichasObservaciones();
		if (!mensaje.equals("")) {
			this.msErrNeg(mensaje);
			return;
		}

		try {

			ObservacionHistorico oObservacion = new ObservacionHistorico(observacion.getCodigoObservacion(),
					observacion.getDescripcion(), null);
			System.out.println("oObservacion--><< " + oObservacion);
			Factory.getInstance().getObservacionHistoricoBO().modificarObservacionHistorico(oObservacion,
					this.getSesNroSesion());

			observacion = new ObservacionHistorico();

			this.setMostrarBotonModificarObservacion("N");
			this.setMostrarAgregarObservaciones("S");

			// listarBandejaCertificados();
			this.msOutDBOK("Se modific� satisfactoriamente");

		} catch (Exception ex) {
			System.err.println("Error modificarFichasObservaciones");
			this.msErrNeg("Error modificarFichasObservaciones): " + ex.getMessage());
		}
	}

	private String mostrarBotonModificarObservacion;

	public String getMostrarBotonModificarObservacion() {
		return mostrarBotonModificarObservacion;
	}

	public void setMostrarBotonModificarObservacion(String mostrarBotonModificarObservacion) {
		this.mostrarBotonModificarObservacion = mostrarBotonModificarObservacion;
	}

	public void limpiarFichasObservaciones() {
		System.out.println("limpiarFichasObservaciones");

		observacion = new ObservacionHistorico();

		this.setMostrarBotonModificarObservacion("S");

	}

	private Integer codigoFicha;

	public Integer getCodigoFicha() {
		return codigoFicha;
	}

	public void setCodigoFicha(Integer codigoFicha) {
		this.codigoFicha = codigoFicha;
	}

	/*
	 * public void listarHistorialObservacionesXFicha(){
	 * 
	 * Map<String, Object> parameter = new HashMap<String, Object>();
	 * 
	 * parameter.put("codigoFicha", this.getCodigoFicha());
	 * 
	 * System.out.println("listarHistorialObservacionesXFicha parameter--> "
	 * +parameter);
	 * 
	 * Factory.getInstance().getObservacionHistoricoBO().
	 * buscarObservacionHistoricoXFicha(parameter);
	 * 
	 * //List<BienInmueble> list; listaObservaciones=
	 * (List<ObservacionHistorico>)parameter.get("resultList");
	 * 
	 * if(listaObservaciones.size()==0){
	 * this.msErrNeg("No hay Observaciones a mostrar."); return; }
	 * 
	 * 
	 * }
	 */

	String unidades[] = { "cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve", "diez" };
	String especiales[] = { "once", "doce", "trece", "catorce", "quince", "diezciseis", "diecisiete", "dieciocho",
			"diecinueve" };
	String decenas[] = { "veinte", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa" };

	public String convertirALetras(Integer numero) {

		String cadena = "";
		int num = numero;

		if (num >= 0 && num < 11)
			cadena = unidades[num];
		else if (num < 20)
			cadena = especiales[num - 11];
		else if (num < 100) {
			int unid = num % 10;
			int dec = num / 10;
			if (unid == 0)
				cadena = decenas[dec - 2];
			else
				cadena = decenas[dec - 2] + " y " + unidades[unid];
		} else
			System.out.print("El numero debe ser menor a 100");
		return cadena;
	}

	/////////////////////

	private String convertirNumeroALetras(Integer _counter) {
		// Limite
		if (_counter > 2000000)
			return "Dos Millones";

		switch (_counter) {
		case 0:
			return "Cero";
		case 1:
			return "Uno"; // UNO
		case 2:
			return "Dos";
		case 3:
			return "Tres";
		case 4:
			return "Cuatro";
		case 5:
			return "Cinco";
		case 6:
			return "Seis";
		case 7:
			return "Siete";
		case 8:
			return "Ocho";
		case 9:
			return "Nueve";
		case 10:
			return "Diez";
		case 11:
			return "Once";
		case 12:
			return "Doce";
		case 13:
			return "Trece";
		case 14:
			return "Catorce";
		case 15:
			return "Quince";
		case 20:
			return "Veinte";
		case 30:
			return "Treinta";
		case 40:
			return "Cuarenta";
		case 50:
			return "Cincuenta";
		case 60:
			return "Sesenta";
		case 70:
			return "Setenta";
		case 80:
			return "Ochenta";
		case 90:
			return "Noventa";
		case 100:
			return "Cien";

		case 200:
			return "Doscientos";
		case 300:
			return "Trescientos";
		case 400:
			return "Cuatrocientos";
		case 500:
			return "Quinientos";
		case 600:
			return "Seiscientos";
		case 700:
			return "Setecientos";
		case 800:
			return "Ochocientos";
		case 900:
			return "Novecientos";

		case 1000:
			return "Mil";

		case 1000000:
			return "Un Mill�n";
		case 2000000:
			return "Dos Millones";
		}
		if (_counter < 20) {
			// System.out.println(">15");
			return "Dieci" + convertirNumeroALetras(_counter - 10);
		}
		if (_counter < 30) {
			// System.out.println(">20");
			return "Veinti" + convertirNumeroALetras(_counter - 20);
		}
		if (_counter < 100) {
			// System.out.println("<100");
			return convertirNumeroALetras((int) (_counter / 10) * 10) + " Y " + convertirNumeroALetras(_counter % 10);
		}
		if (_counter < 200) {
			// System.out.println("<200");
			return "Ciento " + convertirNumeroALetras(_counter - 100);
		}
		if (_counter < 1000) {
			// System.out.println("<1000");
			return convertirNumeroALetras((int) (_counter / 100) * 100) + " " + convertirNumeroALetras(_counter % 100);
		}
		if (_counter < 2000) {
			// System.out.println("<2000");
			return "Mil " + convertirNumeroALetras(_counter % 1000);
		}
		if (_counter < 1000000) {
			String var = "";
			// System.out.println("<1000000");
			var = convertirNumeroALetras((int) (_counter / 1000)) + " Mil";
			if (_counter % 1000 != 0) {
				// System.out.println(var);
				var += " " + convertirNumeroALetras(_counter % 1000);
			}
			return var;
		}
		if (_counter < 2000000) {
			return "Un Mill�n " + convertirNumeroALetras(_counter % 1000000);
		}
		return "";
	}

	public void inicializarBusqueda() {
		System.out.println("inicializarBusqueda");

		this.setCodigoUsuarioRegistro(999);

		listarBandejaCertificados();

	}

	public void seleccionarTipoDocumento() {
		System.out.println("seleccionarTipoDocumento");

		if (this.ficha.getTipoPersona() != null && this.ficha.getTipoPersona().equals("E")) {
			this.ficha.setTipoDocumento("RUC");
		} else {
			if (this.ficha.getTipoPersona() != null && this.ficha.getTipoPersona().equals("P")) {
				this.ficha.setTipoDocumento("DNI");
			}

		}

	}

	public void inicializarAsignarCertificadoATecnico() {
		System.out.println("inicializarAsignarCertificadoATecnico");

		this.setNumeroExpedienteAsignado(null);
		this.setCantidaSolicitadaAsignada(null);
		this.setCodigoUsuarioRegistroAsignacion(null);
		this.setCodigoBienesAsignacion(null);

		ficha = (Ficha) tablaCertificados.getRowData();

		System.out.println("ficha--> " + ficha);

		this.setNumeroExpedienteAsignado(ficha.getNumeroExpediente());
		this.setCantidaSolicitadaAsignada(ficha.getCantidadSolicitada());
		this.setCodigoUsuarioRegistroAsignacion(ficha.getCodigoUsuarioRegistrador());
		// this.setCodigoBienesAsignacion(ficha.getCodigoUsuarioRegistrador());

		listarBandejaAsignacion(ficha.getCodigoFicha());
		// getListarHorarios(ficha.getCodigoUsuarioRegistrador());

	}

	public void eliminarListaAsignacion() {
		System.out.println("eliminarListaAsignacion");
		try {
			CodigoHabilitado oCodigoHabilitado = (CodigoHabilitado) tablaAsignacion.getRowData();
			System.out.println("oCodigoHabilitado " + oCodigoHabilitado);

			// if(oCodigoHabilitado.getUtilizado().equals("N") ){
			oCodigoHabilitado.setActivo("N");
			Factory.getInstance().getFichaBO().eliminarCodigoHabilitadoPorCodigo(oCodigoHabilitado,
					this.getSesNroSesion());
			/*
			 * }else{ this.msErrNeg("El Codigo a eliminar ya esta utilizado"); return; }
			 */

		} catch (Exception ex) {
			System.err.println("Error eliminarListaAsignacion: " + ex);
			this.msErrNeg("Error eliminarListaAsignacion: " + ex.getMessage());
		}

	}

	public void eliminarAsignacionaTecnico() {
		System.out.println("eliminarAsignacionaTecnico");
		try {
			CodigoHabilitado oCodigoHabilitado = new CodigoHabilitado();
			oCodigoHabilitado.setCodigoFicha(this.ficha.getCodigoFicha());
			oCodigoHabilitado.setActivo("N");
			Factory.getInstance().getFichaBO().eliminarTodaAsignacionXFicha(oCodigoHabilitado, this.getSesNroSesion());

			this.setCodigoBienesAsignacion(null);
			listarBandejaAsignacion(ficha.getCodigoFicha());

		} catch (Exception ex) {
			System.err.println("Error eliminarListaAsignacion: " + ex);
			this.msErrNeg("Error eliminarListaAsignacion: " + ex.getMessage());
		}

	}

	public void buscarBandejaAsignacion() {
		System.out.println("buscarBandejaAsignacion ");

		listarBandejaAsignacion(ficha.getCodigoFicha());

	}

	private List<SelectItem> listarTipoDocumentoXTipoPersona;

	public List<SelectItem> getListarTipoDocumentoXTipoPersona() {

		listarTipoDocumentoXTipoPersona = new ArrayList<SelectItem>();
		listarTipoDocumentoXTipoPersona.add(new SelectItem("", "-- Seleccione --"));

		if (this.ficha.getTipoPersona() != null) {

			if (this.ficha.getTipoPersona().equals("E")) {
				listarTipoDocumentoXTipoPersona.add(new SelectItem("RUC", "R.U.C."));
			} else if (this.ficha.getTipoPersona().equals("P")) {
				listarTipoDocumentoXTipoPersona.add(new SelectItem("CEE", "Carn� de Extranjer�a"));
				listarTipoDocumentoXTipoPersona.add(new SelectItem("DNI", "D.N.I."));
				listarTipoDocumentoXTipoPersona.add(new SelectItem("PAS", "Pasaporte"));
			} /*
				 * else if(this.ficha.getTipoPersona().equals("P")){
				 * listarTipoDocumentoXTipoPersona.add(new SelectItem("CEE",
				 * "Carn� de Extranjer�a")); //listarTipoDocumentoXTipoPersona.add(new
				 * SelectItem("RUC", "R.U.C.")); listarTipoDocumentoXTipoPersona.add(new
				 * SelectItem("PAS", "Pasaporte")); }
				 */

			/*
			 * if(this.getSesCodigoPerfil()!=this.getCtePerfilMantenimiento()){
			 * listarTipoDocumentoXTipoPersona.add(new SelectItem("C", "Creado")); }
			 * listarTipoDocumentoXTipoPersona.add(new SelectItem("P", "Pendiente"));
			 * listarTipoDocumentoXTipoPersona.add(new SelectItem("N", "Rechazado")); return
			 * listarTipoDocumentoXTipoPersona; }
			 */

			/*
			 * <!-- <h:selectOneMenu value="#{fichaBean.ficha.tipoDocumento}"
			 * id="txtTipoDocumento"
			 * disabled="#{!fichaBean.modoEditable ||  usuarioBean.sesCodigoPerfil == usuarioBean.ctePerfilRegistrador}"
			 * > <f:selectItem itemValue="" itemLabel="Seleccione" /> <f:selectItem
			 * itemValue="CEE" itemLabel="Carn� de Extranjer�a" /> <f:selectItem
			 * itemValue="DNI" itemLabel="D.N.I." /> <f:selectItem itemValue="PAS"
			 * itemLabel="Pasaporte" /> <f:selectItem itemValue="RUC" itemLabel="R.U.C." />
			 * </h:selectOneMenu> -->
			 */

		}
		return listarTipoDocumentoXTipoPersona;
	}

	public void inicializarNoAtencion() {
		System.out.println("inicializarNoAtencion");
	}

	public String validarMotivoNoAtencion() {

		String mensaje = "";

		if (this.ficha.getMotivoNoAtencion() == null) {
			mensaje += "El motivo es requerido \n";
		} else if (this.ficha.getMotivoNoAtencion().equals("")) {
			mensaje += "El motivo es requerido \n";
		}

		Map<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("codigoFicha", ficha.getCodigoFicha());
		Factory.getInstance().getDocumentoBO().buscarDocumentoXFicha(parameter);
		List<Documento> listaDoc = (List<Documento>) parameter.get("resultList");

		if (listaDoc.size() == 0) {
			mensaje += "El Informe T�cnico es requerido \n";
		}

		/*
		 * if( listaDocumento.size()==0){ mensaje+="El Informe T�cnico es requerido \n";
		 * }
		 */
		System.out.println("this.ficha.getMotivoNoAtencion() " + this.ficha.getMotivoNoAtencion());

		if (this.ficha.getMotivoNoAtencion().equals("D")) {

			Map<String, Object> parameter1 = new HashMap<String, Object>();
			parameter1.put("codigoFicha", ficha.getCodigoFicha());
			Factory.getInstance().getBienBO().buscarBienesXFicha(parameter1);
			List<Bien> listaBien = (List<Bien>) parameter1.get("resultList");

			if (listaBien.size() > 0) {
				mensaje += "Se debe eliminar la lista de descripcion de bienes \n";
			}
		}

		return mensaje;
	}

	public void guardarMotivoNoAtencion() {
		System.out.println("guardarMotivoNoAtencion");

		try {

			String mensaje = "";
			mensaje = validarMotivoNoAtencion();
			if (!mensaje.equals("")) {

				this.ficha.setMotivoNoAtencion(null);
				this.msErrNeg(mensaje);
				return;
			}
			System.out.println("paso validarMotivoNoAtencion");

			Factory.getInstance().getFichaBO().modificarRegistroNoAtencion(ficha, this.getSesNroSesion());

			this.msOutDBOK("Se Registr� el Motivo de la No atenci�n al Expediente: " + ficha.getNumeroExpediente());

		} catch (Exception e) {
			this.msErrNeg("Error al guardar el Motivo de No Atenci�n.");
			System.out.println("Error al modificarRegistroNoAtencion " + e.getMessage());
		}

	}

	public void enviarCertificadoDenegadoADirector() {
		System.out.println("enviarCertificadoDenegadoADirector");

		ficha = (Ficha) tablaCertificados.getRowData();

		// System.out.println("ficha-> "+ficha);

		try {
			/*
			 * if(cantidadDetalleBienes< ficha.getCantidadAutorizada()){ this.
			 * msErrNeg("La cantidad de Bienes registrados es menor a la cantidad de bienes autorizados"
			 * ); return; }
			 * 
			 * // validar si se ha registrados los bienes q contendran el certificado
			 * if(listDetalleBien.size()<1){
			 * this.msErrNeg("Falta ingresar los bienes del certificado"); return; }
			 * 
			 * 
			 * String mensaje=""; mensaje =validarDatosExpedienteAEnviar();
			 * if(!mensaje.equals("")){ this.msErrNeg(mensaje); return; }
			 */

			// validar si no tiene observaciones por levantar

			// Factory.getInstance().getFichaBO().enviarADirectorDenegarCertificado(ficha,
			// this.getSesNroSesion());

			Factory.getInstance().getFichaBO().enviarCertificadoSupervisor(ficha, this.getSesNroSesion());

			// System.out.println("Nro cert "+ficha.getNumeroCertificadoFormato());

			this.msOutDBOK("Se envi� al Director el Expediente: " + ficha.getNumeroExpediente());

			listarBandejaCertificados();

		} catch (Exception ex) {
			System.err.println("Error enviarCertificado()");
			this.msErrNeg("Error enviarCertificado(): " + ex.getMessage());
		}

	}

	public void denegarCertificadoDirector() {
		System.out.println("denegarCertificadoDirector");

		ficha = (Ficha) tablaCertificados.getRowData();

		System.out.println("ficha-> " + ficha);

		try {
			/*
			 * if(cantidadDetalleBienes< ficha.getCantidadAutorizada()){ this.
			 * msErrNeg("La cantidad de Bienes registrados es menor a la cantidad de bienes autorizados"
			 * ); return; }
			 * 
			 * // validar si se ha registrados los bienes q contendran el certificado
			 * if(listDetalleBien.size()<1){
			 * this.msErrNeg("Falta ingresar los bienes del certificado"); return; }
			 * 
			 * 
			 * String mensaje=""; mensaje =validarDatosExpedienteAEnviar();
			 * if(!mensaje.equals("")){ this.msErrNeg(mensaje); return; }
			 */

			// validar si no tiene observaciones por levantar

			Factory.getInstance().getFichaBO().enviarADirectorDenegarCertificado(ficha, this.getSesNroSesion());

			// System.out.println("Nro cert "+ficha.getNumeroCertificadoFormato());

			this.msOutDBOK("Se archiv� el Expediente: " + ficha.getNumeroExpediente());

			listarBandejaCertificados();

		} catch (Exception ex) {
			System.err.println("Error enviarCertificado()");
			this.msErrNeg("Error enviarCertificado(): " + ex.getMessage());
		}

	}

	public void seleccionarItems() {

		Documento lcomprob = null;

		Documento oCompr = new Documento();

		oCompr = (Documento) tablaDocumento.getRowData();

		System.out.println("Selecciona " + oCompr.getCodigoDocumento());

		Map<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("codigoFicha", ficha.getCodigoFicha());
		Factory.getInstance().getDocumentoBO().buscarDocumentoXFicha(parameter);
		List<Documento> listaDoc = (List<Documento>) parameter.get("resultList");

		if (listaDoc.size() > 1) {

			try {

				Documento oDocumento = null;
				for (Documento cc : listaDoc) {
					lcomprob = (Documento) cc;
					oDocumento = new Documento();

					if ((oCompr.getCodigoDocumento()).equals(lcomprob.getCodigoDocumento())) {
						System.out.println("isFlagSeleccion " + lcomprob.isFlagSeleccion());
						// lcomprob.setFlagSeleccion(true);
						// establecerEstadoPrincipalDocumento
						oDocumento.setCodigoFicha(ficha.getCodigoFicha());
						oDocumento.setCodigoDocumento(oCompr.getCodigoDocumento());
						Factory.getInstance().getDocumentoBO().establecerEstadoPrincipalDocumento(oDocumento,
								this.getSesNroSesion());

					}

				}

			} catch (Exception ex) {
				System.err.println("Error seleccionarItems()");
				this.msErrNeg("Error al seleccionar el Items " + ex.getMessage());
			}

		} /*
			 * else{ this.msErrNeg("No hay item a Seleccionar"); return; }
			 */

	}

	java.text.SimpleDateFormat sdAnio = new java.text.SimpleDateFormat("yyyy");

	// LISTAR ANIOS
	private List<SelectItem> oSelectItemListarAnios() {
		List<SelectItem> lista = new ArrayList<SelectItem>();
		// lista.add(new SelectItem("","-- Seleccione --"));

		Sistema oSistema = new Sistema();

		FactoryBase.getInstance().getSistemaBO().leerFechaActual(oSistema);

		Integer anioActual = Integer.parseInt(sdAnio.format(oSistema.getFechaActual()));

		// System.out.println("anioActual: "+anioActual);

		for (int inicio = (anioActual - 1); inicio <= anioActual + 2; inicio++) {
			lista.add(new SelectItem(inicio, inicio + ""));
		}
		// lista.add(new SelectItem("S","Si"));

		return lista;
	}

	public UploadedFile getoUpCertificado() {
		return oUpCertificado;
	}

	public void setoUpCertificado(UploadedFile oUpCertificado) {
		this.oUpCertificado = oUpCertificado;
	}

	public void guardarCertificadoExpediente() {
		System.out.println("guardarCertificadoExpediente");

		if (certificadoIMG.getCodigoFicha() == null) {
			this.msErrNeg("Error al seleccionar el certificado");
			return;
		}

		String nombreArchivo = null;
		try {
			// Guardar expediente
			String mensaje = "";
			mensaje = validarDatosCertificadoExpediente();
			if (!mensaje.equals("")) {
				this.msErrNeg(mensaje);
				return;
			}

			if (oUpCertificado != null) {
				nombreArchivo = getArchivos().tomahawkListener(oUpCertificado);
			}

			certificadoIMG.setReferencia(null);
			certificadoIMG.setRutaArchivo(getArchivos().getRutaWebAnexos());
			certificadoIMG.setNombreArchivo(nombreArchivo);
			ficha.setNombreArchivo(nombreArchivo);
			ficha.setRutaArchivo(certificadoIMG.getRutaArchivo());
			Factory.getInstance().getDocumentoBO().insertarCertificado(certificadoIMG, this.getSesNroSesion());
			System.out.println("certificadoIMG--> " + certificadoIMG);

			this.msOutDBOK("Se guard� el documento ");

		} catch (Exception e) {
			this.msErrNeg("Error al guardar los datos del Certificado.");

			System.out.println("Error al guardar los datos del Certificado. " + e.getMessage());
		}
	}

	//17-10-2019
	public void guardarCertificadoExpediente_firma() {
		System.out.println("guardarCertificadoExpediente_firma");

		if (certificadoIMG.getCodigoFicha() == null) {
			this.msErrNeg("Error al seleccionar el certificado");
			return;
		}

		String nombreArchivo = null;
		try {
			// Guardar expediente
			String mensaje = "";
			mensaje = validarDatosCertificadoExpediente();
			if (!mensaje.equals("")) {
				this.msErrNeg(mensaje);
				return;
			}

			if (oUpCertificado != null) {
				nombreArchivo = getArchivos().tomahawkListener(oUpCertificado);
			}

			certificadoIMG.setReferencia(null);
			certificadoIMG.setRutaArchivo(getArchivos().getRutaWebAnexos());
			certificadoIMG.setNombreArchivo(nombreArchivo);
			ficha.setNombreArchivo(nombreArchivo);
			ficha.setRutaArchivo(certificadoIMG.getRutaArchivo());
			Factory.getInstance().getDocumentoBO().insertarCertificado(certificadoIMG, this.getSesNroSesion());
			System.out.println("certificadoIMG--> " + certificadoIMG);

			this.msOutDBOK("Se guard� el documento ");

		} catch (Exception e) {
			this.msErrNeg("Error al guardar los datos del Certificado.");

			System.out.println("Error al guardar los datos del Certificado. " + e.getMessage());
		}
	}

	public String validarDatosCertificadoExpediente() {
		System.out.println("validarDatosCertificadoExpediente");
		String mensaje = "";

		if (oUpCertificado != null) {

			String contextoAnexo = oUpCertificado.getContentType().toUpperCase();

			if ((contextoAnexo).indexOf("PDF") != -1) {

			} else {
				mensaje = "Solo se permite subir archivos con extensi�n .pdf";
				oUpCertificado = null;
			}

		} else {
			mensaje = "El certificado es requerido";

		}
		return mensaje;
	}

	public void eliminarCertificadoIMG() {
		System.out.println("eliminarCertificadoIMG");

		oUpCertificado = null;
		ficha.setNombreArchivo(null);
		ficha.setRutaArchivo(null);

	}

	public void consultaBandejaCertificados() {

		System.out.println("consultaBandejaCertificados");

		this.setCantidadElementosPaginado(0);

		Map<String, Object> parameter = new HashMap<String, Object>();

		// int cont=0;

		if (this.getNumeroCertificado() != null) {
			parameter.put("numeroCertificado", this.getNumeroCertificado());
		} /*
			 * else{ cont++; }
			 */

		///////////////// WYUCRA
		if (this.getNumeroExpediente() != null && !this.getNumeroExpediente().trim().equals("")) {
			parameter.put("numeroExpediente", this.getNumeroExpediente());
		} /*
			 * else{ cont++; }
			 */

		if (this.getNombreAdministrado() != null && !this.getNombreAdministrado().trim().equals("")) {
			parameter.put("nombreAdministrado", this.getNombreAdministrado());
		} /*
			 * else{ cont++; }
			 */
		////////////////////////

		/*
		 * if(this.getFechaListIni() ==null && this.getFechaListFin() ==null ){ cont++;
		 * //cad="Las FECHA INICIO - FIN"; }else{
		 */

		///
		/*
		 * if(this.getFechaListIni()!=null && this.getFechaListFin() ==null ){
		 * this.msErrNeg("Debe Ingresar la Fecha Fin del expediente"); return;
		 * 
		 * } if(this.getFechaListFin()!=null && this.getFechaListIni() ==null ){
		 * this.msErrNeg("Debe Ingresar la Fecha Fin del expediente"); return;
		 * 
		 * }
		 */

		//

		if ((this.getFechaListIni()) != null) {
			if (isFechaMayorActual(this.getFechaListIni())) {
				this.msErrNeg("La Fecha Inicial del Certificado no puede ser mayor a la actual. ");
				return;
			}
		}

		if ((this.getFechaListFin()) != null) {
			if (isFechaMayorActual(this.getFechaListFin())) {
				this.msErrNeg("La Fecha Final del Certificado no puede ser mayor a la actual. ");
				return;
			}
		}

		if (this.getFechaListIni() != null && this.getFechaListFin() != null) {

			if (isFechaInicialMayorFinal(this.getFechaListIni(), this.getFechaListFin())) {
				this.msErrNeg("La Fecha Inicial no puede ser mayor a la Fecha Final del Certificado.");
				return;
			}

		}

		parameter.put("fechaInicioExpedicion", this.getFechaListIni());
		parameter.put("fechaFinExpedicion", this.getFechaListFin());
		// }

		/*
		 * if(cont==4){ this.msErrNeg("No se ha Ingresado los datos para la B�squeda");
		 * return; }
		 */

		// para esta primera etapa son los vigentes y de un pais

		if (this.getSesCodigoPerfil().equals(this.getCtePerfilConsultaPais())) {
			parameter.put("codigoPais", this.getCtePaisDeUsuario());
			parameter.put("vigente", "S");
		}

		System.out.println("parameter perfil " + parameter);

		Factory.getInstance().getFichaBO().buscarConsultaFichaPorPerfil(parameter);

		// List<BienInmueble> list;
		listaCertificados = (List<Ficha>) parameter.get("resultList");

		if (listaCertificados.size() == 0) {
			this.msErrNeg("No hay resultado para su b�squeda.");
			return;
		}

		this.setCantidadElementosPaginado(listaCertificados.size());

	}

	public void inicializarBusquedaConsulta() {
		System.out.println("inicializarBusquedaConsulta");

		// this.setCodigoUsuarioRegistro(999);

		consultaBandejaCertificados();

	}

	public void limpiarBandejaConsultaCertificados() {
		System.out.println("limpiarBandejaConsultaCertificados");

		this.setNumeroCertificado(null);
		this.setFechaListIni(null);
		this.setFechaListFin(null);

		/*
		 * this.setCodigoEstadoCertificado(null); this.setCodigoUsuarioRegistro(null);
		 */

		this.setNumeroExpediente(null);
		this.setNombreAdministrado(null);

		listaCertificados.clear();

		this.setCantidadElementosPaginado(0);

	}
	
	
	
}
