package pe.gob.mc.cnp.controller.managedBean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FilenameUtils;

import pe.gob.mc.msag.model.domain.Empresa;
import pe.gob.mc.msag.model.domain.Pais;
import pe.gob.mc.msag.model.domain.Sistema;
import pe.gob.mc.msag.model.factory.FactoryBase;

import pe.gob.mc.cnp.model.common.constants.Constants;
import pe.gob.mc.cnp.model.domain.Documento;
import pe.gob.mc.cnp.util.Archivos;


public class BaseBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
	 
	//VARIABLES DE SESION
	private String sesUsuarioLogin;
	private Integer sesDependencia;
	private Integer sesOrganigrama;
	private Integer sesCodUsuario;
	private Integer sesCodNivel;
	private Integer sesCodigoPerfil;
	private String sesDatosUsuario;
	private Integer sesNroSesion;
	private String sesFechaActual;
	private Integer sesCodigoOrganizacion;
	
	//VARIABLES PARA MANEJO DE PESTA�AS
	private String rutaTab;
	private String tabSeleccionado;
	private boolean modoEditable;
		
	//VARIABLES PARA PERFIL
	private Integer ctePerfilAdministrador;
	private Integer ctePerfilConsulta;	
	private Integer ctePerfilMantenimiento;
	private Integer ctePerfilSupervisor;
	private Integer ctePerfilDirector;
	private Integer ctePerfilRegistrador;
	private Integer ctePerfilConsultaExterna;
	private Integer ctePerfilAdministrativo;
	private Integer ctePerfilConsultaPais;
	private Integer ctePerfilConsultaAutoridadNacional;
	private Integer ctePerfilConsultaAltaDireccion;
	
	private Integer ctePaisDeUsuario;
	
	
	
	public Integer getCtePaisDeUsuario() {
		return ctePaisDeUsuario;
	}
	public void setCtePaisDeUsuario(Integer ctePaisDeUsuario) {
		this.ctePaisDeUsuario = ctePaisDeUsuario;
	}
	public Integer getCtePerfilConsultaPais() {
		return ctePerfilConsultaPais;
	}
	public void setCtePerfilConsultaPais(Integer ctePerfilConsultaPais) {
		this.ctePerfilConsultaPais = ctePerfilConsultaPais;
	}
	public Integer getCtePerfilConsultaAutoridadNacional() {
		return ctePerfilConsultaAutoridadNacional;
	}
	public void setCtePerfilConsultaAutoridadNacional(
			Integer ctePerfilConsultaAutoridadNacional) {
		this.ctePerfilConsultaAutoridadNacional = ctePerfilConsultaAutoridadNacional;
	}
	public Integer getCtePerfilConsultaAltaDireccion() {
		return ctePerfilConsultaAltaDireccion;
	}
	public void setCtePerfilConsultaAltaDireccion(
			Integer ctePerfilConsultaAltaDireccion) {
		this.ctePerfilConsultaAltaDireccion = ctePerfilConsultaAltaDireccion;
	}
	public Integer getCtePerfilAdministrativo() {
		return ctePerfilAdministrativo;
	}
	public void setCtePerfilAdministrativo(Integer ctePerfilAdministrativo) {
		this.ctePerfilAdministrativo = ctePerfilAdministrativo;
	}
	public Integer getCtePerfilSupervisor() {
		return ctePerfilSupervisor;
	}
	public void setCtePerfilSupervisor(Integer ctePerfilSupervisor) {
		this.ctePerfilSupervisor = ctePerfilSupervisor;
	}
	public Integer getCtePerfilDirector() {
		return ctePerfilDirector;
	}
	public void setCtePerfilDirector(Integer ctePerfilDirector) {
		this.ctePerfilDirector = ctePerfilDirector;
	}
	public String getSesUsuarioLogin() {
		return sesUsuarioLogin;
	}
	public void setSesUsuarioLogin(String sesUsuarioLogin) {
		this.sesUsuarioLogin = sesUsuarioLogin;
	}
	public Integer getSesDependencia() {
		return sesDependencia;
	}
	public void setSesDependencia(Integer sesDependencia) {
		this.sesDependencia = sesDependencia;
	}
	public Integer getSesOrganigrama() {
		return sesOrganigrama;
	}
	public void setSesOrganigrama(Integer sesOrganigrama) {
		this.sesOrganigrama = sesOrganigrama;
	}
	public Integer getSesCodUsuario() {
		return sesCodUsuario;
	}
	public void setSesCodUsuario(Integer sesCodUsuario) {
		this.sesCodUsuario = sesCodUsuario;
	}
	public Integer getSesCodNivel() {
		return sesCodNivel;
	}
	public void setSesCodNivel(Integer sesCodNivel) {
		this.sesCodNivel = sesCodNivel;
	}
	public Integer getSesCodigoPerfil() {
		return sesCodigoPerfil;
	}
	public void setSesCodigoPerfil(Integer sesCodigoPerfil) {
		this.sesCodigoPerfil = sesCodigoPerfil;
	}
	public String getSesDatosUsuario() {
		return sesDatosUsuario;
	}
	public void setSesDatosUsuario(String sesDatosUsuario) {
		this.sesDatosUsuario = sesDatosUsuario;
	}
	public Integer getSesNroSesion() {
		return sesNroSesion;
	}
	public void setSesNroSesion(Integer sesNroSesion) {
		this.sesNroSesion = sesNroSesion;
	}
	public String getSesFechaActual() {
		return sesFechaActual;
	}
	public void setSesFechaActual(String sesFechaActual) {
		this.sesFechaActual = sesFechaActual;
	}
	public Integer getSesCodigoOrganizacion() {
		return sesCodigoOrganizacion;
	}
	public void setSesCodigoOrganizacion(Integer sesCodigoOrganizacion) {
		this.sesCodigoOrganizacion = sesCodigoOrganizacion;
	}
	public String getRutaTab() {
		return rutaTab;
	}
	public void setRutaTab(String rutaTab) {
		this.rutaTab = rutaTab;
	}
	public String getTabSeleccionado() {
		return tabSeleccionado;
	}
	public void setTabSeleccionado(String tabSeleccionado) {
		this.tabSeleccionado = tabSeleccionado;
	}
	public boolean isModoEditable() {
		return modoEditable;
	}
	public void setModoEditable(boolean modoEditable) {
		this.modoEditable = modoEditable;
	}
	public Integer getCtePerfilAdministrador() {
		return ctePerfilAdministrador;
	}
	public void setCtePerfilAdministrador(Integer ctePerfilAdministrador) {
		this.ctePerfilAdministrador = ctePerfilAdministrador;
	}
	public Integer getCtePerfilConsulta() {
		return ctePerfilConsulta;
	}
	public void setCtePerfilConsulta(Integer ctePerfilConsulta) {
		this.ctePerfilConsulta = ctePerfilConsulta;
	}
	public Integer getCtePerfilMantenimiento() {
		return ctePerfilMantenimiento;
	}
	public void setCtePerfilMantenimiento(Integer ctePerfilMantenimiento) {
		this.ctePerfilMantenimiento = ctePerfilMantenimiento;
	}
	public Integer getCtePerfilRegistrador() {
		return ctePerfilRegistrador;
	}
	public void setCtePerfilRegistrador(Integer ctePerfilRegistrador) {
		this.ctePerfilRegistrador = ctePerfilRegistrador;
	}
	public Integer getCtePerfilConsultaExterna() {
		return ctePerfilConsultaExterna;
	}
	public void setCtePerfilConsultaExterna(Integer ctePerfilConsultaExterna) {
		this.ctePerfilConsultaExterna = ctePerfilConsultaExterna;
	}
		
	
	private Archivos  archivos= new Archivos();

	public Archivos getArchivos() {
		return archivos;
	}

	public void setArchivos(Archivos archivos) {
		this.archivos = archivos;
	}

	
	public String[] cortarCadenaPorGuion(String cadena) {
		  return cadena.split("\\-");
		}

	public String[] cortarCadenaPorComas(String cadena) {
		  return cadena.split("\\,");
		}
	
	public BaseBean() {
		setSesUsuarioLogin((String) session.getAttribute(Constants.USUARIOLOGIN));
		setSesDependencia((Integer) session.getAttribute(Constants.CODDEPENDENCIA));		
		setSesNroSesion((Integer) session.getAttribute(Constants.NROSESION));
		setSesCodigoPerfil((Integer) session.getAttribute(Constants.CODPERFIL));
		setSesFechaActual((String) session.getAttribute(Constants.FECHAACTUAL));
		setSesCodUsuario((Integer) session.getAttribute(Constants.CODIGOUSUARIO));
		setSesDatosUsuario((String) session.getAttribute(Constants.DATOS_USUARIO));
		setSesCodigoOrganizacion((Integer) session.getAttribute(Constants.CODIGOORGANIZACION));		
	    //PERFIL
		setCtePerfilAdministrador(Constants.PERFILADMINISTRADOR);
		setCtePerfilConsulta(Constants.PERFILCONSULTA);		
		setCtePerfilSupervisor(Constants.PERFILSUPERVISOR);
		setCtePerfilRegistrador(Constants.PERFILREGISTRADOR);
		setCtePerfilDirector(Constants.PERFILDIRECTOR);
		setCtePerfilAdministrativo(Constants.PERFILADMINISTRATIVO);
		//WYUCRA21072017
		setCtePerfilConsultaPais(Constants.PERFILCONSULTAPAIS);
		setCtePerfilConsultaAutoridadNacional(Constants.PERFILCONSULTAAUTORIDADNACIONAL);
		setCtePerfilConsultaAltaDireccion(Constants.PERFILCONSULTAALTADIRECCION);
		
		System.out.println("getCtePerfilConsultaPais--> "+this.getCtePerfilConsultaPais());
		System.out.println("getSesCodigoPerfil--> "+this.getSesCodigoPerfil());
		if(this.getCtePerfilConsultaPais().equals(this.getSesCodigoPerfil())){
			asignarPais();	
		}
		
	}
	
	/************************************************REPORTES************************************************/
	protected static HttpServletRequest getRequest(){
		return (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	protected static HttpServletResponse getResponse(){
		return (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();
	}
	protected static FacesContext getContext(){
		return FacesContext.getCurrentInstance();	
	}
	/************************************************REPORTES************************************************/

	
	/************************************************PAGINACION************************************************/
	// B --> CUANDO SE REALIZA LA PRIMERA BUSQUEDA. Calcula la paginacion de la
	// aplicacion, maneja botones de navegacion de la paginaci�n
	protected List<Integer> paginacionPrimeraBusqueda(String paginacion,Integer oTotalRegistrosDb, Integer oRegistrosPorPg,	Integer oPaginaInicial, Integer oPaginaFinal) {
		Integer oPrimero = 0, oAnterior = 0, oSiguiente = 0, oUltimo = 0;List<Integer> oLista = new ArrayList<Integer>();

		// OBTENER PAGINA INICIAL Y FINAL
		if ((oRegistrosPorPg < oTotalRegistrosDb) && paginacion == null) {
			oPaginaInicial = 1;
			Double d = (Double.parseDouble(Integer.toString(oTotalRegistrosDb))) / (Double.parseDouble(Integer.toString(oRegistrosPorPg)));
			d = java.lang.Math.ceil(d);
			oPaginaFinal = d.intValue();
		}
		if ((oRegistrosPorPg >= oTotalRegistrosDb) && paginacion == null) {
			oPaginaInicial = 1;
			oPaginaFinal = 1;
		}
		// DEFINE NAVEGACION
		if (oPaginaFinal != 1 && paginacion == null) {
			oPrimero = 0;
			oAnterior = 0;
			oSiguiente = 1;
			oUltimo = 1;
		}
		if (oPaginaFinal == 1 && paginacion == null) {
			oPrimero = 0;
			oAnterior = 0;
			oSiguiente = 0;
			oUltimo = 0;
		}

		oLista.add(oPaginaInicial);
		oLista.add(oPaginaFinal);
		oLista.add(oPrimero);
		oLista.add(oAnterior);
		oLista.add(oSiguiente);
		oLista.add(oUltimo);

		return oLista;
	}

	// P --> CUANDO SE USA BOTONES DE NAVEGACION DE LA PAGINACION. Calcula el
	// rango de registros a pedir a la db, calcula la paginacion de la
	// aplicacion, maneja botones de navegacion de la paginaci�n
	protected List<Integer> paginacionNavega(String paginacion,	Integer oRegistrosPorPg, Integer registroInicial,Integer registroFinal, Integer oPaginaInicial, Integer oPaginaFinal) {
		Integer oPrimero = 0, oAnterior = 0, oSiguiente = 0, oUltimo = 0; List<Integer> oLista = new ArrayList<Integer>();

		if (paginacion.equals("P")) {
			registroInicial = 1;
			//
			registroFinal = oRegistrosPorPg;
			oPaginaInicial = 1;
			//
			oPrimero = 0;
			oAnterior = 0;
			oSiguiente = 1;
			oUltimo = 1;
		}
		if (paginacion.equals("A")) {
			oPaginaInicial = oPaginaInicial - 1;// *
			//
			registroInicial = (oRegistrosPorPg * (oPaginaInicial - 1)) + 1;// db
			registroFinal = oRegistrosPorPg * oPaginaInicial;// db
			//
			if (oPaginaInicial == 1) {
				oPrimero = 0;
				oAnterior = 0;
				oSiguiente = 1;
				oUltimo = 1;
			} else {
				oPrimero = 1;
				oAnterior = 1;
				oSiguiente = 1;
				oUltimo = 1;
			}
		}
		if (paginacion.equals("S")) {
			oPaginaInicial = oPaginaInicial + 1;// *
			//
			registroInicial = (oRegistrosPorPg * (oPaginaInicial - 1)) + 1;// db
			registroFinal = oRegistrosPorPg * oPaginaInicial;// db
			//
			if (oPaginaInicial == oPaginaFinal) {
				oPrimero = 1;
				oAnterior = 1;
				oSiguiente = 0;
				oUltimo = 0;
			} else {
				oPrimero = 1;
				oAnterior = 1;
				oSiguiente = 1;
				oUltimo = 1;
			}
		}
		if (paginacion.equals("U")) {
			oPaginaInicial = oPaginaFinal;// *
			//
			registroInicial = (oRegistrosPorPg * (oPaginaInicial - 1)) + 1;// db
			registroFinal = oRegistrosPorPg * oPaginaInicial;// db
			//
			oPrimero = 1;
			oAnterior = 1;
			oSiguiente = 0;
			oUltimo = 0;

		}

		oLista.add(registroInicial);
		oLista.add(registroFinal);
		oLista.add(oPaginaInicial);
		oLista.add(oPaginaFinal);
		oLista.add(oPrimero);
		oLista.add(oAnterior);
		oLista.add(oSiguiente);
		oLista.add(oUltimo);

		return oLista;
	}
	/************************************************PAGINACION************************************************/
	
	
	/************************************************MENSAJES************************************************/	
	public void msErrNeg(String msg) { //va
		FacesMessage facesMessage = new FacesMessage(
				FacesMessage.SEVERITY_ERROR, msg, null);
		FacesContext.getCurrentInstance().addMessage(null, facesMessage);
	}
	public void msOutDBOK(String msg) { //va
		FacesMessage facesMessage = new FacesMessage(
				FacesMessage.SEVERITY_INFO, msg, null);
		FacesContext.getCurrentInstance().addMessage(null, facesMessage);
	}
	/************************************************MENSAJES************************************************/	

	
	/************************************************GENERAL************************************************/	
	public boolean validarImagen(String nombre) {
		boolean ok = false;
		String ext = FilenameUtils.getExtension(nombre).toLowerCase();
		System.out.println("Validando imagen ("+ nombre+ ")->"+ ext+ " "+ (ext.equals("jpg") || ext.equals("png") || ext.equals("gif")));
		if (ext.equals("jpg") || ext.equals("png") || ext.equals("gif")) {
			ok = true;
		}
		return ok;
	}

	public String eliminarTagHtml(String html) {
		html = html.replaceAll("\\<.*?>", "");
		html = html.replaceAll("&nbsp;", "");
		html = html.replaceAll("&amp;", "");
		return html;
	}	

	public String obtenerConComaDecimal(Double val) {
		return String.valueOf(val).replace(".", ",");
	}

	public Double obtenerConPuntoDecimal(String val) {
		return Double.parseDouble((val.replace(",", ".")));
	}

	public double Redondear(double numero) {
		String val = numero + "";
		BigDecimal big = new BigDecimal(val);
		big = big.setScale(2, RoundingMode.HALF_UP);
		return Double.parseDouble(String.valueOf(big));
	}

	public String getFParam(String param) {
		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, String> params = context.getExternalContext().getRequestParameterMap();
		return params.get(param);
	}
	/************************************************GENERAL************************************************/	

	
	/************************************************FECHAS************************************************/	
	public Date agregarDiasAFecha(Date date, int days) {
		return addMinutesToDate(date, 60 * 24 * days);
	}

	public Date addMinutesToDate(Date date, int minutes) {
		Calendar calendarDate = Calendar.getInstance();
		calendarDate.setTime(date);
		calendarDate.add(Calendar.MINUTE, minutes);
		return calendarDate.getTime();
	}

	public int getDiasHabiles(Calendar fechaInicial, Calendar fechaFinal) {
		int diffDays = 0;
		// mientras la fecha inicial sea menor o igual que la fecha final se
		// cuentan los dias
		while (fechaInicial.before(fechaFinal)
				|| fechaInicial.equals(fechaFinal)) {
			// System.out.println( "fechaFinal-> "+fechaFinal);

			// si el dia de la semana de la fecha minima es diferente de sabado
			// o domingo
			if (fechaInicial.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
					&& fechaInicial.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY) {
				// se aumentan los dias de diferencia entre min y max
				diffDays++;
			}
			// se suma 1 dia para hacer la validacion del siguiente dia.
			fechaInicial.add(Calendar.DATE, 1);

		}
		return diffDays;
	}

	public Date getPrimerDiaDelMes() {
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
				cal.getActualMinimum(Calendar.DAY_OF_MONTH),
				cal.getMinimum(Calendar.HOUR_OF_DAY),
				cal.getMinimum(Calendar.MINUTE),
				cal.getMinimum(Calendar.SECOND));
		return cal.getTime();
	}

	public static Date aniadirMinutosAFecha(Date date, int minutes) {
		Calendar calendarDate = Calendar.getInstance();
		calendarDate.setTime(date);
		calendarDate.add(Calendar.MINUTE, minutes);
		return calendarDate.getTime();
	}

	public static Date aniadirDiasAFecha(Date date, int days) {
		return aniadirMinutosAFecha(date, 60 * 24 * days);
	}	
	
	//-->
	protected Date obtenerFechaServidorDb(){
		Sistema oSistema = new Sistema();
		FactoryBase.getInstance().getSistemaBO().leerFechaActual(oSistema);
		return oSistema.getFechaActual();
	}	
	protected Date obtenerFechaServidorApp(){
		return new Date();		
	}
	
	public String getRetornarFechaServidorAppFormato(){		
		SimpleDateFormat formateador = new SimpleDateFormat("'Hoy es' EEEE, dd 'de' MMMM 'del' yyyy"); 
		String oFecha = formateador.format(obtenerFechaServidorApp());
		return oFecha;
	}	
	
	public String getRetornarFechaServidorDbFormato(){
		SimpleDateFormat formateador = new SimpleDateFormat("'Hoy es' EEEE, dd 'de' MMMM 'del' yyyy"); 
		String oFecha = formateador.format(obtenerFechaServidorDb());
		return oFecha;		
	}
	
	protected Boolean isFechaMayorActual(Date oDate){	
		Boolean result = false;			
		try {
			SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy"); 
			String oFecha = formateador.format(oDate);
			String oFechaActual = formateador.format(obtenerFechaServidorDb());
			oDate = formateador.parse(oFecha);
			Date oDateActual = formateador.parse(oFechaActual);
			
			//Si esta antes de la fecha actual
			if(oDate.before(oDateActual) || oDate.equals(oDateActual) ){
				result = false;
			}else{
				result = true;
			}
		} catch (ParseException e) {			
			e.printStackTrace();
			return result;
		}		
		return result;		
	}
	
	protected Boolean isFechaMenorActual(Date oDate){	
		Boolean result = false;			 
		try {
			SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy"); 
			String oFecha = formateador.format(oDate);
			String oFechaActual = formateador.format(obtenerFechaServidorDb());
			oDate = formateador.parse(oFecha);
			Date oDateActual = formateador.parse(oFechaActual);
			
			//Si esta antes de la fecha actual
			if(oDate.after(oDateActual) || oDate.equals(oDateActual) ){
				result = false;
			}else{
				result = true;
			}
		} catch (ParseException e) {			
			e.printStackTrace();
			return result;
		}		
		return result;		
	}
	
	protected Integer restarAniosFechaActual(Date oDate){			
		Integer anos = 0;
		Calendar calendarActual = Calendar.getInstance();
		calendarActual.setTime(obtenerFechaServidorDb());
		
		Calendar calendarMenor = Calendar.getInstance();
		calendarMenor.setTime(oDate);
		  
		anos = calendarActual.get(Calendar.YEAR) - calendarMenor.get(Calendar.YEAR);		
		
		return anos;		
	}
	
	protected Boolean isFechaIgualActual(Date oDate){		
		Boolean result = false;				
		try {
			SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy"); 
			String oFecha = formateador.format(oDate);
			String oFechaActual = formateador.format(obtenerFechaServidorDb());
			oDate = formateador.parse(oFecha);
			Date oDateActual = formateador.parse(oFechaActual);
			
			
			if(oDate.equals(oDateActual) ){
				result = true;
			}else{
				result = false;
			}
		} catch (ParseException e) {			
			e.printStackTrace();
			return result;
		}		
		return result;		
	}
	
	protected Boolean isFechaInicialMayorFinal(Date oDateI, Date oDateF){	
		Boolean result = false;			
		try {
			SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy"); 
			String oFechaI = formateador.format(oDateI);
			String oFechaF = formateador.format(oDateF);
			oDateI = formateador.parse(oFechaI);
			oDateF = formateador.parse(oFechaF);
			
			//Si esta antes de la fecha actual
			if(oDateI.before(oDateF) || oDateI.equals(oDateF) ){
				result = false;
			}else{
				result = true;
			}
		} catch (ParseException e) {			
			e.printStackTrace();
			return result;
		}		
		return result;		
	}		

	protected String retornarFechaString(Date oDate){
		SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy"); 
		String oFecha = formateador.format(oDate);
		return oFecha;
	}
	
	protected static int diferenciaDeFechasEnDias(Date fechaIn, Date fechaFinal ){
		GregorianCalendar fechaInicio= new GregorianCalendar();
		fechaInicio.setTime(fechaIn);
		GregorianCalendar fechaFin= new GregorianCalendar();
		fechaFin.setTime(fechaFinal);
		int dias = 0;
		if(fechaFin.get(Calendar.YEAR)==fechaInicio.get(Calendar.YEAR)){	
			dias =(fechaFin.get(Calendar.DAY_OF_YEAR)- fechaInicio.get(Calendar.DAY_OF_YEAR))+1;
		}else{
			int rangoAnyos = fechaFin.get(Calendar.YEAR) - fechaInicio.get(Calendar.YEAR);
		
			for(int i=0;i<=rangoAnyos;i++){
				int diasAnio = fechaInicio.isLeapYear(fechaInicio.get(Calendar.YEAR)) ? 366 : 365;
				if(i==0){
					dias=1+dias +(diasAnio- fechaInicio.get(Calendar.DAY_OF_YEAR));
				}else if(i==rangoAnyos){
					dias=dias +fechaFin.get(Calendar.DAY_OF_YEAR);
				}else{
					dias=dias+diasAnio;
				}
			}
		}	
		return dias;

	}
	
	// Suma los d�as recibidos a la fecha  
	protected Date sumarRestarDiasFecha(Date fecha, int dias){	 
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(fecha); // Configuramos la fecha que se recibe
	  calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de d�as a a�adir, o restar en caso de d�as<0	 
      return calendar.getTime(); // Devuelve el objeto Date con los nuevos d�as a�adidos
	 
	} 	
	
	/************************************************FECHAS************************************************/	
	
	
	@SuppressWarnings("unchecked")
	public List<SelectItem> getListarPais(){
		List<SelectItem> lista = new ArrayList<SelectItem>();
		lista.add(new SelectItem("", "-- Seleccione --"));
		try{
			Map<String, Object> param = new HashMap<String, Object>();
			FactoryBase.getInstance().getPaisBO().buscarPaisTodos(param);
			List<Pais> lpais = (List<Pais>)param.get("resultList");
			for(Pais p : lpais){
				lista.add(new SelectItem(p.getCodigoPais(), p.getNombre()));
			}
		}catch(Exception ex){}
		return lista;
	}
	
	
	public Date obtenerFechaActual(){
		 Sistema oSistema= new Sistema();		    
	    	FactoryBase.getInstance().getSistemaBO().leerFechaActual(oSistema); 
	    	
	    	return oSistema.getFechaActual();
	    	
	}
	
	public void asignarPais(){
        System.out.println( "asignarPais");
        //dea cuerdo a la empresa
        
        try{
		Map<String, Object> parameter = new HashMap<String, Object>();
	    parameter.put("codigoEmpresa",   this.getSesDependencia()  );	
        FactoryBase.getInstance().getEmpresaBO().leerEmpresaPorCodigo(parameter);        
         List<Empresa> listaEmpresa=   (List<Empresa>)parameter.get("resultList"); 
        
         if(listaEmpresa.size()==1){
        	 this.setCtePaisDeUsuario(listaEmpresa.get(0).getCodigoPais())	 ;
         }else{
        	 this.msErrNeg("No se encuentra el pais");
        	 return;
         }        
        
    	}catch(Exception ex){
			System.err.println("Error asignarPaisasignarPais()");
			this.msErrNeg("Error asignarPais(): "+ex.getMessage());
		}
		
        
	}
	
}
