package pe.gob.mc.cnp.controller.managedBean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.commons.codec.digest.DigestUtils;
import org.richfaces.component.html.HtmlDataTable;
import pe.gob.mc.msag.model.domain.Usuario;
import pe.gob.mc.msag.model.factory.FactoryBase;

public class UsuarioBean extends BaseBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4715687466928159377L;

	private String claveAnterior;
	
	private String confirmarClave;
	
	public String mensaje;
	
	private Usuario  oUsu;
	private Usuario  usuario;	
	private Usuario usuBusq;
    private boolean cambiarClave;
	public String claveConfirmar;
	public String mostrarBotonModUsuario;        
	private  HtmlDataTable  tablaUsuario;
	private  HtmlDataTable  tablaUsuarioBusq;
	
		 
	 private String mostrarBotonInicio;
	  
	public String getMostrarBotonInicio() {
		return mostrarBotonInicio;
	}

	public void setMostrarBotonInicio(String mostrarBotonInicio) {
		this.mostrarBotonInicio = mostrarBotonInicio;
	}
	 
	public HtmlDataTable getTablaUsuarioBusq() {
		return tablaUsuarioBusq;
	}

	public void setTablaUsuarioBusq(HtmlDataTable tablaUsuarioBusq) {
		this.tablaUsuarioBusq = tablaUsuarioBusq;
	}

	public Usuario getUsuBusq() {
		return usuBusq;
	}

	public void setUsuBusq(Usuario usuBusq) {
		this.usuBusq = usuBusq;
	}

	

	public String getMostrarBotonModUsuario() {
		return mostrarBotonModUsuario;
	}

	public void setMostrarBotonModUsuario(String mostrarBotonModUsuario) {
		this.mostrarBotonModUsuario = mostrarBotonModUsuario;
	}

	
	
	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	public boolean isCambiarClave() {
		return cambiarClave;
	}


	public void setCambiarClave(boolean cambiarClave) {
		this.cambiarClave = cambiarClave;
	}



	public String getClaveConfirmar() {
		return claveConfirmar;
	}

	public void setClaveConfirmar(String claveConfirmar) {
		this.claveConfirmar = claveConfirmar;
	}

	public HtmlDataTable getTablaUsuario() {
		return tablaUsuario;
	}

	public void setTablaUsuario(HtmlDataTable tablaUsuario) {
		this.tablaUsuario = tablaUsuario;
	}


	public UsuarioBean (){
		oUsu=new Usuario();
		usuario=new Usuario();
		usuBusq=new Usuario();
	/*	fechaListFin= new Date();
		fechaListIni= new Date();*/
		//this.setFechaListIni(getPrimerDiaDelMes());
		this.setMostrarBotonModUsuario("N");
		this.setCambiarClave(false);
		
		this.setMostrarBotonInicio("N");
		oUsu.setLogin(this.getSesUsuarioLogin());
	}
		
	
	
	
	public void limpiarBandejaUsuario(){
		usuario= new Usuario();
		this.setCambiarClave(false);
		this.setClaveConfirmar(null);
	}



	
	
	public String getMensaje() {
		return mensaje;
	}


	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


	Usuario oUsuario=null;
	
	
	

	@SuppressWarnings({ "unchecked", "deprecation" })
	public void /*String*/ guardarFormCambioClave(){
	
	try{ 
		System.out.println("guardarFormCambioClave->");

		String cad="";
		String mensajeSalida="";
		cad=validarIngresoClaves();
		
		if (!cad.equals("")){	
			cad+=" no se ha ingresado ";
		    //  mensajeSalida="cambioClave-error";
		      setMensaje(cad);
		      
		      FacesMessage facesMessage =  new FacesMessage(FacesMessage.SEVERITY_ERROR, cad , null);
		      FacesContext.getCurrentInstance().addMessage(null, facesMessage);	
		      
			 // 	return mensajeSalida;  
		
	    }	
				
			
		Usuario  oU =null;
		
		this.oUsu.setLogin(this.oUsu.getLogin().trim().toUpperCase());
		
		
		System.out.println("llega del formulario login: "+ this.oUsu.getLogin() +" clave "+this.oUsu.getClave());
		
		Boolean isEmpresa = this.oUsu.getLogin().contains("MC-");
		
		  Map<String, Object> parameter = new HashMap<String, Object>();
		  parameter.put("login", this.oUsu.getLogin() );
		   
		  if (!isEmpresa){
			  FactoryBase.getInstance().getUsuarioBO().leerUsuarioPorLogin(parameter);
		  }else{
			  FactoryBase.getInstance().getUsuarioBO().leerUsuarioEmpresaPorLogin(parameter);
		  }
		   
	      List<Usuario> listado;
	      listado= (List<Usuario>)parameter.get("resultList"); 
	      System.out.println("listado.size()--> "+listado.size());
	      if(listado.size()==1){
	    	  for (Usuario dat : listado) {
				  oU=(Usuario)dat;	
			  } 		    	  
	      }else{			
	    	  mensajeSalida="El usuario Ingresado no existe";
	      }		      	     
	      
	      if(   !(DigestUtils.shaHex(this.getClaveAnterior()).equals(oU.getClave()))  ){

	     	  mensajeSalida="LA CLAVE ANTERIOR ingresada es incorrecta";
	      }    		
	      
	      if(   !( this.getConfirmarClave().trim().equals( this.oUsu.getClave().trim()  ))  ){

	    	  mensajeSalida="Las CLAVES ingresadas son diferentes, revize su escritura";
	      }   
	      
	      
	      if(!mensajeSalida.equals("")){
	    	  setMensaje(mensajeSalida);		
	    	  FacesMessage facesMessage =  new FacesMessage(FacesMessage.SEVERITY_ERROR, mensajeSalida , null);
			  FacesContext.getCurrentInstance().addMessage(null, facesMessage);	
		      mensajeSalida="cambioClave-error";
			  //return mensajeSalida;
		      return;
	      }    
	      
	     this.oUsu.setClave(DigestUtils.shaHex(this.oUsu.getClave().trim()));
	      this.oUsu.setCodigoUsuario(oU.getCodigoUsuario());

		  System.out.println("antes de enviar "+ oUsu.getLogin()+" clave: "+oUsu.getClave());
		  
		  if (!isEmpresa){
			  FactoryBase.getInstance().getUsuarioBO().cambiarClaveUsuario(this.oUsu, (this.getSesNroSesion()==null)? 0:this.getSesNroSesion());
		  }else{
			  FactoryBase.getInstance().getUsuarioBO().resetearClaveUsuarioEmpresa(this.oUsu, (this.getSesNroSesion()==null)? 0:this.getSesNroSesion());
		  }      
	      
			 this.setMostrarBotonInicio("S");
			 
			  FacesMessage facesMessage =  new FacesMessage(FacesMessage.SEVERITY_ERROR, "Se ha modificado satisfactoriamente la CONTRASENA, Favor de Ir a LOGUEO para su inicio de sesion" , null);
			  FacesContext.getCurrentInstance().addMessage(null, facesMessage);	
			 
		 
      }catch(Exception e){
        	System.out.println(e.getStackTrace());
        }
	setMensaje(null);
	

}	

public String validarIngresoClaves(){
	String mensaje="";
	if(this.getClaveAnterior() ==null){
		 mensaje+="La CLAVE ANTERIOR, ";	
	}else{
		if(this.getClaveAnterior().trim().equals("") ){
			mensaje+="La CLAVE ANTERIOR, ";			
		}
	}
	
	if(this.getConfirmarClave() ==null){
		 mensaje+="La CLAVE a CONFIRMAR, ";	
	}else{
		if(this.getConfirmarClave().trim().equals("") ){
			 mensaje+="La CLAVE a CONFIRMAR, ";				
		}
	}
	
			
	if(this.getoUsu().getClave() ==null){
		 mensaje+="La nueva CLAVE, ";	
	}else{
		if(this.getoUsu().getClave() .trim().equals("") ){
			mensaje+="La nueva CLAVE, ";				
		}
	}

	
	return mensaje;
}





public void limpiarFormCambioClave(){
	this.setClaveAnterior("");
	this.setConfirmarClave("");
	String nombreUsua=oUsu.getLogin();
	oUsu= new Usuario();
	oUsu.setLogin(nombreUsua);
}


	
public String getClaveAnterior() {
	return claveAnterior;
}

public void setClaveAnterior(String claveAnterior) {
	this.claveAnterior = claveAnterior;
}

public String getConfirmarClave() {
	return confirmarClave;
}


public void setConfirmarClave(String confirmarClave) {
	this.confirmarClave = confirmarClave;
}


public Usuario getoUsu() {
	return oUsu;
}


public void setoUsu(Usuario oUsu) {
	this.oUsu = oUsu;
}



public String irVentanaLogueo(){
	this.setMostrarBotonInicio("N");
	
	  session.setAttribute("USUARIOLOGIN",null );
	  session.setAttribute("CUENTA",null );
	//session.invalidate();
	
	return "cualquiera-login";
}


public String ircambioClave(){
	
	  oUsu.setLogin(this.getSesUsuarioLogin());
	
	return "cambioClave-error";
}

}
