/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.mc.cnp.controller.managedBean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.digest.DigestUtils;
import pe.gob.mc.msag.model.common.constants.SistConstants;
import pe.gob.mc.msag.model.domain.Aplicativo;
import pe.gob.mc.msag.model.domain.HistorialLogueo;
import pe.gob.mc.msag.model.domain.IncidenciaLogueo;
import pe.gob.mc.msag.model.domain.Sesion;
import pe.gob.mc.msag.model.domain.Perfil;
import pe.gob.mc.msag.model.domain.Sistema;
import pe.gob.mc.msag.model.domain.TipoSalida;
import pe.gob.mc.msag.model.domain.Usuario;
import pe.gob.mc.msag.model.domain.Version;
import pe.gob.mc.msag.model.factory.FactoryBase;
import pe.gob.mc.msag.model.service.SesionService;

import pe.gob.mc.cnp.model.common.constants.Constants;

/**
 *
 * @author wyucra
 */
public class Login /* extends BaseBean*/  implements Serializable{
	
    private static final long serialVersionUID = 4495062281137084743L;
	private String mensaje;
	@SuppressWarnings("unused")
	private String sisVersion;

	
	public Login() {
		super();		
		usu= new  Usuario();
	}	
	
	HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true); 
	
	public String getSisVersion() {
		return Constants.NRO_VERSION;
	}

	public void setSisVersion(String sisVersion) {
		this.sisVersion = sisVersion;
	}

	//retorna mensaje en caso de un login incorrecto
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	//--------------------------------------------
	
	private Usuario   usu  ;
	
	
	/**
	 * @return the usu
	 */
	public Usuario getUsu() {
		return usu;
	}

	/**
	 * @param usu the usu to set
	 */
	public void setUsu(Usuario usu) {
		this.usu = usu;
	}
	
	Aplicativo oAplicativo=null;
	Version oVersion=null;
	Usuario oUsuario=null;
	Sesion oSesion=null;
	TipoSalida oTipoSalida=null;  
	IncidenciaLogueo oIncidenciaLogueo=null;
	HistorialLogueo  oHistorialLogueo=null;
	SesionService  obj;
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public String login() {
		
		String mensajeSalida="";
		
	
		//if(usu.getLogin()!=null || usu.getClave()!=null){
			///////////////////////////////
			if(usu.getLogin()!=null && usu.getLogin().length()<3){
				mensajeSalida="El USUARIO ingresado no tiene los suficientes caracteres";	
				return "login-error";
			}else if(   usu.getClave()!=null &&  usu.getClave().length()<3){
				mensajeSalida="El PASSWORD ingresado no tiene los suficientes caracteres";
				return "login-error";
			}
	//	}
		
		
		if(usu.getLogin()==null || usu.getClave()==null){
			///////////////////////////////
			if(usu.getLogin()==null){
				mensajeSalida="El USUARIO ingresado no tiene los suficientes caracteres";	
				return "login-error";
			}else if(   usu.getClave()==null){
				mensajeSalida="El PASSWORD ingresado no tiene los suficientes caracteres";
				return "login-error";
			}
			///////////////////
			if(  usu.getLogin().length()<3){
				mensajeSalida="El USUARIO ingresado no tiene los suficientes caracteres";	
		 		  }else{
		 				if(   usu.getClave().length()<3){
		 					mensajeSalida="El PASSWORD ingresado no tiene los suficientes caracteres";	
		 			 		  }
		 		  }
			
			return "login-error";//mostrara la pagina de logueo actual previo mostrara el mensaje correspondiente
		 }			 
		
		String  mensajeLogin="";
		String username = usu.getLogin().toUpperCase();
		String clave = DigestUtils.shaHex(usu.getClave());
	
		
		System.out.println("username: "+username +" encriptada "+clave);
		/* ******************************************************************************** */

		//Obtener Codigo Aplicativo y Codigo Version
	     
		oUsuario= new Usuario(username,clave);		
		obj = new SesionService();
		// En este metodo se le enviara un objeto de tipo usuario conteniendo Usuario y clave (codificado) + COD Aplicacion y Nro de Version que se deben definir como constantes
		int opc;
		Boolean isEmpresa = username.contains("MC-");
		
		if (!isEmpresa){
			opc= obj.sesionIniciarWeb(oUsuario, Constants.ABREV_APLICACION, Constants.NRO_VERSION);
		}else{
			opc= obj.sesionIniciarWebEmpresa(oUsuario, Constants.ABREV_APLICACION, Constants.NRO_VERSION);
		}
		
		System.out.println("sesion creada-> "+opc);
							
		Map<String, Object> parameter;
		
		if(opc<0){// Errores de Logueo
			
			opc=opc*(-1);
			  parameter = new HashMap<String, Object>();
 		      parameter.put("codigoIncidencia",opc); 
 		      FactoryBase.getInstance().getIncidenciaLogueoBO().leerIncidenciaLogueo(parameter);
 		    		  
 		      List<IncidenciaLogueo> lista;
 		      lista= (List<IncidenciaLogueo>)parameter.get("resultList"); 
 		    
 			  for (IncidenciaLogueo inc : lista) {
				  oIncidenciaLogueo=(IncidenciaLogueo)inc;	
				  mensajeLogin=oIncidenciaLogueo.getDescripcion();
 			  }  			
			  setMensaje(mensajeLogin);
			    if(opc==17){		
			    	
			          parameter = new HashMap<String, Object>();
					  parameter.put("login", username );
					  if (!isEmpresa){
						  FactoryBase.getInstance().getUsuarioBO().leerUsuarioPorLogin(parameter);
					  }else{
						  FactoryBase.getInstance().getUsuarioBO().leerUsuarioEmpresaPorLogin(parameter);
					  }
				      List<Usuario> listado;
				      listado= (List<Usuario>)parameter.get("resultList"); 
				      
				      
				      Usuario oUsu=null;
				      for (Usuario dat : listado) {
						  oUsu=(Usuario)dat;	
						  session.setAttribute("DATOS_USUARIO",oUsu.getNombresPersona()+" "+ oUsu.getPaternoPersona()+" codp "+oUsu.getCodigoPerfil());
						  session.setAttribute("USUARIO",oUsu.getLogin() );
						  session.setAttribute("USUARIOLOGIN",oUsu.getLogin() );
						  session.setAttribute(Constants.CODPERFIL, -1);
						  session.setAttribute(Constants.CUENTA, oUsuario.getLogin());
						  } 
			    	  
				    mensajeSalida="cambioClave-error";	
			    }else{
			    	  if(opc==13){
						  mensajeSalida="login-mantenimiento";
					  }else{
						  mensajeSalida="login-error";
					  }		
			    }			  
			  
			
		}else{
			//Lee
		 	Sesion sess = new Sesion();
	    	Map<String, Object> parameterSes = new HashMap<String, Object>();    	  
	    	parameterSes.put("numeroSesion", opc);
	    	if (!isEmpresa){
	    		FactoryBase.getInstance().getSesionBO().leerSesionPorNumero(parameterSes);
	    	}else{
	    		FactoryBase.getInstance().getSesionBO().leerSesionEmpresaPorNumero(parameterSes);
	    	}	    	
	    	List<Sesion> lSesion;
	    	lSesion= (List<Sesion>)parameterSes.get("resultList");
	    	
	    	if(lSesion.size()==1) {
	    		for (Sesion ses : lSesion) {
	    			sess = (Sesion)ses;
	    			System.out.println("obsjeto sess "+sess);
	    		}
	    	}
	    	
	    	//Verifica si es ADMIN para tomar por defecto el perfil Administrador del Aplicativo
	    	Perfil oPerfilAdministradorAplicativo = null;
	    	int codigoPerfil = -1;
	    	if(sess.getCodigoUsuario()==1 && sess.getCodigoPerfil()==null) {
	    		//Lee Perfil Administrador del Aplicativo por Nombre
	    		Map<String, Object> parameterPerfil = new HashMap<String, Object>();    	  
	    		parameterPerfil.put("codigoAplicativo", sess.getCodigoAplicativo());
	    		parameterPerfil.put("nombrePerfil", "ADMINISTRADOR");
	    		FactoryBase.getInstance().getPerfilBO().buscarPerfilPorNombre(parameterPerfil);
	    		List<Perfil> lPerfil;
	    		lPerfil= (List<Perfil>)parameterPerfil.get("resultList");
	    		if(lPerfil.size()==1) {
	    			for (Perfil pfl : lPerfil) {
	    				oPerfilAdministradorAplicativo = (Perfil)pfl;
	    			}
	    			//Asigna el Perfil de la Sesion
	    			codigoPerfil = oPerfilAdministradorAplicativo.getCodigoPerfil();
	    		}
	    	} else {
	    		//Asigna el Perfil de la Sesion
	    		codigoPerfil = sess.getCodigoPerfil();
	    	}
	    	
	    	System.out.println("codigoPerfil "+codigoPerfil);
	    	
			session.setAttribute(Constants.NROSESION,Integer.parseInt(opc+"") );
			parameter = new HashMap<String, Object>();
			parameter.put("login", username );
			
			if (!isEmpresa){   
				System.out.println("noe s empresa");
				FactoryBase.getInstance().getUsuarioBO().leerUsuarioPorLogin(parameter);			
			}else{
				System.out.println("es emprsa");
				System.out.println("par "+parameter);
				
				FactoryBase.getInstance().getUsuarioBO().leerUsuarioEmpresaPorLogin(parameter);
			}
		    List<Usuario> listado;
		    listado= (List<Usuario>)parameter.get("resultList"); 
		  
		    
		    for (Usuario dat : listado) {
		    oUsuario=(Usuario)dat;
		    System.out.println("oUsuarioNEw-> "+oUsuario);
		    if (!isEmpresa){  
		    	session.setAttribute(Constants.DATOS_USUARIO,oUsuario.getNombresPersona()+" "+ oUsuario.getPaternoPersona()+" "+ oUsuario.getMaternoPersona());	
		    }else{
		    	session.setAttribute(Constants.DATOS_USUARIO,oUsuario.getNombreEmpresa());
		    }
		    
		    session.setAttribute(Constants.USUARIOLOGIN, oUsuario.getLogin());
		    session.setAttribute(Constants.CODDEPENDENCIA, oUsuario.getCodigoEmpresa());
		    
			    session.setAttribute(Constants.CODORGANIGRAMA, oUsuario.getCodigoOrganigrama());
			session.setAttribute(Constants.CUENTA, oUsuario.getLogin());
			session.setAttribute(Constants.CODPERFIL, codigoPerfil);
			    session.setAttribute(Constants.NOMOFICINA, oUsuario.getNombreOrganigrama());
			session.setAttribute(Constants.CODIGOUSUARIO, oUsuario.getCodigoUsuario());  
			
			if (!isEmpresa){ 
				session.setAttribute(Constants.CODIGOORGANIZACION, (oUsuario.getCodigoTipoOrganizacional()!=null)?(( oUsuario.getCodigoTipoOrganizacional()!=1 &&  oUsuario.getCodigoTipoOrganizacional()!=2)?9999:oUsuario.getCodigoTipoOrganizacional()):9999);
			}else{
				System.out.println("tipoorg "+oUsuario.getCodigoTipoOrganizacional());
				
				session.setAttribute(Constants.CODIGOORGANIZACION, (oUsuario.getCodigoTipoOrganizacional()!=null)?(( oUsuario.getCodigoTipoOrganizacional()!=1 &&  oUsuario.getCodigoTipoOrganizacional()!=2)?9999:oUsuario.getCodigoTipoOrganizacional()):9999);
				
			}
			
			
			}
		    
		    
		    Sistema oSistema= new Sistema();		    
	    	FactoryBase.getInstance().getSistemaBO().leerFechaActual(oSistema);  
	    	
	    	//System.out.println("antes "+oSistema.getFechaActual()+" "+" fewecha y hora-> "+formatoFechaHoraMinSeg.format(oSistema.getFechaActual()) );
	    	
	    	session.setAttribute(SistConstants.FECHAACTUAL, formatoFecha.format(oSistema.getFechaActual())  ); 		   		    
		    
		    oUsuario.setSesionCreacion(opc);
		      
			setMensaje(null);
			mensajeSalida="login-principal";
		}
		
		return mensajeSalida;

	}
	
	
	public void 	obtenerFechaActual(ActionEvent event){
		
		obtenerFechaActualAction();
		
	}
	public void obtenerFechaActualAction(){
		 Sistema oSistema= new Sistema();		    
	    	FactoryBase.getInstance().getSistemaBO().leerFechaActual(oSistema);  
	    	
	}
	
	
	SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
	
	SimpleDateFormat formatoFechaHoraMinSeg = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	public String logout() {
				
		session.invalidate();
				
		obj = new SesionService();
		oTipoSalida= new TipoSalida("LOGOUT");
		
        if(oUsuario.getSesionCreacion()!=null){
        	obj.sesionFinalizar(oUsuario.getSesionCreacion(), oTipoSalida);	
        }else{
        	obj.sesionFinalizar(0, oTipoSalida);
        }
	
		return "cualquiera-login";
	
	}

	

}
