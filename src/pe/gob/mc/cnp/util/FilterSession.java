/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.mc.cnp.util;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pe.gob.mc.cnp.model.common.constants.Constants;


/**
 *
 * @author wyucra
 */
public class FilterSession implements Filter{
    
    
	FilterConfig fc;
	
	public void destroy() {		
		fc=null;
	}

    public void init(FilterConfig filterConfig) throws ServletException {
        	fc=filterConfig;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
         
                HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res =(HttpServletResponse)response;
		HttpSession session = req.getSession(true);
		
		String paginaRequerida=req.getRequestURL().toString();
	//	System.out.println("paginaRequerida-> "+paginaRequerida+ " cuenta: "+SistConstants.CUENTA);
		
		/*if( session.getAttribute(SistConstants.CUENTA) == null){
			System.out.println("cuenta nullo");
		}else{
			System.out.println("cuenta no nullo");
		}
		*/
		
		if(paginaRequerida.contains("principalWeb.jsfx")){	//		
			chain.doFilter(req, res); 
		}else{//		
			//original
			if( session.getAttribute(Constants.CUENTA) == null	&& !paginaRequerida.contains("login.jsfx")){
				res.sendRedirect("../../index.jsp");		
				//System.out.println("index.jsp  "+ paginaRequerida);
			}else{		
				if( session.getAttribute(Constants.CUENTA) != null && paginaRequerida.contains("login.jsfx")){
					res.sendRedirect("principal.jsfx");	
					//System.out.println("prncipal  "+paginaRequerida);
				}
				//res.sendRedirect("principal.jsfx");	
				//System.out.println("???? "+paginaRequerida);				
				chain.doFilter(req, res);
			}	
			//original
					
			
		}	
		
	}
    
    
}
