/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.mc.cnp.util;

import java.io.Serializable;

/**
 *
 * @author wyucra
 */
public class LinkMenuWeb implements Serializable{    

	private static final long serialVersionUID = 1L;
	
	private String ruta;
	
    public LinkMenuWeb() {
    	this.ruta="/pages/bienvenidaWeb.xhtml";
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getRuta() {
        return ruta;
    } 
    
    
}
