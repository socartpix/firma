package pe.gob.mc.cnp.util;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.richfaces.model.UploadItem;

import org.apache.commons.net.ftp.FTP;  
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.myfaces.custom.fileupload.UploadedFile;

import pe.gob.mc.msag.model.factory.FactoryBase;
import com.jcraft.jsch.*;
import java.io.BufferedInputStream;
import java.net.InetAddress;

import javax.imageio.ImageIO;

public class Archivos implements Serializable {    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    private static final int IMG_WIDTH=359, IMG_HEIGHT=479;
    private static final int tipoImagenFormato = BufferedImage.TYPE_4BYTE_ABGR;
    private static final Color colorFondo = new Color(0,0,0,0);
    
  //VARIABLES PARA SUBIR ARCHIVOS AL SRV FTP
    private String prefijo;
  	private String modoFS = "";
  	
  	/*18 - 10 -2019*/
  	private String rutaAnexosFirmado = "/var/www/html/repositorio/prueba/";
  	public String getRutaAnexosFirmado() {
		return rutaAnexosFirmado;
	}
	public void setRutaAnexosFirmado(String rutaAnexosFirmado) {
		this.rutaAnexosFirmado = rutaAnexosFirmado;
	}
	/*18 - 10 -2019*/
	private String hostFirma = "https://repositorio.cultura.pe/prueba/"; 	
	
	
	/*18 - 10 -2019*/
	
	public String getHostFirma() {
		return hostFirma;
	}
	public void setHostFirma(String hostFirma) {
		this.hostFirma = hostFirma;
	}
	private String rutaAnexos = "";
  	private String rutaFotos = "";	
  	private String rutaWebAnexos = ""; 
  	private String rutaWebFotos = "";
  	private String rutaFTPAnexos = "";
  	private String rutaFTPFotos = ""; 
  	private String iFTPHOST = ""; 
  	private String uFTPUser = ""; 
  	private String uFTPPass = "";
  	
  	
	/*ARCHIVOS SET GET*/
	public  String getModoFS() {
		modoFS = getParametroGen("CNP_MODOFS");
		return modoFS;
	}
	public void setModoFS(String modoFS) {
		this.modoFS = modoFS;
	}
	public String getRutaAnexos() {
		rutaAnexos = getParametroGen("PATH_CNP_ANEXO");
		return rutaAnexos;
	}
	public void setRutaAnexos(String rutaAnexos) {
		this.rutaAnexos = rutaAnexos;
	}
	public String getRutaFotos() {
		rutaFotos = getParametroGen("PATH_CNP_FOTOS");
		return rutaFotos;
	}
	public void setRutaFotos(String rutaFotos) {
		this.rutaFotos = rutaFotos;
	}
	public String getRutaWebAnexos() {
		rutaWebAnexos = getParametroGen("URL_CNP_ANEXO");
		return rutaWebAnexos;
	}
	public void setRutaWebAnexos(String rutaWebAnexos) {
		this.rutaWebAnexos = rutaWebAnexos;
	}
	public String getRutaWebFotos() {
		rutaWebFotos = getParametroGen("URL_CNP_FOTOS");
		return rutaWebFotos;
	}
	public void setRutaWebFotos(String rutaWebFotos) {
		this.rutaWebFotos = rutaWebFotos;
	}
	public String getRutaFTPAnexos() {
		rutaFTPAnexos = getParametroGen("FTP_CNP_ANEXO");
		return rutaFTPAnexos;
	}
	public void setRutaFTPAnexos(String rutaFTPAnexos) {
		this.rutaFTPAnexos = rutaFTPAnexos;
	}
	public String getRutaFTPFotos() {
		rutaFTPFotos = getParametroGen("FTP_CNP_FOTO");
		return rutaFTPFotos;
	}
	public void setRutaFTPFotos(String rutaFTPFotos) {
		this.rutaFTPFotos = rutaFTPFotos;
	}
	public String getiFTPHOST() {
		iFTPHOST = getParametroGen("FTP_CNP_HOST");
		return iFTPHOST;
	}
	public void setiFTPHOST(String iFTPHOST) {
		this.iFTPHOST = iFTPHOST;
	}
	public String getuFTPUser() {
		uFTPUser = getParametroGen("FTP_CNP_USER");
		return uFTPUser;
	}
	public void setuFTPUser(String uFTPUser) {
		this.uFTPUser = uFTPUser;
	}
	public String getuFTPPass() {
		uFTPPass = getParametroGen("FTP_CNP_PASS");
		return uFTPPass;
	}
	public void setuFTPPass(String uFTPPass) {
		this.uFTPPass = uFTPPass;
	}		
	public String getPrefijo() {
		return prefijo;
	}
	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}
	
	public Archivos() {
		super();	
		this.prefijo = "CNP";
	}
	
	
	/************************************************SUBIR ARCHIVOS AL SERVIDOR FTP************************************************/
	@SuppressWarnings("unchecked")
	public static String getParametroGen(String nombreParametro) {
		String sParametro = "";
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("codigoParametro", nombreParametro);
			FactoryBase.getInstance().getParametroBO().leerParametroPorCodigo(param);
			ArrayList<pe.gob.mc.msag.model.domain.Parametro> listadet = (ArrayList<pe.gob.mc.msag.model.domain.Parametro>) param.get("resultList");
			if (listadet.size() > 0) {
				sParametro = listadet.get(0).getValor();
			}
		} catch (Exception ex) {
			System.err.println("Error getParametroGen: " + ex);
		}
		return sParametro;
	}
	
	public String tomahawkListener(UploadedFile xFile) throws Exception { 
		
		if (xFile != null) {
			File file = File.createTempFile("sdafasfsaf", "." + ((int) (Math.random() * 1000)) + ".am");
			OutputStream output = new FileOutputStream(file);
			IOUtils.copy(xFile.getInputStream(), output);
			UploadItem fileu = new UploadItem(xFile.getName(),(int) xFile.getSize(), xFile.getContentType(), file);
			String modo = getModoFS();
			String nombreArchivo="";
			if (modo.equals("LOCAL")) {
				nombreArchivo =  subirArchivoLocal(fileu,	getRutaAnexos(), 'A');
			} else if (modo.equals("FTP")) {
				nombreArchivo =   enviarFTP(getiFTPHOST(),getuFTPUser(), getuFTPPass(), fileu,getRutaFTPAnexos(), 'A');
			} else if (modo.equals("SFTP")) {
				nombreArchivo =   enviarSFTP(getiFTPHOST(),getuFTPUser(), getuFTPPass(), fileu,getRutaFTPAnexos(), 'A');
			}
			System.out.println("Fielelr " + modo + " " + getRutaFTPAnexos());
			file.deleteOnExit();
			return nombreArchivo;
		}
		
		return null;
	}

	
	public String tomahawkListenerIMG(UploadedFile xFile) throws Exception { 
		
		if (xFile != null) {
			File file = File.createTempFile("sdafasfsaf", "." + ((int) (Math.random() * 1000)) + ".am");
			OutputStream output = new FileOutputStream(file);IOUtils.copy(xFile.getInputStream(), output);
			UploadItem fileu = new UploadItem(xFile.getName(),(int) xFile.getSize(), xFile.getContentType(), file);
			String modo = getModoFS();
			String nombreArchivo="";
			if (modo.equals("LOCAL")) {
				nombreArchivo =   subirArchivoLocal(fileu, getRutaFotos(),'I');
			} else if (modo.equals("FTP")) {
				nombreArchivo =   enviarFTP(getiFTPHOST(),getuFTPUser(), getuFTPPass(), fileu,getRutaFTPFotos(), 'I');
			} else if (modo.equals("SFTP")) {
				nombreArchivo =   enviarSFTP(getiFTPHOST(),getuFTPUser(), getuFTPPass(), fileu,getRutaFTPFotos(), 'I');
			}
			System.out.println("Fielelr " + modo + " " + getRutaFTPFotos());
			file.deleteOnExit();
			return nombreArchivo;
		}
		
		return null;
	}
	
    
    private String subirArchivoLocal(UploadItem nFile, String directorio, char ftipo) throws Exception {        
        System.out.println("subirArchivoLocal() directorio: "+directorio);
        UploadItem fileit = nFile;
        String suffix = FilenameUtils.getExtension(fileit.getFileName());
        File file = null;
        OutputStream output = null;

        try { 
            String dir  = directorio;

            System.out.println("dir "+dir);
            file = File.createTempFile(prefijo, "." + suffix, new File(dir));
            output = new FileOutputStream(file);
            IOUtils.copy(new FileInputStream(fileit.getFile()), output);
            System.out.println("Archivo subido: "+file.getName());
            /*if(ftipo == 'I'){
            	InputStream rimg = resizeIMGtoInputStream(file);
            	if(rimg != null){
            		output = new FileOutputStream(new File(new File(dir), "thumb_"+file.getName()));
                    IOUtils.copy(rimg, output);
            	}
            }*/
            return file.getName();
            
        } catch (IOException e) {
            if (file != null) file.delete();
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(output);
        }
        return null;
    }
    
    private String enviarFTP(String host, String user, String pass, UploadItem nFile, String directorio, char ftipo) throws Exception{
        String archivoFinal = "";       
        UploadItem fileit = null;
        String suffix = null;
        try{     
            fileit = nFile;
            suffix = FilenameUtils.getExtension(fileit.getFileName());
            archivoFinal = prefijo + ((int)(new Date().getTime())) +"."+ suffix;
            
            FTPClient ftpClient = new FTPClient();
            ftpClient.connect(InetAddress.getByName(host));
            ftpClient.login(user,pass);
            int reply = ftpClient.getReplyCode();
            System.out.println("Respuesta recibida de conexion FTP:" + reply);
            
            if(FTPReply.isPositiveCompletion(reply))            {
                System.out.println("Conectado Satisfactoriamente");   
                ftpClient.changeWorkingDirectory(directorio);
                System.out.println("enviarFTP() directorio: "+directorio);

                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                ftpClient.enterLocalPassiveMode();
                BufferedInputStream buffIn = null;
                File file = fileit.getFile();
                buffIn = new BufferedInputStream(new FileInputStream(file));
                ftpClient.storeFile(archivoFinal, buffIn);
                buffIn.close();
                /*if(ftipo == 'I'){
                	InputStream rimg = resizeIMGtoInputStream(file);
                	if(rimg != null){
                		buffIn = new BufferedInputStream(rimg);
                        ftpClient.storeFile("thumb_"+archivoFinal, buffIn);
                        buffIn.close();
                	}
                }*/
                ftpClient.logout();
                ftpClient.disconnect();
                
                System.out.println("Archivo subido: "+archivoFinal);
                return archivoFinal;
            }
            else{
                System.out.println("Imposible conectarse al servidor "+host+" ("+user+", "+pass+")");
            }
        }catch(Exception ex){
            System.err.println("Error enviarFTP() Host "+host+" :" +ex);
        }


        return null;
    }
    
       
    public String enviarFTP_firma(String host, String user, String pass, String directorio, char ftipo,File file_,String _ext) throws Exception{
        String archivoFinal = "";       
        UploadItem fileit = null;
        String suffix = null;
        try{     

            suffix = _ext;
            archivoFinal = prefijo + ((int)(new Date().getTime())) +"."+ suffix;
            
            FTPClient ftpClient = new FTPClient();
            ftpClient.connect(InetAddress.getByName(host));
            ftpClient.login(user,pass);
            int reply = ftpClient.getReplyCode();
            System.out.println("Respuesta recibida de conexion FTP:" + reply);
            
            if(FTPReply.isPositiveCompletion(reply))            {
                System.out.println("Conectado Satisfactoriamente");   
                ftpClient.changeWorkingDirectory(directorio);
                System.out.println("enviarFTP() directorio: "+directorio);

                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                ftpClient.enterLocalPassiveMode();
                BufferedInputStream buffIn = null;
                File file = file_;
                buffIn = new BufferedInputStream(new FileInputStream(file));
                ftpClient.storeFile(archivoFinal, buffIn);
                buffIn.close();
                /*if(ftipo == 'I'){
                	InputStream rimg = resizeIMGtoInputStream(file);
                	if(rimg != null){
                		buffIn = new BufferedInputStream(rimg);
                        ftpClient.storeFile("thumb_"+archivoFinal, buffIn);
                        buffIn.close();
                	}
                }*/
                ftpClient.logout();
                ftpClient.disconnect();
                
                System.out.println("Archivo subido: "+archivoFinal);
                return archivoFinal;
            }
            else{
                System.out.println("Imposible conectarse al servidor "+host+" ("+user+", "+pass+")");
            }
        }catch(Exception ex){
            System.err.println("Error enviarFTP() Host "+host+" :" +ex);
        }


        return null;
    }
    
    public String enviarFTPS_firma(String host, String user, String pass, String directorio, char ftipo,File file_,String _ext,String filename) throws Exception{
        System.out.println("Entrando a subida de sftp");
        String SFTPHOST = host;
        int    SFTPPORT = 22;
        String SFTPUSER = user;
        String SFTPPASS = pass;
        System.out.println("Setting connexion usual");
        SFTPHOST = "repositorio.cultura.pe"; 	
        SFTPUSER = "repositorio";
        SFTPPASS = "Rino.20Pro,$";
        
        System.out.println("Recuerda comentar estas dos variables anteriores al pasarlo a produccion");
        /*
                String SFTPHOST = host;
        int    SFTPPORT = 22;
        String SFTPUSER = repositorio;
        String SFTPPASS = Rino.20Pro,$;
         * 
         * */
        String SFTPWORKINGDIR = directorio;
        System.out.println(SFTPHOST+" Host");
        System.out.println(SFTPUSER+" User");
        System.out.println(SFTPPASS+" Pass");
        System.out.println("Entrando a subida de sftp");
        Session     session     = null;
        Channel     channel     = null;
        ChannelSftp channelSftp = null;
        String suffix = _ext;
        //String archivoFinal = prefijo + ((int)(new Date().getTime())) +"."+ suffix;
        String archivoFinal = filename;
        System.out.println("ARchivo name"+archivoFinal);
        try{
            JSch jsch = new JSch();
            System.out.println("Iniciando FTP");
            session = jsch.getSession(SFTPUSER,SFTPHOST,SFTPPORT);
            UserInfo ui = new Archivos.SUserInfo(SFTPPASS, null); 
            session.setUserInfo(ui);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            channel = (ChannelSftp)session.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp)channel;
            channelSftp.cd(SFTPWORKINGDIR);
            System.out.println("enviarSFTP() directorio: "+SFTPWORKINGDIR);
            File file = file_;
            
            channelSftp.put(new FileInputStream(file), archivoFinal);
            /*if(ftipo == 'I'){
            	InputStream rimg = resizeIMGtoInputStream(file);
            	if(rimg != null){
            		channelSftp.put(rimg, "thumb_"+archivoFinal);
            		System.out.println("enviarSFTP() archivo subido:: "+"thumb_"+archivoFinal);
            	}
            }*/
            System.out.println("enviarSFTP() archivo subido:: "+archivoFinal);
            channelSftp.exit();
            channelSftp.disconnect();
            session.disconnect();
            return archivoFinal;
        }catch(Exception ex){
        	ex.printStackTrace();
            System.err.println("Error enviarSFTP() Host "+SFTPHOST+"("+SFTPUSER+", "+SFTPPASS+"): "+ex);
        }
        return null;
    } 
    
    private String enviarSFTP(String host, String user, String pass, UploadItem nFile, String directorio, char ftipo) throws Exception{
        
        String SFTPHOST = host;
        int    SFTPPORT = 22;
        String SFTPUSER = user;
        String SFTPPASS = pass;
        String SFTPWORKINGDIR = directorio;

        Session     session     = null;
        Channel     channel     = null;
        ChannelSftp channelSftp = null;
        String suffix = FilenameUtils.getExtension(nFile.getFileName());
        String archivoFinal = prefijo + ((int)(new Date().getTime())) +"[F]."+ suffix;
        try{
            JSch jsch = new JSch();
            session = jsch.getSession(SFTPUSER,SFTPHOST,SFTPPORT);
            UserInfo ui = new Archivos.SUserInfo(SFTPPASS, null); 
            session.setUserInfo(ui);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            channel = (ChannelSftp)session.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp)channel;
            channelSftp.cd(SFTPWORKINGDIR);
            System.out.println("enviarSFTP() directorio: "+SFTPWORKINGDIR);
            File file = nFile.getFile();
            channelSftp.put(new FileInputStream(file), archivoFinal);
            /*if(ftipo == 'I'){
            	InputStream rimg = resizeIMGtoInputStream(file);
            	if(rimg != null){
            		channelSftp.put(rimg, "thumb_"+archivoFinal);
            		System.out.println("enviarSFTP() archivo subido:: "+"thumb_"+archivoFinal);
            	}
            }*/
            System.out.println("enviarSFTP() archivo subido:: "+archivoFinal);
            channelSftp.exit();
            channelSftp.disconnect();
            session.disconnect();
            return archivoFinal;
        }catch(Exception ex){
            System.err.println("Error enviarSFTP() Host "+SFTPHOST+"("+SFTPUSER+", "+SFTPPASS+"): "+ex);
        }
        return null;
    }    
    /************************************************SUBIR ARCHIVOS AL SERVIDOR FTP************************************************/
    
    
    public static class SUserInfo implements UserInfo {
 
        private String password;
        private String passPhrase;

        public SUserInfo (String password, String passPhrase) {
            this.password = password;
            this.passPhrase = passPhrase;
        }

        @Override
        public String getPassphrase() {
            return passPhrase;
        }
        @Override
        public String getPassword() {
            return password;
        }
        
        @Override
        public boolean promptPassphrase(String arg0) {
            return true;
        }

        @Override
        public boolean promptPassword(String arg0) {
            return false;
        }

        @Override
        public boolean promptYesNo(String arg0) {
            return true;
        }

        @Override
        public void showMessage(String arg0) {
            System.out.println("SUserInfo.showMessage(): " + arg0);
        }
    }

    
 
    public long getTimeStamp() {
        return System.currentTimeMillis();
    }

	
    @SuppressWarnings("unused")
	private BufferedImage resizeImage(BufferedImage originalImage, int type){
    	BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, tipoImagenFormato);
    	Graphics2D g = resizedImage.createGraphics();
    	g.setBackground(colorFondo);
    	g.clearRect(0, 0, IMG_WIDTH, IMG_HEIGHT);
    	g.setColor(Color.WHITE);
    	g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
    	g.dispose();
    	return resizedImage;
    }
     
    private BufferedImage resizeImageWithHint(BufferedImage originalImage, int type){
        
    	BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, tipoImagenFormato);
    	Graphics2D g = resizedImage.createGraphics();
    	g.setBackground(colorFondo);
    	g.clearRect(0, 0, IMG_WIDTH, IMG_HEIGHT);
    	g.setColor(Color.WHITE);
    	g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
    	g.dispose();	
    	
    	g.setComposite(AlphaComposite.Src);
     
    	g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
    	RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    	g.setRenderingHint(RenderingHints.KEY_RENDERING,
    	RenderingHints.VALUE_RENDER_QUALITY);
    	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
    	RenderingHints.VALUE_ANTIALIAS_ON);
    	g.dispose();
    	return resizedImage;
    } 
    
    @SuppressWarnings("unused")
	private BufferedImage resizeImageScale(BufferedImage sourceImage, int type){
    	BufferedImage img2=null;

        try{
        	int width = sourceImage.getWidth();
            int height = sourceImage.getHeight();
        	if(width>=height){
                float extraSize=    height-IMG_HEIGHT;
                float percentHight = (extraSize/height)*100;
                float percentWidth = width - ((width/100)*percentHight);
                BufferedImage img = new BufferedImage((int)percentWidth, IMG_HEIGHT, tipoImagenFormato);
                Image scaledImage = sourceImage.getScaledInstance((int)percentWidth, IMG_HEIGHT, Image.SCALE_SMOOTH);
                Graphics2D g = img.createGraphics();
            	g.setBackground(colorFondo); 
            	g.clearRect(0, 0, img.getWidth(), img.getHeight());
                g.drawImage(scaledImage, 0, 0, null);
                g.dispose();
                img2 = new BufferedImage(IMG_WIDTH, IMG_HEIGHT ,tipoImagenFormato);
                img2 = img.getSubimage((int)((percentWidth-IMG_WIDTH)/2), 0, IMG_WIDTH, IMG_HEIGHT);

            }else{
            	float extraSize=    width-IMG_WIDTH;
                float percentWidth = (extraSize/width)*100;
                float percentHight = height - ((height/100)*percentWidth);
                BufferedImage img = new BufferedImage(IMG_WIDTH, (int)percentHight, tipoImagenFormato);
                Image scaledImage = sourceImage.getScaledInstance(IMG_WIDTH, (int)percentHight, Image.SCALE_SMOOTH);
                Graphics2D g = img.createGraphics();
            	g.setBackground(colorFondo); 
            	g.clearRect(0, 0, img.getWidth(), img.getHeight());
            	g.setColor(Color.WHITE);
                g.drawImage(scaledImage, 0, 0, null);
                g.dispose();
                img2 = new BufferedImage(IMG_WIDTH, IMG_HEIGHT ,tipoImagenFormato);
                img2 = img.getSubimage(0, (int)((percentHight-IMG_HEIGHT)/2), IMG_WIDTH, IMG_HEIGHT);

            }
        	if(img2!=null){
        		Graphics2D g2 = img2.createGraphics();
            	g2.setBackground(Color.WHITE);
            	g2.dispose();
        	}
        	return  img2;
        }catch(Exception ex){
        	System.err.println("resizeImageScale(): "+ex);
        	return resizeImageWithHint(sourceImage, type);
        }
    } 
    
    private static BufferedImage reducirImagen(BufferedImage sourceImage, int type){
    	BufferedImage img=null;
        	double wi = sourceImage.getWidth();
        	double hi = sourceImage.getHeight();
        	int wn=0, hn=0;
        	int xn=0, yn=0;
        	if(wi>=hi){
        		double porc = IMG_WIDTH / wi;
        		wn = (int)(wi * porc);
        		hn = (int)(hi*porc);
        		yn = (int)((IMG_HEIGHT-hn)/2);
        	}else{
        		double porc = IMG_HEIGHT / hi;
        		wn = (int)(wi * porc);
        		hn = (int)(hi*porc);
        		xn =  (int)((IMG_WIDTH-wn)/2);
        	}
        	img = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, tipoImagenFormato);
        	Image scaledImage = sourceImage.getScaledInstance(wn, hn, Image.SCALE_SMOOTH);
        	Graphics2D g = img.createGraphics();
        	g.setBackground(colorFondo); 
        	g.clearRect(0, 0, IMG_WIDTH, IMG_HEIGHT);
            g.drawImage(scaledImage, xn, yn, null);
            g.dispose();
            
        	return img;
    } 
	
    @SuppressWarnings("unused")
	private static InputStream resizeIMGtoInputStream(File oImg){
    	InputStream ima = null;
    	try{
    		BufferedImage originalImage = ImageIO.read(oImg);
    		//int type = originalImage.getType() == 0? tipoImagenFormato : originalImage.getType();
    		int type = tipoImagenFormato;
    		BufferedImage resizeImageJpg = reducirImagen(originalImage, type);
    		ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(resizeImageJpg, "png", os);
            ima = new ByteArrayInputStream(os.toByteArray());
    	}catch(Exception ex){
    		System.err.println("Error resizeIMGtoInputStream(): "+ex);
    	}
    	return ima;
    }
   
}
