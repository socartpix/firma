package pe.gob.reniec.pkiep.demoinvoker.controller.servlet;

public class Gmap {
	private String app;
	private String clientId;
	private String clientSecret;
	private String idFile;
	private String type;
	private String protocol;
	private String fileDownloadUrl;
	private String fileDownloadLogoUrl;
	private String fileDownloadStampUrl;
	private String fileUploadUrl;
	private String contentFile;
	private String reason;
	private String isSignatureVisible;
	private String stampAppearanceId;
	private String pageNumber;
	private String posx;
	private String posy;
	private String fontSize;
	private String dcfilter;
	private String timestamp;
	private String outputFile;
	private String maxFileSize;
	   public Gmap(){} 
	   
	
	public String getapp(){return app;}
	public String getclientId(){return clientId;}
	public String getclientSecret(){return clientSecret;}
	public String getidFile(){return idFile;}
	public String gettype(){return type;}
	public String getprotocol(){return protocol;}
	public String getfileDownloadUrl(){return fileDownloadUrl;}
	public String getfileDownloadLogoUrl(){return fileDownloadLogoUrl;}
	public String getfileDownloadStampUrl(){return fileDownloadStampUrl;}
	public String getfileUploadUrl(){return fileUploadUrl;}
	public String getcontentFile(){return contentFile;}
	public String getreason(){return reason;}
	public String getisSignatureVisible(){return isSignatureVisible;}
	public String getstampAppearanceId(){return stampAppearanceId;}
	public String getpageNumber(){return pageNumber;}
	public String getposx(){return posx;}
	public String getposy(){return posy;}
	public String getfontSize(){return fontSize;}
	public String getdcfilter(){return dcfilter;}
	public String gettimestamp(){return timestamp;}
	public String getoutputFile(){return outputFile;}
	public String getmaxFileSize(){return maxFileSize;}
	

	public void setapp(String app){this.app=app;}
	public void setclientId(String clientId){this.clientId=clientId;}
	public void setclientSecret(String clientSecret){this.clientSecret=clientSecret;}
	public void setidFile(String idFile){this.idFile=idFile;}
	public void settype(String type){this.type=type;}
	public void setprotocol(String protocol){this.protocol=protocol;}
	public void setfileDownloadUrl(String fileDownloadUrl){this.fileDownloadUrl=fileDownloadUrl;}
	public void setfileDownloadLogoUrl(String fileDownloadLogoUrl){this.fileDownloadLogoUrl=fileDownloadLogoUrl;}
	public void setfileDownloadStampUrl(String fileDownloadStampUrl){this.fileDownloadStampUrl=fileDownloadStampUrl;}
	public void setfileUploadUrl(String fileUploadUrl){this.fileUploadUrl=fileUploadUrl;}
	public void setcontentFile(String contentFile){this.contentFile=contentFile;}
	public void setreason(String reason){this.reason=reason;}
	public void setisSignatureVisible(String isSignatureVisible){this.isSignatureVisible=isSignatureVisible;}
	public void setstampAppearanceId(String stampAppearanceId){this.stampAppearanceId=stampAppearanceId;}
	public void setpageNumber(String pageNumber){this.pageNumber=pageNumber;}
	public void setposx(String posx){this.posx=posx;}
	public void setposy(String posy){this.posy=posy;}
	public void setfontSize(String fontSize){this.fontSize=fontSize;}
	public void setdcfilter(String dcfilter){this.dcfilter=dcfilter;}
	public void settimestamp(String timestamp){this.timestamp=timestamp;}
	public void setoutputFile(String outputFile){this.outputFile=outputFile;}
	public void setmaxFileSize(String maxFileSize){this.maxFileSize=maxFileSize;}

}
