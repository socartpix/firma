package pe.gob.reniec.pkiep.demoinvoker.controller.servlet;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.io.ByteSource;
import com.google.common.io.Files;

import pe.gob.mc.cnp.model.domain.Documento;
import pe.gob.mc.cnp.model.factory.Factory;
import pe.gob.mc.cnp.util.Archivos;

/*
	import java.nio.file.Path;
	import java.nio.file.Paths;*/
/**
 * Servlet implementation class GetFileServlet
 */
public class GetFileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String UPLOAD_DIRECTORY = "upload";	
		@Autowired
	private Archivos archivos;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetFileServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String documentName = request.getParameter("documentName").toString(); 
		String path_tmp=System.getProperty("java.io.tmpdir") + File.separator + UPLOAD_DIRECTORY + File.separator + documentName;
/*		Path path = Paths.get(System.getProperty("java.io.tmpdir") + File.separator + UPLOAD_DIRECTORY + File.separator + documentName);
		byte[] data = Files.readAllBytes(path);				*/		
		   File file = new File(path_tmp);
		    ByteSource source = Files.asByteSource(file);
		    
		    byte[] data = source.read();
		response.setContentType ("application/pdf");	
		response.setHeader("Content-disposition", "attachment; filename=" + documentName);	
		response.setHeader("Cache-Control", "max-age=30");
        response.setHeader("Pragma", "No-cache");
        response.setDateHeader("Expires",0);
        response.setContentLength(data.length);
        
        ServletOutputStream out = response.getOutputStream();
        out.write(data,0,data.length);
        out.flush();
        out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Cert_firma.setCodigoDocumento(Integer.parseInt(request.getParameter("nroficha")));
		System.out.println(request.getParameter("nroficha"));
		
		String nroficha= request.getParameter("nroficha");
		nroficha=nroficha.trim();
		
		String rutaArchivo= request.getParameter("rutaArchivo");
		String NombreArchivo= request.getParameter("nombreArchivo");
		//
		//rutaArchivo=rutaArchivo.trim();
		Archivos archivos= new Archivos();
		rutaArchivo=archivos.getHostFirma();
		//
		String NombreArchivo_sin_ext= getNameWitoutExtension(NombreArchivo);
		String Extension= getExtensionByApacheCommonLib(NombreArchivo) ;
		//
		String NombreArchivo_firma= NombreArchivo_sin_ext + "[R]."  +Extension;
		NombreArchivo_firma=NombreArchivo_firma.trim();
		//
		System.out.println(request.getParameter("nroficha"));
		System.out.println(nroficha);
		System.out.println("Ruta Archivo");
		System.out.println(rutaArchivo);
		System.out.println("Nombre Archivo");
		System.out.println(NombreArchivo);
		System.out.println("Setting Cert ____1 ");
		System.out.println("Setting Numero Cert ____ 1 ");
		Documento Cert_firma = new Documento();
		Cert_firma.setNombreArchivo_firma(NombreArchivo_firma);
		Cert_firma.setRutaArchivo_firma(rutaArchivo);
		Cert_firma.setMensajeOut(null);
		Cert_firma.setCodigoDocumento(Integer.parseInt(nroficha));
		
		
		System.out.println("Setting Cert ____1 ");
		System.out.println(Cert_firma);
		

		Factory.getInstance().getDocumentoBO().setFirmaDocumento(Cert_firma);
		System.out.println("Setting Cert ____2 ");
		System.out.println("ACtualizando base de datos certificado ");
		response.getWriter().print(rutaArchivo+NombreArchivo_firma);
	}
	public String getExtensionByApacheCommonLib(String filename) {
	    return FilenameUtils.getExtension(filename);
	}
	public String getNameWitoutExtension(String filename) {
	    return FilenameUtils.removeExtension(filename);
	}
	
}
