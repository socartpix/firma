package pe.gob.reniec.pkiep.demoinvoker.controller.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.*;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.FilenameUtils;

//import java.nio.charset.StandardCharsets;
import pe.gob.reniec.pkiep.demoinvoker.util.Configuration;

/**
 * Servlet implementation class ArgumentsPAdESReFirmaPDFServlet
 */
public class ArgumentsPAdESReFirmaPDFServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private final Configuration config = Configuration.getInstance();	
	private static String DOCUMENT_NAME = "demo.pdf";
	private static String DOCUMENT_NAME_SIGN = "demo[R].pdf";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ArgumentsPAdESReFirmaPDFServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter writer =  response.getWriter();
		writer.write(DOCUMENT_NAME_SIGN);
		writer.close();
        System.out.println("PDF CARGADO" );
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//req.getParameter("user");
		//CNP359037233.pdf
		setDOCUMENT_NAME(request.getParameter("nombreArchivo"));
		System.out.println("request --->>>"+request.getParameter("nombreArchivo"));
		System.out.println("DOCUMENT NAME :"+ DOCUMENT_NAME);
		/*Creando nombre de la firma*/
		String extension=getExtensionByApacheCommonLib(DOCUMENT_NAME);
		String fileNameWithOutExt = FilenameUtils.removeExtension(DOCUMENT_NAME);
		String document_signed=fileNameWithOutExt+"[R]."+extension;
		setDOCUMENT_NAME_SIGN(document_signed);
		System.out.println("DOCUMENT NAME signed:"+ DOCUMENT_NAME_SIGN);
		/*Creando nombre de la firma*/
		
		
		String pathServlet = request.getServletPath();
        String fullPathServlet = request.getRequestURL().toString();
        int resInt = fullPathServlet.length() - pathServlet.length();
        //String serverPath = fullPathServlet.substring(0, resInt + 1);
        String serverPath= fullPathServlet.substring(0, resInt + 1);
        //http://repositorioarchivos.cultura.gob.pe/cnp_files/anexos/
        String serverPath_extern=request.getParameter("rutaArchivo");
        if(!serverPath.contains("localhost")){        
        	// EN PRODUCCIÓN: config.getProtocol() define el protocolo.
        	serverPath = config.getProtocol() + "://" + serverPath.replace("http://", "").replace("https://", "");
        }
        
		PrintWriter writer =  response.getWriter();
        try{
           String type = request.getParameter("type").toString();         
           String arguments = "";
           
           String protocol = "";
           if (serverPath.contains("https://")){
        	   protocol = "S";
           }else{
        	   protocol = "T";
           }
           System.out.println("PDF entrando a fase 1" );
           if(type.equals("W")){
               System.out.println("PDF entrando a servicio web" );
        	   arguments = paramWeb(protocol, serverPath,serverPath_extern);
           }else if(type.equals("L")){
        	  // arguments = paramLocal(protocol, serverPath);
           }           
           writer.write(arguments);
           writer.close();
           }
       catch(Exception ex)
      {
    	   ex.getStackTrace();
      }
	}
	
	
	public String paramWeb(String protocol, String ServerPath, String serverPath_extern){		
		Gmap map=new Gmap();

		map.setapp("pdf");
		map.setclientId(config.getClientId());   
		map.setclientSecret(config.getClientSecret());         
		map.setidFile("001"); 
		map.settype("W"); 
		map.setprotocol(protocol); 
		//		map.setfileDownloadUrl(ServerPath + "common/documents/" + getDOCUMENT_NAME());
		map.setfileDownloadUrl(serverPath_extern + DOCUMENT_NAME);
		map.setfileDownloadLogoUrl(ServerPath + "common/images/firma/iLogo1.png");
		map.setfileDownloadStampUrl(ServerPath + "common/images/firma/iFirma1.png");
		map.setfileUploadUrl(ServerPath + "UploadServlet");     
		//ServerPath + getDOCUMENT_NAME()
		System.out.println(serverPath_extern + DOCUMENT_NAME);
		System.out.println(ServerPath + "common/images/firma/iLogo1.png");
		System.out.println(ServerPath + "common/images/firma/iFirma1.png");
		System.out.println(ServerPath+ "UploadServlet");   
		map.setcontentFile(DOCUMENT_NAME);
		map.setreason("Soy el autor del documento");
		map.setisSignatureVisible("true"); 
		map.setstampAppearanceId("0"); 
		map.setpageNumber("0");
		map.setposx("5"); 
		map.setposy("5");     
		map.setfontSize("7"); 
		map.setdcfilter(".*FIR.*|.*FAU.*"); 
		map.settimestamp("false");   
		map.setoutputFile(DOCUMENT_NAME_SIGN);    
		map.setmaxFileSize("5242880"); 
		final Gson gson = new Gson();
		final String representacionJSON = gson.toJson(map);
		/*String param = "";
        ObjectMapper mapper = new ObjectMapper();        
        //Java to JSON
        try {
        	  System.out.println( ServerPath + "documents/" + DOCUMENT_NAME );
            Map<String, String> map = new HashMap<String, String>();
            map.put("app", "pdf");
            map.put("clientId", config.getClientId());   
            map.put("clientSecret", config.getClientSecret());         
            map.put("idFile", "001"); 
            map.put("type", "W"); 
            map.put("protocol", protocol); 
            map.put("fileDownloadUrl", ServerPath + "documents/" + DOCUMENT_NAME);
            map.put("fileDownloadLogoUrl", ServerPath + "resources/img/iLogo1.png");
            map.put("fileDownloadStampUrl", ServerPath + "resources/img/iFirma1.png");
            map.put("fileUploadUrl", ServerPath + "UploadServlet");      
            map.put("contentFile", DOCUMENT_NAME);
            map.put("reason", "Soy el autor del documento");
            map.put("isSignatureVisible", "true");             
            map.put("stampAppearanceId", "0"); 
            map.put("pageNumber", "0");
            map.put("posx", "5"); 
            map.put("posy", "5");                 
            map.put("fontSize", "7"); 
            map.put("dcfilter", ".*FIR.*|.*FAU.*"); 
            map.put("timestamp", "false");               
            map.put("outputFile", DOCUMENT_NAME_SIGN);    
            map.put("maxFileSize", "5242880"); 
            //JSON
            param = mapper.writeValueAsString(map);                    
            System.out.println(param);
                      
            //Base64 (JAVA 8)
            param = DatatypeConverter.printBase64Binary(param.getBytes());
            //param = Base64.encodeBase64(param.getBytes(StandardCharsets.UTF_8()));    
            // param = Base64.getEncoder().encodeToString(param.getBytes(StandardCharsets.UTF_8()));         
           //   param = DatatypeConverter.printBase64Binary(param.getBytes());
            System.out.println(param);             
        } catch (Exception ex) {            
        }	
        */
		System.out.println(representacionJSON);
		String param=representacionJSON;
	 	param=DatatypeConverter.printBase64Binary(param.getBytes());
		System.out.println(param);
        return param;
	}

	public static String getDOCUMENT_NAME() {
		return DOCUMENT_NAME;
	}

	public static void setDOCUMENT_NAME(String dOCUMENT_NAME) {
		DOCUMENT_NAME = dOCUMENT_NAME;
	}
	public String getExtensionByApacheCommonLib(String filename) {
	    return FilenameUtils.getExtension(filename);
	}

	public static String getDOCUMENT_NAME_SIGN() {
		return DOCUMENT_NAME_SIGN;
	}

	public static void setDOCUMENT_NAME_SIGN(String dOCUMENT_NAME_SIGN) {
		DOCUMENT_NAME_SIGN = dOCUMENT_NAME_SIGN;
	}
	

}
