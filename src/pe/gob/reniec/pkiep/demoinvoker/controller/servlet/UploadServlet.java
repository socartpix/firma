package pe.gob.reniec.pkiep.demoinvoker.controller.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;

import pe.gob.mc.cnp.model.dao.iface.DocumentoMapper;
import pe.gob.mc.cnp.model.domain.Documento;
import pe.gob.mc.cnp.util.Archivos;


/**
 * Servlet implementation class UploadServlet
 */
public class UploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String UPLOAD_DIRECTORY = "upload";
	private static final int THRESHOLD_SIZE 	= 1024 * 1024 * 3; 	// MB
	private static final int MAX_FILE_SIZE 		= 1024 * 1024 * 100; // MB
	private static final int MAX_REQUEST_SIZE 	= 1024 * 1024 * 110; // MB 
	private DocumentoMapper documentoMapper;
	@Autowired
private Archivos archivos;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter writer =  response.getWriter();
		writer.write("Directorio temporal donde se almacenan los archivos: " + System.getProperty("java.io.tmpdir"));
		writer.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	try {	    	
			if (!ServletFileUpload.isMultipartContent(request)) {	
				response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);	
				return;
			}	
			
			DiskFileItemFactory factory = new DiskFileItemFactory();
			factory.setSizeThreshold(THRESHOLD_SIZE);
			factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
			
			System.out.println(System.getProperty("java.io.tmpdir"));
			
			ServletFileUpload upload = new ServletFileUpload(factory);
			upload.setFileSizeMax(MAX_FILE_SIZE);
			upload.setSizeMax(MAX_REQUEST_SIZE);
						
			//String uploadPath = getServletContext().getRealPath("") + File.separator + UPLOAD_DIRECTORY;			
			String uploadPath = System.getProperty("java.io.tmpdir") + File.separator + UPLOAD_DIRECTORY;	
			File uploadDir = new File(uploadPath);
			if (!uploadDir.exists()) {
				uploadDir.mkdir();
			}				
			
			List<FileItem> formItems = upload.parseRequest(request);
			Iterator<FileItem> iter = formItems.iterator();						
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();				
				if (!item.isFormField()) {
					String idFile = item.getFieldName();//idFile asignado en los argumentos, se puede utilizar como un id.	
					String fileName = URLDecoder.decode(item.getName(),"UTF-8");						
					String filePath = uploadPath + File.separator + fileName;
					File storeFile = new File(filePath);		
					Archivos archivo= new Archivos();
					String extension=getExtensionByApacheCommonLib(fileName);
					//			nombreArchivo =   enviarFTP(getiFTPHOST(),getuFTPUser(), getuFTPPass(), fileu,getRutaFTPFotos(), 'I');
					//archivo.enviarFTPS_firma(archivo.getiFTPHOST(), archivo.getuFTPUser(), archivo.getuFTPPass(),  archivo.getRutaFTPAnexos(), 'A',storeFile, "extension");
					Documento Cert_firma = new Documento();
					System.out.println("Setting Cert");
					//Cert_firma.setCodigoDocumento(Integer.parseInt(request.getParameter("nroficha")));
					/*System.out.println(request.getParameter("nroficha"));
					Cert_firma.setNombreArchivo_firma("Teststs");
					Cert_firma.setRutaArchivo_firma("testetst ruta");
					Cert_firma.setMensajeOut(null);
					System.out.println("Setting Cert ____2 ");*/
					/*
					 * 	setNombreArchivo_firma
					setRutaArchivo_firma
					
					documentoMapper.modificarCertificadoActivo_firma(Cert_firma);
					 * */
					System.out.println("Setting Cert END");
					//nombreArchivo =   enviarFTP(getiFTPHOST(),getuFTPUser(), getuFTPPass(), fileu,getRutaFTPFotos(), 'I');
					archivo.enviarFTPS_firma(archivo.getiFTPHOST(), archivo.getuFTPUser(), archivo.getuFTPPass(), archivo.getRutaAnexosFirmado() , 'A',storeFile, extension,fileName);
		
					System.out.println();
					if(storeFile.exists()) storeFile.delete();
					item.write(storeFile);
				}
			}			
			response.setStatus(HttpServletResponse.SC_OK);			
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);	
		}	
	}
	
	public String getExtensionByApacheCommonLib(String filename) {
	    return FilenameUtils.getExtension(filename);
	}

}
