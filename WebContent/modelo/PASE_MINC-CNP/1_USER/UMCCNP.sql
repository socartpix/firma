-- Create the user 
create user UMCCNP identified by UMCCNP
  default tablespace USUARIO_CONEXION
  temporary tablespace TEMP
  profile DEFAULT
  quota unlimited on usuario_conexion;
-- Grant/Revoke role privileges 
grant connect to UMCCNP;
grant cre_general to UMCCNP;
grant resource to UMCCNP;
-- Grant/Revoke system privileges 
grant create operator to UMCCNP;
grant create procedure to UMCCNP;
grant create role to UMCCNP;
grant create sequence to UMCCNP;
grant create session to UMCCNP;
grant create synonym to UMCCNP;
grant create table to UMCCNP;
grant create trigger to UMCCNP;
grant create type to UMCCNP;
grant create user to UMCCNP;
grant create view to UMCCNP;
grant debug connect session to UMCCNP;
grant unlimited tablespace to UMCCNP;
