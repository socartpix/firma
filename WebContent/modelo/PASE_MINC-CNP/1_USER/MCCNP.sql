-- Create the user 
create user MCCNP identified by MCCNP
  default tablespace MCCNP
  temporary tablespace TEMP
  profile DEFAULT
  quota unlimited on MCCNP;
-- Grant/Revoke role privileges 
grant connect to MCCNP;
grant cre_general to MCCNP;
grant resource to MCCNP;
-- Grant/Revoke system privileges 
grant create operator to MCCNP;
grant create procedure to MCCNP;
grant create session to MCCNP;
grant create type to MCCNP;
grant unlimited tablespace to MCCNP;
grant rol_mcsgagen_delete to MCCNP;
grant rol_mcsgagen_insert to MCCNP;
grant rol_mcsgagen_select to MCCNP;
grant rol_sisinc_delete to MCCNP;
grant rol_sisinc_insert to MCCNP;
grant rol_sisinc_select to MCCNP;

grant SELECT on MCSGAGEN.TM_GENUBIGEO to MCCNP;
grant SELECT on MCSGAGEN.TM_GENTIPOIMAGEN to MCCNP;
grant SELECT on MCSGAGEN.TM_GENPAIS to MCCNP;
grant SELECT on MCSGAGEN.tm_SGAUSUARIO to MCCNP;
--nuevos 0705
grant SELECT on MCSGAGEN.TR_SGAPERFILUSUARIO to MCCNP;
grant SELECT on MCSGAGEN.TM_SGAPERFIL to MCCNP;

grant SELECT on sismc.tramite_documentario_frd to MCCNP;
grant SELECT on sisinc.entidad_frd to MCCNP;

