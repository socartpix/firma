CREATE OR REPLACE TRIGGER TI_EXPEDIENTE
     BEFORE INSERT ON MCCNP.TR_CNPEXPEDIENTE        FOR EACH ROW
BEGIN  
             IF :new.ncodigoexpediente IS NULL THEN  
                 SELECT MCCNP.SEQ_EXPEDIENTE.nextval INTO :new.ncodigoexpediente FROM DUAL;  
             END IF;  
         END;
		 
		 CREATE OR REPLACE TRIGGER TI_HISTORIALOBSERVACION
     BEFORE INSERT ON MCCNP.TR_CNPHISTORIALOBSERVACION        FOR EACH ROW
BEGIN
             IF :new.ncodigoOBSERVACION IS NULL THEN
                 SELECT MCCNP.SEQ_HISTORIALOBSERVACION.nextval INTO :new.ncodigoOBSERVACION FROM DUAL;
             END IF;
         END;
		 
		 CREATE OR REPLACE TRIGGER TI_DOCUMENTO
     BEFORE INSERT ON MCCNP.TR_CNPDOCUMENTO        FOR EACH ROW
BEGIN  
             IF :new.NCODIGODOCUMENTO IS NULL THEN  
                 SELECT MCCNP.SEQ_DOCUMENTO.nextval INTO :new.NCODIGODOCUMENTO FROM DUAL;  
             END IF;  
         END;
		 
		 CREATE OR REPLACE TRIGGER TI_BIEN
     BEFORE INSERT ON MCCNP.TR_CNPBIEN        FOR EACH ROW
BEGIN  
             IF :new.ncodigobien IS NULL THEN  
                 SELECT MCCNP.SEQ_BIEN.nextval INTO :new.ncodigobien FROM DUAL;  
             END IF;  
         END;
		 
		 CREATE OR REPLACE TRIGGER TI_DETALLEBIEN
     BEFORE INSERT ON MCCNP.TR_CNPDETALLEBIEN        FOR EACH ROW
BEGIN  
             IF :new.ncodigodetallebien IS NULL THEN  
                 SELECT MCCNP.SEQ_DETALLEBIEN.nextval INTO :new.ncodigodetallebien FROM DUAL;  
             END IF;  
         END;

		 	 CREATE OR REPLACE TRIGGER TI_FICHA
     BEFORE INSERT ON MCCNP.TR_CNPFICHA        FOR EACH ROW
BEGIN  
             IF :new.ncodigoFICHA IS NULL THEN  
                 SELECT MCCNP.SEQ_FICHA.nextval INTO :new.ncodigoFICHA FROM DUAL;  
             END IF;  
         END;
		 
	
  
  CREATE OR REPLACE TRIGGER MCCNP.TI_codigohabilitado
BEFORE INSERT ON MCCNP.TR_CNPcodigohabilitado
FOR EACH ROW
BEGIN
   IF :new.ncodigohabilitado IS NULL THEN
     SELECT MCCNP.SEQ_codigohabilitado.nextval INTO :new.ncodigohabilitado FROM DUAL;
   END IF;
 END;
 	 