-- Create sequence 
create sequence SEQ_EXPEDIENTE
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;
/
-- Create sequence 
create sequence SEQ_HISTORIALOBSERVACION
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;
/
-- Create sequence 
create sequence SEQ_DOCUMENTO
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;
/
-- Create sequence 
create sequence SEQ_BIEN
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;
/
-- Create sequence 
create sequence SEQ_DETALLEBIEN
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;
/

-- Create sequence 
create sequence SEQ_FICHA
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;
/

 -- Create sequence  
create sequence MCCNP.SEQ_codigohabilitado
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;
 /
