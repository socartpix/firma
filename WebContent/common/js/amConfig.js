var limiteTexto = 2000;


function limitador(ed){
	var elemEditor = $jq($jq(ed.getElement()).parent());
	var btnCorrector = $jq('<img class="btnCorrector" src="/MINC-SINAR/common/images/iconos/abc.png">');
//	var btnCorrector = $jq('<img class="btnCorrector" src="/MINC-BMU/common/images/iconos/abc.png">');
	
	$jq(elemEditor).prepend($jq(btnCorrector));

	$jq(btnCorrector).click(function(){
		corectorPopUp(ed);
	});
	ed.onPaste.add(function(ed, e){
		e.preventDefault();
	    var text = "";

    	var ifrm = document.getElementById($jq(ed.contentAreaContainer).find("iframe").attr("id"));
    	var docIfr = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;
    	
	    if(window.clipboardData){
	    	text = window.clipboardData.getData("Text");
	    	
	    }else{
	    	text = (e.originalEvent || e).clipboardData.getData('text/plain');
	    }
	    var docElem;
	    if(docIfr.document){
	    	docElem=$jq('body', docIfr.document);
	    }else{
	    	docElem=$jq('body', docIfr);
	    }
insertarTextoEnEditor(docIfr, text);
	});

	ed.onKeyUp.add(function(ed, e){
				var txCont =  $jq(ed.getElement());
				var txt =  $jq('<p>'+ed.getContent()+'</p>').text()+"";
				  var dvCounter = $jq(txCont).parent().find('.taCounter');
					if($jq(dvCounter).size()==0){
						dvCounter = $jq("<span class=\"taCounter\">"+(txt.length)+"/"+ed.getParam('max_chars')+"</span>");
						$jq($jq(txCont).parent().find('.mceEditor')).after(dvCounter);
					}
					$jq(dvCounter).html((txt.length)+"/"+ed.getParam('max_chars'));
			});
    
};

function insertarTextoEnEditor(elem, val){

	try{
		(elem.document || elem).execCommand('insertText', false, amTextDiff(val))
	}catch(err){
	 (elem.document || elem).selection.createRange().pasteHTML(val);
	}

}


var editorPrincipal;

function corectorPopUp(ed){
	var popup = window.open('/MINC-SINAR/corrector.jsp',('bmuCorrector'+parseInt(Math.random()*100000000)),'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=655,height=260,screenX=150,screenY=150,top=150,left=150');
	//var popup = window.open('/MINC-BMU/corrector.jsp',('bmuCorrector'+parseInt(Math.random()*100000000)),'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=655,height=260,screenX=150,screenY=150,top=150,left=150');
	popup.editor = ed;
}

function imgZoom(img){
	window.open('/MINC-SINAR/zoom.jsp?img='+img,'bmuPopupWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=620,height=680,screenX=150,screenY=150,top=150,left=150')
//	window.open('/MINC-BMU/zoom.jsp?img='+img,'bmuPopupWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=620,height=680,screenX=150,screenY=150,top=150,left=150')
}
/*
$jq(function(){
$jq( "body" ).on( "keyup", "textarea", function(event){
	var max_car = $jq(this).attr('maxlength');
	if(!(max_car!=null && max_car>0)){
		max_car = $jq(this).data('max_car'); 
		if(!(max_car!=null && max_car>0)){
			max_car = limiteTexto;
		}
	}else{
		$jq(this).removeAttr('maxlength');
		$jq(this).data('max_car', max_car);
	}
	
	
	var dvCounter = null;
	var txCont = $jq(this);
	if (!($jq(txCont).is('[readonly]') || $jq(txCont).is('[disabled]'))) { 
		 var txt = $jq(txCont).val()+"";
		 
		dvCounter = $jq(txCont).parent().find('.taCounter');
		if($jq(dvCounter).size()==0){
			dvCounter = $jq("<span class=\"taCounter\">"+(txt.length)+"/"+max_car+"</span>");
			$jq(txCont).after(dvCounter);
			
		}
		$jq(dvCounter).html((txt.length)+"/"+max_car);
	}
});
});
*/
/*
function addTACounter(opcounter){
	if(opcounter==true){
		$jq('textarea').each(function( index ) {
			var max_car = $jq(this).attr('maxlength');
			if(!(max_car!=null && max_car>0)){
				max_car = $jq(this).data('max_car'); 
				if(!(max_car!=null && max_car>0)){
					max_car = limiteTexto;
				}
			}else{
				$jq(this).removeAttr('maxlength');
				$jq(this).data('max_car', max_car);
			}
			
			if($jq(this).css('display')!='none'){
			
			var limitC = max_car;
			//if($jq(txCont).attr('maxlength')!=null && $jq(txtCont).attr('maxlength')>0){
			//	limitC = parseInt($(txtCont).attr('maxlength'));
			//}
			 var txt = $jq(txCont).val()+"";
			  var dvCounter = $jq("<span class=\"taCounter\">"+(txt.length)+"/"+limitC+"</span>");
			  
			  $jq(txCont).after(dvCounter);
			  $jq(txCont).keyup(function() {
				  var txt = $jq(txCont).val()+"";
				  $jq(dvCounter).html((txt.length)+"/"+limitC);
			  });
			}
		});
	}
}
*/
function bajarArqOrdenMat(tdMaterial){
	var item = $jq(tdMaterial).parent().parent();
	item.insertAfter(item.next());
	actualizaArqOrden();
}

function subirArqOrdenMat(tdMaterial){
	var item = $jq(tdMaterial).parent().parent();
	item.insertBefore(item.prev());
	actualizaArqOrden();
}
function actualizaArqOrden(){
	var cadOrden = "";
	$jq(".dtArqMat tr span.oculto").each(function( index ) {
		if(index==0)cadOrden=cadOrden+""+$jq(this).text();
		else cadOrden=cadOrden+"-"+$jq(this).text();
	});
	$jq(".arqOrdenMat input").val(cadOrden);
	actArqOrdenMat(cadOrden);
}



function amTextDiff(texto) {
	return limpiarCarEspeciales($jq("<div/>").html(texto).text());
}

function limpiarCarEspeciales(texto){
    if(texto!=null){
            texto = texto.replace("�", "&#338;");
		texto = texto.replace("�", "oe");
		texto = texto.replace("�", "S");
		texto = texto.replace("�", "s");
		texto = texto.replace("�", "Y");
		texto = texto.replace("�", "f");
                
                texto = texto.replace("�", "-");
                texto = texto.replace("�", "-");
                texto = texto.replace("�", "'");
                texto = texto.replace("�", "'");
                texto = texto.replace("�", "'");
                texto = texto.replace("�", "\"");
                texto = texto.replace("�", "\"");
                texto = texto.replace("�", "\"");
                texto = texto.replace("�", "");
                texto = texto.replace("�", "");
                texto = texto.replace("�", "-");
                texto = texto.replace("�", "...");
                texto = texto.replace("�", "%");
                texto = texto.replace("�", "Euro");
                texto = texto.replace("�", "TM");
    }
    return texto;
}


function showLoadBMU(){
	$jq("#loadBMU").fadeIn('fast');
}

function hideLoadBMU(){
	$jq("#loadBMU").hide();
}

function toHeader(){
	var body = $jq("html, body");
	body.animate({scrollTop:0}, '500', 'swing', function() { 
	});
}

$jq(function(){
	$jq("#toTop").click(function(){
		toHeader();
	});
	$jq("#toBandeja").click(function(){
		cargarBandeja();
	});
	/*$jq(window).scroll(function() {
		  $jq("#toTop").css( "display", "block" ).fadeOut( "slow" );
		});*/
});