
		//<![CDATA[
		var documentName_ = null;		
		//
		//Listener obligatorio a implementar
		window.addEventListener('getArguments', function (e) {								
			type = e.detail;					
			if(type === 'W'){
				ArgumentosFirmaWeb(); // Llama a sendArguments al terminar.
		    }else if(type === 'L'){
		    	ArgumentosFirmaLocal(); // Llama a sendArguments al terminar.
		    } 				    
		});
		function sendArguments(arg_){				
			//Event obligatorio a implementar					
			dispatchEventClient('sendArguments', arg_); // Envía los argumentos																
		}
		//Listener obligatorio a implementar
		window.addEventListener('invokerOk', function (e) { 
			type = e.detail;		   
			if(type === 'W'){
				OkWeb();	
		    }else if(type === 'L'){
		    	OkLocal();	
		    }	
		});
		//Listener obligatorio a implementar
		window.addEventListener('invokerCancel', function (e) {    
			Cancel();	
		});

		//::LÓGICA DEL PROGRAMADOR::	
		var getUrl = window.location;
		var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
		function ArgumentosFirmaWeb(){
			document.getElementById("signedDocument").href="#";
			$jq.get(baseUrl+"/ArgumentsPAdESReFirmaPDFServlet", {}, function(data, status) {			
				if (status === 'success') {
					documentName_ = data;	
					console.log("Documentos");
					console.log(documentName_);
					//Obtiene argumentos
					$jq.post(baseUrl+"/ArgumentsPAdESReFirmaPDFServlet", {
						type : "W",
						rutaArchivoCompleta:$jq("#svbody\\:svBodyI\\:svfrmRegistroCertificadoCERT\\:parametrosRegistroCertificadoCERT\\:txtArchivoCertCERT").text(),
						nombreArchivo:$jq("#svbody\\:svBodyI\\:svfrmRegistroCertificadoCERT\\:parametrosRegistroCertificadoCERT\\:txtArchivo_CertCERT").text(),
						rutaArchivo:$jq("#svbody\\:svBodyI\\:svfrmRegistroCertificadoCERT\\:parametrosRegistroCertificadoCERT\\:txtRuta_CertCERT").text(),
						nroficha:$jq("#svbody\\:svBodyI\\:svfrmRegistroCertificadoCERT\\:parametrosRegistroCertificadoCERT\\:nroficha").text()
						
				
					}, function(data, status) {	
						if (status === 'success') {
							//alert("Data: " + data + "\nStatus: " + status);						
							sendArguments(data);
						}else{
							alert('No se encuentra argumentsPAdESReFirmaPDFServlet');
						}						
					});
				}else{
					alert('No se encuentra argumentsPAdESReFirmaPDFServlet');
				}													
			});				
		}

		function ArgumentosFirmaLocal(){
			
			var getUrl = window.location;
			var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
			document.getElementById("signedDocument").href="#";
			$jq.get(baseUrl+"/argumentsPAdESReFirmaPDFServlet", {}, function(data, status) {			
				if (status === 'success') {
					documentName_ = data;	
					//Obtiene argumentos
					$.post(baseUrl+"/argumentsPAdESReFirmaPDFServlet", {
						type : "L"
					}, function(data, status) {
						if (status === 'success') {
							//alert("Data: " + data + "\nStatus: " + status);						
							sendArguments(data);
						}else{
							alert('No se encuentra argumentsPAdESReFirmaPDFServlet');
						}								
					});
				}else{
					alert('No se encuentra argumentsPAdESReFirmaPDFServlet');
				}														
			});				
		}
		
		
		function OkWeb(){
			var getUrl = window.location;
			var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
			console.log("Tesest ok web setting firma before");
			setting_firma();
			console.log("Tesest ok web setting firma after");
			alert("Documento firmado desde una URL correctamente.");
		
			//https://repositorio.cultura.pe/prueba/CNP-805452202.pdf
			//document.getElementById("signedDocument").href=baseUrl+"/getFileServlet?documentName=" + documentName_;
			//location.reload();
			
		}

		function setting_firma(){
			$jq.post(baseUrl+"/getFileServlet", {
				type : "W",
				rutaArchivoCompleta:$jq("#svbody\\:svBodyI\\:svfrmRegistroCertificadoCERT\\:parametrosRegistroCertificadoCERT\\:txtArchivoCertCERT").text(),
				nombreArchivo:$jq("#svbody\\:svBodyI\\:svfrmRegistroCertificadoCERT\\:parametrosRegistroCertificadoCERT\\:txtArchivo_CertCERT").text(),
				rutaArchivo:$jq("#svbody\\:svBodyI\\:svfrmRegistroCertificadoCERT\\:parametrosRegistroCertificadoCERT\\:txtRuta_CertCERT").text(),
				nroficha:$jq("#svbody\\:svBodyI\\:svfrmRegistroCertificadoCERT\\:parametrosRegistroCertificadoCERT\\:nroficha").text()
				
		
			}, function(data, status) {	
				if (status === 'success') {
					//alert("Data: " + data + "\nStatus: " + status);						
					//sendArguments(data);
					var filename=$jq("#svbody\\:svBodyI\\:svfrmRegistroCertificadoCERT\\:parametrosRegistroCertificadoCERT\\:txtArchivo_CertCERT").text();
					filename=filename.split('.').slice(0, -1).join('.');
					console.log("Data final--->"+data);
					document.getElementById("signedDocument").href=data;
					$jq("#signedDocument").click;
					$jq("#iconsvMnuIz\\:j_id28\\:j_id35").click;
				}else{
					alert('No se encuentra getFileServlet');
				}						
			});
		}
		
		function OkLocal(){
			var getUrl = window.location;
			var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
			alert("Documento firmado desde la PC correctamente.");
			document.getElementById("signedDocument").href=baseUrl+"/getFileServlet?documentName=" + documentName_;
		}
		
		function Cancel(){
			alert("El proceso de firma digital fue cancelado.");
			document.getElementById("signedDocument").href="#";
		}								
	//]]>
		
//Si no existe la ruta no oculta todo
		function HideSign(){
			if($jq("#svbody\\:svBodyI\\:svfrmRegistroCertificadoCERT\\:parametrosRegistroCertificadoCERT\\:txtArchivoCertCERT").text()==""){
				console.log("ocultando");
				$jq("#signedDocument").hide();
				$jq("#signDocument").hide();
			}
			var txtArchivo_Firma=$jq("#svbody\\:svBodyI\\:svfrmRegistroCertificadoCERT\\:parametrosRegistroCertificadoCERT\\:txtArchivo_Firma").text().trim();
			var txtRuta_Firma=$jq("#svbody\\:svBodyI\\:svfrmRegistroCertificadoCERT\\:parametrosRegistroCertificadoCERT\\:txtRuta_Firma").text().trim();
			
			if(txtArchivo_Firma!=""){
				document.getElementById("signedDocument").href=txtRuta_Firma+txtArchivo_Firma;
			}
			
			//Colocando firmas 
			//txtRuta_Firma
			//txtArchivo_Firma
		}
		
		
		
		