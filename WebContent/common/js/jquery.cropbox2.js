(function() {
  // helper functions
  function is_touch_device() {
    return 'ontouchstart' in window || // works on most browsers
           'onmsgesturechange' in window; // works on ie10
  }

  function fill(value, target, container) {
    if (value + target < container)
      value = container - target;
    return value > 0 ? 0 : value;
  }

  function uri2blob(dataURI) {
      var uriComponents = dataURI.split(',');
      var byteString = atob(uriComponents[1]);
      var mimeString = uriComponents[0].split(':')[1].split(';')[0];
      var ab = new ArrayBuffer(byteString.length);
      var ia = new Uint8Array(ab);
      for (var i = 0; i < byteString.length; i++)
          ia[i] = byteString.charCodeAt(i);
      return new Blob([ab], { type: mimeString });
  }

  var pluginName = 'cropbox';

  function factory($jq) {
    function Crop($jqimage, options) {
      this.width = null;
      this.height = null;
      this.img_width = null;
      this.img_height = null;
      this.img_left = 0;
      this.img_top = 0;
      this.minPercent = null;
      this.options = options;
      this.$jqimage = $jqimage;
      this.$jqimage.hide().prop('draggable', false).addClass('cropImage').wrap('<div class="cropFrame" />'); // wrap image in frame;
      this.$jqframe = this.$jqimage.parent();
      this.init();
    }

    Crop.prototype = {
      init: function () {

        var self = this;

        var defaultControls = $jq('<div/>', { 'class' : 'cropControls' })
              .append($jq('<span>'+this.options.label+'</span>'))
              .append($jq('<a/>', { 'class' : 'cropZoomIn' }).on('click', $jq.proxy(this.zoomIn, this)))
              .append($jq('<a/>', { 'class' : 'cropZoomOut' }).on('click', $jq.proxy(this.zoomOut, this)));

        this.$jqframe.append(this.options.controls || defaultControls);
        this.updateOptions();

        if (typeof $jq.fn.hammer === 'function' || typeof Hammer !== 'undefined') {
          var hammerit, dragData;
          if (typeof $jq.fn.hammer === 'function')
            hammerit = this.$jqimage.hammer();
          else
            hammerit = Hammer(this.$jqimage.get(0));

          hammerit.on('touch', function(e) {
            e.gesture.preventDefault();
          }).on("dragleft dragright dragup dragdown", function(e) {
            if (!dragData)
              dragData = {
                startX: self.img_left,
                startY: self.img_top
              };
            dragData.dx = e.gesture.deltaX;
            dragData.dy = e.gesture.deltaY;
            e.gesture.preventDefault();
            e.gesture.stopPropagation();
            self.drag.call(self, dragData, true);
          }).on('release', function(e) {
            e.gesture.preventDefault();
            dragData = null;
            self.update.call(self);
          }).on('doubletap', function(e) {
            e.gesture.preventDefault();
            self.zoomIn.call(self);
          }).on('pinchin', function (e) {
            e.gesture.preventDefault();
            self.zoomOut.call(self);
          }).on('pinchout', function (e) {
            e.gesture.preventDefault();
            self.zoomIn.call(self);
          });
        } else {
          this.$jqimage.on('mousedown.' + pluginName, function(e1) {
            var dragData = {
              startX: self.img_left,
              startY: self.img_top
            };
            e1.preventDefault();
            $jq(document).on('mousemove.' + pluginName, function (e2) {
              dragData.dx = e2.pageX - e1.pageX;
              dragData.dy = e2.pageY - e1.pageY;
              self.drag.call(self, dragData, true);
            }).on('mouseup.' + pluginName, function() {
              self.update.call(self);
              $jq(document).off('mouseup.' + pluginName);
              $jq(document).off('mousemove.' + pluginName);
            });
          });
        }
        if ($jq.fn.mousewheel) {
          this.$jqimage.on('mousewheel.' + pluginName, function (e) {
            e.preventDefault();
            if (e.deltaY < 0)
              self.zoomIn.call(self);
            else
              self.zoomOut.call(self);
          });
        }
    	  
      },

      updateOptions: function () {
        var self = this;
        self.img_top = 0;
        self.img_left = 0;
        self.$jqimage.css({width: '', left: self.img_left, top: self.img_top});
        self.$jqframe.width(self.options.width).height(self.options.height);
        self.$jqframe.off('.' + pluginName);
        self.$jqframe.removeClass('hover');
        if (self.options.showControls === 'always' || self.options.showControls === 'auto' && is_touch_device())
          self.$jqframe.addClass('hover');
        else if (self.options.showControls !== 'never') {
          self.$jqframe.on('mouseenter.' + pluginName, function () { self.$jqframe.addClass('hover'); });
          self.$jqframe.on('mouseleave.' + pluginName, function () { self.$jqframe.removeClass('hover'); });
        }

        // Image hack to get width and height on IE
        var img = new Image();
        img.src = self.$jqimage.attr('src');
        img.onload = function () {
          self.width = img.width;
          self.height = img.height;
          img.src = '';
          img.onload = null;
          self.percent = undefined;
          self.fit.call(self);
          if (self.options.result)
            self.setCrop.call(self, self.options.result);
          else
            self.zoom.call(self, self.minPercent);
          self.$jqimage.fadeIn('fast');
        };
      },

      remove: function () {
        var hammerit;
        if (typeof $jq.fn.hammer === 'function')
          hammerit = this.$jqimage.hammer();
        else if (typeof Hammer !== 'undefined')
          hammerit = Hammer(this.$jqimage.get(0));
        if (hammerit)
          hammerit.off('mousedown dragleft dragright dragup dragdown release doubletap pinchin pinchout');
        this.$jqframe.off('.' + pluginName);
        this.$jqimage.off('.' + pluginName);
        this.$jqimage.css({width: '', left: '', top: ''});
        this.$jqimage.removeClass('cropImage');
        this.$jqimage.removeData(pluginName);
        this.$jqimage.insertAfter(this.$jqframe);
        this.$jqframe.removeClass('cropFrame');
        this.$jqframe.removeAttr('style');
        this.$jqframe.empty();
        this.$jqframe.hide();
      },

      fit: function () {
        var widthRatio = this.options.width / this.width,
          heightRatio = this.options.height / this.height;
        this.minPercent = (widthRatio >= heightRatio) ? widthRatio : heightRatio;
      },

      setCrop: function (result) {
        this.percent = Math.max(this.options.width/result.cropW, this.options.height/result.cropH);
        this.img_width = Math.ceil(this.width*this.percent);
        this.img_height = Math.ceil(this.height*this.percent);
        this.img_left = -Math.floor(result.cropX*this.percent);
        this.img_top = -Math.floor(result.cropY*this.percent);
        this.$jqimage.css({ width: this.img_width, left: this.img_left, top: this.img_top });
        this.update();
      },

      zoom: function(percent) {
        var old_percent = this.percent;

        this.percent = Math.max(this.minPercent, Math.min(this.options.maxZoom, percent));
        this.img_width = Math.ceil(this.width * this.percent);
        this.img_height = Math.ceil(this.height * this.percent);

        if (old_percent) {
          var zoomFactor = this.percent / old_percent;
          this.img_left = fill((1 - zoomFactor) * this.options.width / 2 + zoomFactor * this.img_left, this.img_width, this.options.width);
          this.img_top = fill((1 - zoomFactor) * this.options.height / 2 + zoomFactor * this.img_top, this.img_height, this.options.height);
        } else {
          this.img_left = fill((this.options.width - this.img_width) / 2, this.img_width,  this.options.width);
          this.img_top = fill((this.options.height - this.img_height) / 2, this.img_height, this.options.height);
        }

        this.$jqimage.css({ width: this.img_width, left: this.img_left, top: this.img_top });
        this.update();
      },
      zoomIn: function() {
    	  this.zoom(this.percent + (1 - this.minPercent) / (this.options.zoom - 1 || 1));
      },
      zoomOut: function() {
    	  this.zoom(this.percent - (1 - this.minPercent) / (this.options.zoom - 1 || 1));
      },
      drag: function(data, skipupdate) {
        this.img_left = fill(data.startX + data.dx, this.img_width, this.options.width);
        this.img_top = fill(data.startY + data.dy, this.img_height, this.options.height);
        this.$jqimage.css({ left: this.img_left, top: this.img_top });
        if (skipupdate)
          this.update();
      },
      update: function() {
        this.result = {
          cropX: -Math.ceil(this.img_left / this.percent),
          cropY: -Math.ceil(this.img_top / this.percent),
          cropW: Math.floor(this.options.width / this.percent),
          cropH: Math.floor(this.options.height / this.percent),
          stretch: this.minPercent > 1
        };

        this.$jqimage.trigger(pluginName, [this.result, this]);
      },
      getDataURL: function () {
        var canvas = document.createElement('canvas'), ctx = canvas.getContext('2d');
        canvas.width = this.options.width;
        canvas.height = this.options.height;
        ctx.drawImage(this.$jqimage.get(0), this.result.cropX, this.result.cropY, this.result.cropW, this.result.cropH, 0, 0, this.options.width, this.options.height);
        return canvas.toDataURL();
      },
      getBlob: function () {
        return uri2blob(this.getDataURL());
      }
    };

    $jq.fn[pluginName] = function(options) {
      return this.each(function() {
    	 if(jQuery.browser!=null && ( jQuery.browser.msie==true && (parseInt(jQuery.browser.version)<=9))){
    			
    	  var self = this;
		  $jq(self).load(function(){
			  $jq(self).parent().css('position', 'relative');
			  $jq(self).css('position', 'absolute');
			  var alturaCrop = $jq( self ).height();
			  var anchoCrop = $jq( self ).width();
			  var alturaPadre = $jq( self ).parent().height();
			  var anchoPadre = $jq( self ).parent().width();
			  var radioCrop = 1;
			  if(alturaCrop>=anchoCrop){
				  radioCrop = alturaPadre/alturaCrop;
				  
			  }else{
				  radioCrop = anchoPadre/anchoCrop;
				  
			  }
			  $jq( self ).height(alturaCrop * radioCrop);
			  $jq( self ).width(anchoCrop * radioCrop);
			  alturaCrop = $jq( self ).height();
			  anchoCrop = $jq( self ).width();
			  
			  $jq(self).css('top', (alturaPadre-alturaCrop)/2);
			  $jq(self).css('left', (anchoPadre-anchoCrop)/2);
			  
			  
		  });
		  
		  $jq(self).parent().css('position', 'relative');
		  $jq(self).css('position', 'absolute');
		  var alturaCrop = $jq( self ).height();
		  var anchoCrop = $jq( self ).width();
		  var alturaPadre = $jq( self ).parent().height();
		  var anchoPadre = $jq( self ).parent().width();
		  var radioCrop = 1;
		  if(alturaCrop>=anchoCrop){
			  radioCrop = alturaPadre/alturaCrop;
			  
		  }else{
			  radioCrop = anchoPadre/anchoCrop;
			  
		  }
		  $jq( self ).height(alturaCrop * radioCrop);
		  $jq( self ).width(anchoCrop * radioCrop);
		  alturaCrop = $jq( self ).height();
		  anchoCrop = $jq( self ).width();
		  
		  $jq(self).css('top', (alturaPadre-alturaCrop)/2);
		  $jq(self).css('left', (anchoPadre-anchoCrop)/2);
    		    		  
    		    		  
    		    	 }else{
        var $jqthis = $jq(this), inst = $jqthis.data(pluginName);
        if (!inst) {
          var opts = $jq.extend({}, $jq.fn[pluginName].defaultOptions, options);
          $jqthis.data(pluginName, new Crop($jqthis, opts));
        } else if (options) {
          $jq.extend(inst.options, options);
          inst.updateOptions();
        }
    		    	  }
      });
    };

    $jq.fn[pluginName].defaultOptions = {
      width: 200,
      height: 200,
      zoom: 10,
      maxZoom: 3,
      controls: null,
      showControls: 'auto',
      label: ''
    };
  }

  if (typeof require === "function" && typeof exports === "object" && typeof module === "object")
      factory(require("jquery"));
  else if (typeof define === "function" && define.amd)
      define(["jquery"], factory);
  else
      factory(window.jQueryMC || window.Zepto);

})();
