 PROCEDURE SP_CNPBUSFICHAXCRITERIO(NNUMEROCERTIFICADO_ IN NUMBER, DFECHAEXPEDICIONINI_ IN DATE, DFECHAEXPEDICIONFIN_ IN DATE, SNUMEROEXPEDIENTE_ IN VARCHAR2 ,  SNOMBREAPELLIDO_ IN VARCHAR2, SDESCRIPCIONBIEN_ IN VARCHAR2,  SCODIGOBIEN_ IN VARCHAR2,  SNROINFORMETECNICO_ IN VARCHAR2,  SANIOINFORMETECNICO_ IN VARCHAR2, NCODIGOPAIS_ IN NUMBER , SESTADOFICHA_ IN CHAR,NUSUARIOREGISTRADOR_ IN NUMBER, CCURSOR_RESULTADO OUT RESULTADO_CURSOR, NRESULTADO OUT INTEGER, SERR_DESC OUT VARCHAR) AS
  SSQL VARCHAR2(9900);
  BEGIN
    NRESULTADO := 1;
    SSQL:= ' ';
    BEGIN    ---CNP.NNUMEROCERTIFICADO,
      SSQL:= SSQL || ' SELECT ROW_NUMBER()   OVER (ORDER BY  CNP.NCODIGOFICHA DESC) NRO,   CNP.NCODIGOFICHA ,
  CNP.NCANTIDADAUTORIZADA, to_date (CNP.DFECHAEXPEDICION,''dd/MM/YY'') DFECHAEXPEDICION,  CNP.NNUMEROCERTIFICADO,  CNP.SOBSERVACION,  CNP.SUSUARIOREGISTRADOR   ,
  CNP.NUSUARIOREGISTRADOR,  CNP.SUSUARIOEVALUADOR ,  CNP.NUSUARIOEVALUADOR,  CNP.SUSUARIODIRECTOR , CNP.SACTIVO, CNP.NUSUARIODIRECTOR, CNP.SESTADOFICHA
    ,EXPE.NCODIGOEXPEDIENTE, EXPE.SNUMEROEXPEDIENTE, EXPE.SNOMBREAPELLIDO,EXPE.SANIO , EXPE.STIPODOCUMENTO, EXPE.SNUMEROIDENTIDAD, EXPE.SDOMICILIO, EXPE.NCODIGOPAIS, EXPE.NCANTIDADSOLICITADA, EXPE.DFECHASOLICITUD
    ,CNP.STIPOPERSONA,CNP.SNACIONALIDAD, CNP.SDESCRIPCIONINICIAL, CNP.SDESCRIPCIONFINAL , CNP.SMOTIVONOATENCION  ,  to_date (CNP.DFECHANOATENCION,''dd/MM/YY'') DFECHANOATENCION
    ,( SELECT C.SNOMBREARCHIVO FROM MCCNP.TR_CNPCERTIFICADO C WHERE C.NCODIGOFICHA= CNP.NCODIGOFICHA AND C.SACTIVO=''S'' and rownum=1 ) SNOMBREARCHIVO,
    ( SELECT C.SRUTAARCHIVO FROM MCCNP.TR_CNPCERTIFICADO C WHERE C.NCODIGOFICHA= CNP.NCODIGOFICHA AND C.SACTIVO=''S'' and rownum=1 ) SRUTAARCHIVO
     FROM MCCNP.TR_CNPFICHA CNP LEFT JOIN MCCNP.TR_CNPEXPEDIENTE EXPE ON CNP.NCODIGOFICHA=EXPE.NCODIGOFICHA
     WHERE 1=1 ';

      IF NNUMEROCERTIFICADO_ IS NOT NULL THEN
        SSQL:= SSQL || ' AND CNP.NNUMEROCERTIFICADO = ' || NNUMEROCERTIFICADO_ || ' ';
      END IF;

        IF  DFECHAEXPEDICIONINI_ IS NOT NULL AND DFECHAEXPEDICIONFIN_ IS NOT NULL THEN
          SSQL:= SSQL || ' AND (  to_date( TRUNC(CNP.DFECHAEXPEDICION), ''DD/MM/YY'' )  BETWEEN  TO_DATE(''' || to_char(DFECHAEXPEDICIONINI_,'dd/mm/yyyy') || ''' , ''DD/MM/YYYY'' )  AND  to_date(''' ||  to_char( DFECHAEXPEDICIONFIN_,'dd/mm/yyyy') || ''' , ''DD/MM/YYYY'' ) )' ;
          END IF;

      IF SESTADOFICHA_ IS NOT NULL THEN
        SSQL:= SSQL || ' AND CNP.SESTADOFICHA = ''' || SESTADOFICHA_ || ''' ';
      END IF;

      IF SESTADOFICHA_ IS NOT NULL THEN
        SSQL:= SSQL || ' AND CNP.SESTADOFICHA = ''' || SESTADOFICHA_ || ''' ';
      END IF;

      IF SNUMEROEXPEDIENTE_ IS NOT NULL THEN
        SSQL:= SSQL || ' AND EXPE.SNUMEROEXPEDIENTE = ''' || SNUMEROEXPEDIENTE_ || ''' ';
      END IF;

      IF SNOMBREAPELLIDO_ IS NOT NULL THEN
        SSQL:= SSQL || ' AND MCCNP.FNC_TRANSLATE_DETERMINISTIC(EXPE.SNOMBREAPELLIDO) like ''%' || MCCNP.FNC_TRANSLATE_DETERMINISTIC(SNOMBREAPELLIDO_) || '%'' ';
      END IF;

      IF SDESCRIPCIONBIEN_ IS NOT NULL THEN
        SSQL:= SSQL || ' AND cnp.ncodigoficha in ( select bi.ncodigoficha from mccnp.tr_cnpbien bi where bi.sactivo=''S'' AND MCCNP.FNC_TRANSLATE_DETERMINISTIC(bi.sdescripcion) like ''%' || MCCNP.FNC_TRANSLATE_DETERMINISTIC(SDESCRIPCIONBIEN_) || '%'' ) ';
      END IF;

      IF SCODIGOBIEN_ IS NOT NULL THEN
        SSQL:= SSQL || ' AND cnp.ncodigoficha in ( select cc.ncodigoficha from mccnp.tr_cnpcodigohabilitado cc where cc.sactivo=''S'' and cc.nnumerobien = ' || To_number ( SCODIGOBIEN_ ) ||' ) ';
      END IF;

      IF (SNROINFORMETECNICO_ IS NOT NULL AND SANIOINFORMETECNICO_ IS NOT NULL) THEN
        SSQL:= SSQL || ' AND cnp.ncodigoficha in ( select doc.ncodigoficha from mccnp.tr_cnpdocumento doc where doc.sactivo=''S'' and To_number(doc.snumero) = ' || To_number ( SNROINFORMETECNICO_ ) ||' and doc.ssufijo =''' || SANIOINFORMETECNICO_||''' ) ';
      END IF;

      IF NCODIGOPAIS_ IS NOT NULL THEN
        SSQL:= SSQL || ' AND EXPE.NCODIGOPAIS = ' || NCODIGOPAIS_ || ' ';
      END IF;

      IF NUSUARIOREGISTRADOR_ IS NOT NULL THEN
        SSQL:= SSQL || ' AND CNP.NUSUARIOREGISTRADOR = ' || NUSUARIOREGISTRADOR_ || '';
      END IF;

      SSQL:= SSQL || ' AND CNP.SACTIVO = ''S'' ORDER BY CNP.NCODIGOFICHA DESC ';

           /*INSERT INTO MCCNP.TEMPORAL1 VALUES( SSQL);

  COMMIT;*/

      OPEN CCURSOR_RESULTADO FOR SSQL;
      EXCEPTION
        WHEN OTHERS THEN
          NRESULTADO := SQLCODE;
          SERR_DESC := SUBSTR(SQLERRM,1,100);
    END;
  END SP_CNPBUSFICHAXCRITERIO;
